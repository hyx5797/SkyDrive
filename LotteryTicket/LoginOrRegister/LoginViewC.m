//
//  LoginViewC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/2.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "LoginViewC.h"

@interface LoginViewC ()

@property(nonatomic,strong)UIImageView *imgTopBg;
@property (nonatomic,strong)UIButton *selectedBtn;//选中登录还是注册
@property (nonatomic,strong)UILabel *labBtnRedLine;
@property (nonatomic,strong)UIView *viewLogin;
//登录的账号、登录的密码
@property(nonatomic,strong)UnderLineTextField *txt_login_account;
@property(nonatomic,strong)UnderLineTextField *txt_login_pwd;

@property (nonatomic,strong)UIView *viewRegister;
//登录的账号、姓名、邮箱、登录密码、确认密码
@property(nonatomic,strong)UnderLineTextField *txt_reigister_account;
@property(nonatomic,strong)UnderLineTextField *txt_register_username;
@property(nonatomic,strong)UnderLineTextField *txt_reigister_mailbox;
@property(nonatomic,strong)UnderLineTextField *txt_register_pwd;
@property(nonatomic,strong)UnderLineTextField *txt_reigister_sure_pwd;


@end

@implementation LoginViewC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self.strLogin_register isEqualToString:@"注册"]) {
        UIButton *btn = [self.imgTopBg viewWithTag:11];
                self.selectedBtn =btn;
    }else{
        //默认登录
        UIButton *btn = [self.imgTopBg viewWithTag:10];
          self.selectedBtn =btn;
    }
    [self btnPhoneOrPwdMethodClick:self.selectedBtn];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorHex(EBEBEB);
    
    //试玩按钮
    UIButton *btn_demo = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_demo setTitle:@"试玩" forState:UIControlStateNormal];
    [btn_demo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_demo setBackgroundColor:[UIColor clearColor]];
    btn_demo.frame = CGRectMake(SCREEN_WIDTH - 100, 7, 80, 30);
    btn_demo.layer.cornerRadius = 15.0;
    btn_demo.layer.masksToBounds = YES;
    btn_demo.layer.borderColor = [UIColor whiteColor].CGColor;
    btn_demo.layer.borderWidth = 1.0;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:btn_demo];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self createFrame];
}
#pragma 初始化界面的UI
- (void)createFrame{
    //头部的背景蓝色条
    UIImageView *imgTopBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    imgTopBg.backgroundColor = STATUS_BAR_BGCOLOR;
    imgTopBg.userInteractionEnabled = YES;
    self.imgTopBg = imgTopBg;
    [self.view addSubview:imgTopBg];
    
    UIButton *btn_user_login = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_user_login.tag = 10;
    self.selectedBtn = btn_user_login;
    [btn_user_login setTitle:@"用户登录" forState:UIControlStateNormal];
    [btn_user_login setTitleColor:[UIColorHex(ffffff) colorWithAlphaComponent:1.0] forState:UIControlStateNormal];
    [btn_user_login setTitleColor:[UIColorHex(ffffff) colorWithAlphaComponent:1.0] forState:UIControlStateSelected];
    btn_user_login.titleLabel.font = MFont(14);
    btn_user_login.frame = CGRectMake(15,20, (SCREEN_WIDTH -30)/2, 34);
    [btn_user_login addTarget:self action:@selector(btnPhoneOrPwdMethodClick:) forControlEvents:UIControlEventTouchUpInside];
    [imgTopBg addSubview:btn_user_login];

    UIButton *btn_user_register = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_user_register.tag = 11;
    [btn_user_register setTitle:@"免费注册" forState:UIControlStateNormal];
    [btn_user_register setTitleColor:[UIColorHex(ffffff) colorWithAlphaComponent:1.0] forState:UIControlStateNormal];
    [btn_user_register setTitleColor:[UIColorHex(ffffff) colorWithAlphaComponent:1.0] forState:UIControlStateSelected];
    btn_user_register.titleLabel.font = MFont(14);
    btn_user_register.frame = CGRectMake(btn_user_login.right, 20, (SCREEN_WIDTH -30)/2, 34);
    [btn_user_register addTarget:self action:@selector(btnPhoneOrPwdMethodClick:) forControlEvents:UIControlEventTouchUpInside];
    [imgTopBg addSubview:btn_user_register];

    UILabel *labBtnRedLine = [[UILabel alloc]init];
    labBtnRedLine.frame = CGRectMake(0, btn_user_login.bottom + 6, 112, 1);
    labBtnRedLine.centerX = self.selectedBtn.centerX;
    labBtnRedLine.backgroundColor = [UIColorHex(ffffff) colorWithAlphaComponent:1.0];
    self.labBtnRedLine = labBtnRedLine;
    [self.view addSubview:labBtnRedLine];
    
    [self initViewLogin];
    [self initViewRegister];
}
#pragma 登录或者注册的选择
- (void)btnPhoneOrPwdMethodClick:(UIButton *)btn{
    if (btn != self.selectedBtn ){
        self.selectedBtn.selected=YES;
        btn.selected=YES;
        self.selectedBtn=btn;
    }else{
        self.selectedBtn.selected=YES;
    }

    if (self.selectedBtn.tag ==10) {
        self.viewLogin.hidden = NO;
        self.viewRegister.hidden = YES;
    }else{
        self.viewLogin.hidden = YES;
        self.viewRegister.hidden = NO;
    }
    //横线的移动
    self.labBtnRedLine.centerX = self.selectedBtn.centerX;
}
#pragma 登录视图
- (void)initViewLogin{
    self.viewLogin = [[UIView alloc]initWithFrame:CGRectMake(10, 90, SCREEN_WIDTH - 20, 300)];
    self.viewLogin.backgroundColor = [UIColor whiteColor];
    self.viewLogin.layer.cornerRadius = 10.0;
    self.viewLogin.layer.masksToBounds = YES;
    [self.view addSubview:self.viewLogin];
    
    //欢迎词
    UILabel *lab_Welcome_speech = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.viewLogin.width - 30, 45)];
    lab_Welcome_speech.text = @"欢迎来到【网盘】娱乐,我们更专注彩票，博彩乐趣都在这里。";
    lab_Welcome_speech.numberOfLines = 0;
    lab_Welcome_speech.textColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    lab_Welcome_speech.font = LFont(15);
    [self.viewLogin addSubview:lab_Welcome_speech];

    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Welcome_speech.bottom + 10, self.viewLogin.width, 1)];
    labLine.backgroundColor = UIColorHex(EBEBEB);
    [self.viewLogin addSubview:labLine];
    
    self.txt_login_account = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0, labLine.bottom, self.viewLogin.width, 50)];
    self.txt_login_account.placeholder = @"请输入登录账号";
    self.txt_login_account.textColor = UIColorHex(A4A4A4);
    self.txt_login_account.font = RFont(15);
    [self.viewLogin addSubview:self.txt_login_account];
    UIView *view_account = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 50)];
    view_account.backgroundColor = [UIColor clearColor];
    self.txt_login_account.leftView = view_account;
    self.txt_login_account.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    self.txt_login_pwd = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0, self.txt_login_account.bottom, self.viewLogin.width, 50)];
    self.txt_login_pwd.placeholder = @"你的密码";
    self.txt_login_pwd.textColor = UIColorHex(A4A4A4);
    self.txt_login_pwd.font = RFont(15);
    [self.viewLogin addSubview:self.txt_login_pwd];
    UIView *view_pwd = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 50)];
    view_pwd.backgroundColor = [UIColor clearColor];
    self.txt_login_pwd.leftView = view_pwd;
    self.txt_login_pwd.leftViewMode = UITextFieldViewModeAlways;
    
    UIButton *btn_keep_pwd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_keep_pwd setTitle:@"记住密码" forState:UIControlStateNormal];
    [btn_keep_pwd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn_keep_pwd.frame = CGRectMake(15, self.txt_login_pwd.bottom + 10, 100, 30);
    [self.viewLogin addSubview:btn_keep_pwd];
    
    UIButton *btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_login setTitle:@"登录" forState:UIControlStateNormal];
    [btn_login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_login.frame = CGRectMake(15, btn_keep_pwd.bottom + 18, self.viewLogin.width - 30, 49);
    [btn_login addTarget:self action:@selector(btn_loginClick) forControlEvents:UIControlEventTouchUpInside];
    btn_login.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_login.layer.cornerRadius = 20.0;
    btn_login.layer.masksToBounds = YES;
    [self.viewLogin addSubview:btn_login];
}
#pragma 注册视图
- (void)initViewRegister{
    self.viewRegister = [[UIView alloc]initWithFrame:CGRectMake(10, 90, SCREEN_WIDTH - 20, 410)];
    self.viewRegister.backgroundColor = [UIColor whiteColor];
    self.viewRegister.hidden = YES;
    self.viewRegister.layer.cornerRadius = 10.0;
    self.viewRegister.layer.masksToBounds = YES;
    [self.view addSubview:self.viewRegister];
    
    //欢迎词
    UILabel *lab_Welcome_speech = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.viewRegister.width - 30, 45)];
    lab_Welcome_speech.text = @"欢迎来到【网盘】娱乐,我们更专注彩票，博彩乐趣都在这里。";
    lab_Welcome_speech.numberOfLines = 0;
    lab_Welcome_speech.textColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    lab_Welcome_speech.font = LFont(15);
    [self.viewRegister addSubview:lab_Welcome_speech];
    
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Welcome_speech.bottom + 10, self.viewRegister.width, 1)];
    labLine.backgroundColor = UIColorHex(EBEBEB);
    [self.viewRegister addSubview:labLine];
    
    self.txt_reigister_account = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0, labLine.bottom, self.viewRegister.width, 50)];
    self.txt_reigister_account.placeholder = @"请输入登录账号";
    self.txt_reigister_account.textColor = UIColorHex(A4A4A4);
    self.txt_reigister_account.font = RFont(15);
    [self.viewRegister addSubview:self.txt_reigister_account];
    UILabel *lab_register_account = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    lab_register_account.font =RFont(15);
    lab_register_account.text = @"登录账号";
    lab_register_account.textColor = COLOR(135, 135, 135);
    lab_register_account.textAlignment = NSTextAlignmentCenter;
    self.txt_reigister_account.leftView = lab_register_account;
    self.txt_reigister_account.leftViewMode = UITextFieldViewModeAlways;
    
    self.txt_register_username = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0, self.txt_reigister_account.bottom, self.viewRegister.width, 50)];
    self.txt_register_username.placeholder = @"请输入真实姓名";
    self.txt_register_username.textColor = UIColorHex(A4A4A4);
    self.txt_register_username.font = RFont(15);
    [self.viewRegister addSubview:self.txt_register_username];
    UILabel *lab_register_username = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    lab_register_username.font =RFont(15);
    lab_register_username.text = @"姓名";
    lab_register_username.textColor = COLOR(135, 135, 135);
    lab_register_username.textAlignment = NSTextAlignmentCenter;
    self.txt_register_username.leftView = lab_register_username;
    self.txt_register_username.leftViewMode = UITextFieldViewModeAlways;
    
    self.txt_reigister_mailbox = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0, self.txt_register_username.bottom, self.viewRegister.width, 50)];
    self.txt_reigister_mailbox.placeholder = @"请输入邮箱";
    self.txt_reigister_mailbox.textColor = UIColorHex(A4A4A4);
    self.txt_reigister_mailbox.font = RFont(15);
    [self.viewRegister addSubview:self.txt_reigister_mailbox];
    UILabel *lab_register_mailbox = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    lab_register_mailbox.font =RFont(15);
    lab_register_mailbox.text = @"邮箱";
    lab_register_mailbox.textColor = COLOR(135, 135, 135);
    lab_register_mailbox.textAlignment = NSTextAlignmentCenter;
    self.txt_reigister_mailbox.leftView = lab_register_mailbox;
    self.txt_reigister_mailbox.leftViewMode = UITextFieldViewModeAlways;
    
    self.txt_register_pwd= [[UnderLineTextField alloc]initWithFrame:CGRectMake(0,  self.txt_reigister_mailbox .bottom, self.viewRegister.width, 50)];
    self.txt_register_pwd.placeholder = @"5-20字符和数字";
    self.txt_register_pwd.textColor = UIColorHex(A4A4A4);
    self.txt_register_pwd.font = RFont(15);
    [self.viewRegister addSubview:self.txt_register_pwd];
    UILabel *lab_register_pwd = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    lab_register_pwd.font =RFont(15);
    lab_register_pwd.text = @"登录密码";
    lab_register_pwd.textColor = COLOR(135, 135, 135);
    lab_register_pwd.textAlignment = NSTextAlignmentCenter;
    self.txt_register_pwd.leftView = lab_register_pwd;
    self.txt_register_pwd.leftViewMode = UITextFieldViewModeAlways;
    
    self.txt_reigister_sure_pwd = [[UnderLineTextField alloc]initWithFrame:CGRectMake(0,  self.txt_register_pwd.bottom, self.viewRegister.width, 50)];
    self.txt_reigister_sure_pwd.placeholder = @"请再次输入登录密码";
    self.txt_reigister_sure_pwd.textColor = UIColorHex(A4A4A4);
    self.txt_reigister_sure_pwd.font = RFont(15);
    [self.viewRegister addSubview:self.txt_reigister_sure_pwd];
    UILabel *lab_register_sure_pwd = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    lab_register_sure_pwd.font =RFont(15);
    lab_register_sure_pwd.text = @"确认密码";
    lab_register_sure_pwd.textColor = COLOR(135, 135, 135);
    lab_register_sure_pwd.textAlignment = NSTextAlignmentCenter;
    self.txt_reigister_sure_pwd.leftView = lab_register_sure_pwd;
    self.txt_reigister_sure_pwd.leftViewMode = UITextFieldViewModeAlways;
    
    UIButton *btn_register = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_register setTitle:@"免费注册" forState:UIControlStateNormal];
    [btn_register setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_register.frame = CGRectMake(15, self.txt_reigister_sure_pwd.bottom + 18, self.viewRegister.width - 30, 49);
    btn_register.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_register.layer.cornerRadius = 20.0;
    btn_register.layer.masksToBounds = YES;
    [self.viewRegister addSubview:btn_register];
}
#pragma 登录
- (void)btn_loginClick{
    [YQHttpRequest get:nil url:@"http://192.168.31.219:8062/user" successed:^(BOOL succ, id responseDic) {
        NSDictionary *dic = responseDic;
    } fail:^(NSError *error) {
        
    }];
}
@end
