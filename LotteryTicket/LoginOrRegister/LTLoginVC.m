//
//  LTLoginVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/19.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "LTLoginVC.h"
#import "LTRegisterVC.h"
#import "LTForgetPwdVC.h"
#import "RandomCodeView.h"

@interface LTLoginVC ()<UITextFieldDelegate>

@property (nonatomic,strong)UITextField *txt_user_num;// 账户
@property (nonatomic,strong)UITextField *txt_user_pwd;// 密码
@property (nonatomic,strong)UITextField *txt_user_code;// 验证码

@property (nonatomic,strong)UILabel *lab_user_num_line;// 账户---下划线
@property (nonatomic,strong)UILabel *lab_user_pwd_line;// 密码---下划线
@property (nonatomic,strong)UILabel *lab_user_code_line;// 验证码---下划线

@property (nonatomic, strong) RandomCodeView *randomView;
@end

@implementation LTLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"登录";
    
    [self initWithFrame];
}

- (void)initWithFrame{
    //header-bg
    UIImageView *img_header_bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 75)];
    img_header_bg.image = [UIImage imageNamed:@"icon_login_header_bg"];
    img_header_bg.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:img_header_bg];
    
    UIImageView *img_logo = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 80)/2,14, 80, 83)];
    img_logo.image = [UIImage imageNamed:@"icon_login_user_logo"];
    img_logo.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:img_logo];
    if (isIphoneX == 1 || IPHONE_6P) {
        img_header_bg.frame = CGRectMake(0, 0, SCREEN_WIDTH, 115);
        img_logo.frame = CGRectMake((SCREEN_WIDTH - 100)/2, 43, 100, 100);
    }
    self.txt_user_num = [[UITextField alloc]initWithFrame:CGRectMake(26, img_logo.bottom + 10, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_num.placeholder = @"请输入账号";
    self.txt_user_num.textColor = NINE_COLOR;
    self.txt_user_num.font = MFont(15);
    NSAttributedString *attrString_user_num = [[NSAttributedString alloc] initWithString:@"请输入账号" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_num.font}];
        self.txt_user_num.attributedPlaceholder = attrString_user_num;
    [self.view addSubview:self.txt_user_num];
    //TODO:账户名称
    UIView *view_user_num = [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_num" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_num.leftView = view_user_num;
    self.txt_user_num.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_num_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_num.mj_x, self.txt_user_num.bottom, self.txt_user_num.width, 1)];
    self.lab_user_num_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_num_line];
    
    self.txt_user_pwd = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_num_line.bottom, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_pwd.placeholder = @"请输入密码";
    self.txt_user_pwd.textColor = NINE_COLOR;
    self.txt_user_pwd.font = MFont(15);
    NSAttributedString *attrString_user_pwd = [[NSAttributedString alloc] initWithString:@"请输入密码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_pwd.font}];
           self.txt_user_pwd.attributedPlaceholder = attrString_user_pwd;
    [self.view addSubview:self.txt_user_pwd];
    //TODO:密码
      UIView *view_user_pwd = [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_pwd" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_pwd.leftView = view_user_pwd;
    self.txt_user_pwd.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_pwd_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_pwd.mj_x, self.txt_user_pwd.bottom, self.txt_user_pwd.width, 1)];
    self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_pwd_line];
    
    self.txt_user_code = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_pwd_line.bottom, SCREEN_WIDTH - 130, 50)];
    self.txt_user_code.placeholder = @"请输入验证码";
    self.txt_user_code.textColor = NINE_COLOR;
    self.txt_user_code.font = MFont(15);
    NSAttributedString *attrString_user_code = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_code.font}];
             self.txt_user_code.attributedPlaceholder = attrString_user_code;
    self.txt_user_code.keyboardType = UIKeyboardTypeASCIICapable;
    self.txt_user_code.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txt_user_code.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [self.view addSubview:self.txt_user_code];
    //随机生成的图形验证码
    self.randomView = [[RandomCodeView alloc]initWithFrame:CGRectMake(self.txt_user_code.right + 2, self.lab_user_pwd_line.bottom + 7.5, 80, 35)];
    self.randomView.layer.borderColor = UIColorHex(AA6500).CGColor;
    self.randomView.layer.borderWidth = 2.0;
    self.randomView.layer.cornerRadius = 3.0;
    self.randomView.layer.masksToBounds = YES;
    [self.view addSubview:self.randomView];
    //TODO:验证码
      UIView *view_user_code = [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_code" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_code.leftView = view_user_code;
    self.txt_user_code.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_code_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_code.mj_x, self.txt_user_code.bottom, self.txt_user_pwd.width, 1)];
    self.lab_user_code_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_code_line];
    
    UIButton *btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_login setBackgroundImage:[UIImage imageNamed:@"icon_login_btn_bg"] forState:UIControlStateNormal];
    [btn_login setTitle:@"登录" forState:UIControlStateNormal];
    [btn_login setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    btn_login.frame = CGRectMake(26, self.lab_user_code_line.bottom + 19, SCREEN_WIDTH - 52, 49);
    [btn_login addTarget:self action:@selector(btnLoginClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_login];
    
    UIButton *btn_forget_pwd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_forget_pwd setTitle:@"忘记密码" forState:UIControlStateNormal];
    [btn_forget_pwd setTitleColor:NINE_COLOR forState:UIControlStateNormal];
    btn_forget_pwd.titleLabel.font = MFont(15);
    btn_forget_pwd.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn_forget_pwd.frame = CGRectMake(26, btn_login.bottom + 10, 100, 20);
    [btn_forget_pwd addTarget:self action:@selector(btnForgetPwdClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_forget_pwd];
    
    CGFloat tabHt = TabBarHeight;
    CGFloat navHt = NaviBarHeight;
    //其他btn_forget_pwd.bottom + 50
    YYLabel *lab_other = [[YYLabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 31)/2,SCREEN_HEIGHT -  tabHt - 110 - navHt, 31, 16)];
    lab_other.text = @"其他";
    lab_other.textColor = NINE_COLOR;
    lab_other.font = MFont(15);
    [self.view addSubview:lab_other];
    
    YYLabel *lab_left_line = [[YYLabel alloc]initWithFrame:CGRectMake(0, lab_other.top + 11,(SCREEN_WIDTH - 56)/2, 1)];
    lab_left_line.backgroundColor = NINE_COLOR;
    [self.view addSubview:lab_left_line];
    
    YYLabel *lab_right_line = [[YYLabel alloc]initWithFrame:CGRectMake(lab_other.right + 13, lab_other.top + 11,(SCREEN_WIDTH - 56)/2, 1)];
    lab_right_line.backgroundColor = NINE_COLOR;
    [self.view addSubview:lab_right_line];
    
    //马上注册
    UIButton *btn_register = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_register.backgroundColor = UIColorHex(FFB600);
    [btn_register setImage:[UIImage imageNamed:@"icon_login_user_register"] forState:UIControlStateNormal];
    btn_register.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btn_register.frame = CGRectMake((SCREEN_WIDTH - 67)/2 - 70, lab_other.bottom + 20,70, 70);
    btn_register.layer.cornerRadius = 35.0;
    btn_register.layer.masksToBounds = YES;
    [btn_register addTarget:self action:@selector(btnRegisterClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_register];
    
    YYLabel *lab_register = [[YYLabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 67)/2 - 70, btn_register.bottom + 14, 70, 16)];
    lab_register.text = @"马上注册";
    lab_register.textColor = SIX_COLOR;
    lab_register.font = MFont(15);
    lab_register.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lab_register];
    
    UIButton *btn_demo = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_demo.backgroundColor = UIColorHex(4B93F3);
    [btn_demo setImage:[UIImage imageNamed:@"icon_login_demo"] forState:UIControlStateNormal];
    btn_demo.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btn_demo.frame = CGRectMake(btn_register.right + 67, lab_other.bottom + 20,70, 70);
    btn_demo.layer.cornerRadius = 35.0;
    btn_demo.layer.masksToBounds = YES;
    [self.view addSubview:btn_demo];
    
    YYLabel *lab_demo= [[YYLabel alloc]initWithFrame:CGRectMake(btn_register.right + 67, btn_register.bottom + 14, 70, 16)];
    lab_demo.text = @"免费试玩";
    lab_demo.textColor = SIX_COLOR;
    lab_demo.font = MFont(15);
    lab_demo.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lab_demo];
    
    self.txt_user_num.delegate = self;
    self.txt_user_pwd.delegate = self;
    self.txt_user_code.delegate = self;
}
//改变下划线
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.txt_user_num) {
        self.lab_user_num_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_code_line.backgroundColor = CCC_COLOR;
    } else if (textField == self.txt_user_pwd) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_code_line.backgroundColor = CCC_COLOR;
    }else if (textField == self.txt_user_code) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_code_line.backgroundColor = TWO_NINW_A_7_FF;
    }
    return YES;
}
#pragma 马上注册
- (void)btnRegisterClick{
    LTRegisterVC *vc =[LTRegisterVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma 忘记密码
- (void)btnForgetPwdClick{
    LTForgetPwdVC *vc =[LTForgetPwdVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma 登录
- (void)btnLoginClick{
//    self.randomView.changeString如果想同就是正确的
    
//    if ([str isEqualToString:self..changeString]) {
//        [self.getPhoneCodeBtn setEnabled:YES];
//    }else{
//        [self shakeAction];
//        [self.getPhoneCodeBtn setEnabled:NO];
//    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
