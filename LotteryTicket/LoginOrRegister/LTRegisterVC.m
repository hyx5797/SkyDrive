//
//  LTRegisterVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/19.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "LTRegisterVC.h"

@interface LTRegisterVC ()<UITextFieldDelegate>

@property (nonatomic,strong)UITextField *txt_user_num;// 账户
@property (nonatomic,strong)UITextField *txt_user_pwd;// 密码
@property (nonatomic,strong)UITextField *txt_user_pwd_two;// 再次输入密码

@property (nonatomic,strong)UILabel *lab_user_num_line;// 账户---下划线
@property (nonatomic,strong)UILabel *lab_user_pwd_line;// 密码---下划线
@property (nonatomic,strong)UILabel *lab_user_pwd_two_line;// 再次输入密码---下划线

@end

@implementation LTRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWithFrame];
}

- (void)initWithFrame{

    self.txt_user_num = [[UITextField alloc]initWithFrame:CGRectMake(26, 30, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_num.placeholder = @"请输入账号";
    self.txt_user_num.textColor = NINE_COLOR;
    self.txt_user_num.font = MFont(15);
    NSAttributedString *attrString_user_num = [[NSAttributedString alloc] initWithString:@"请输入账号" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_num.font}];
                self.txt_user_num.attributedPlaceholder = attrString_user_num;
    [self.view addSubview:self.txt_user_num];
    //TODO:账户名称
      UIView *view_user_num= [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_num" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_num.leftView = view_user_num;
    self.txt_user_num.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_num_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_num.mj_x, self.txt_user_num.bottom, self.txt_user_num.width, 1)];
    self.lab_user_num_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_num_line];
    
    self.txt_user_pwd = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_num_line.bottom, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_pwd.placeholder = @"请输入密码";
    self.txt_user_pwd.textColor = NINE_COLOR;
    self.txt_user_pwd.font = MFont(15);
    NSAttributedString *attrString_user_pwd = [[NSAttributedString alloc] initWithString:@"请输入密码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_num.font}];
    self.txt_user_pwd.attributedPlaceholder = attrString_user_pwd;
    self.txt_user_pwd.secureTextEntry = YES;
    [self.view addSubview:self.txt_user_pwd];
    //TODO:密码
     UIView *view_user_pwd = [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_pwd" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_pwd.leftView = view_user_pwd;
    self.txt_user_pwd.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_pwd_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_pwd.mj_x, self.txt_user_pwd.bottom, self.txt_user_pwd.width, 1)];
    self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_pwd_line];
    
    self.txt_user_pwd_two = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_pwd_line.bottom, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_pwd_two.placeholder = @"确认密码";
    self.txt_user_pwd_two.textColor = NINE_COLOR;
    self.txt_user_pwd_two.font = MFont(15);
    NSAttributedString *attrString_user_pwd_two = [[NSAttributedString alloc] initWithString:@"确认密码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_pwd_two.font}];
       self.txt_user_pwd_two.attributedPlaceholder = attrString_user_pwd_two;
    self.txt_user_pwd_two.secureTextEntry = YES;
    [self.view addSubview:self.txt_user_pwd_two];
    //TODO:第二遍密码
     UIView *view_user_pwd_two = [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_pwd" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_pwd_two.leftView = view_user_pwd_two;
    self.txt_user_pwd_two.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_pwd_two_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_pwd_two.mj_x, self.txt_user_pwd_two.bottom, self.txt_user_pwd_two.width, 1)];
    self.lab_user_pwd_two_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_pwd_two_line];
    
    
    UIButton *btn_goForword = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_goForword setBackgroundImage:[UIImage imageNamed:@"icon_login_btn_bg"] forState:UIControlStateNormal];
    [btn_goForword setTitle:@"下一步" forState:UIControlStateNormal];
    [btn_goForword setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    btn_goForword.frame = CGRectMake(26, self.lab_user_pwd_two_line.bottom + 19, SCREEN_WIDTH - 52, 49);
    [self.view addSubview:btn_goForword];
    
    UILabel *lab_detail = [[UILabel alloc]initWithFrame:CGRectMake(26, btn_goForword.bottom + 15,  SCREEN_WIDTH - 52, 16)];
    lab_detail.font = MFont(15);
    lab_detail.textColor = SIX_COLOR;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"已有账号，请 登录"];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorHex(3984E8)} range:NSMakeRange(attributedString.length - 2, 2)];
    lab_detail.attributedText = attributedString;
    lab_detail.textAlignment = NSTextAlignmentCenter;
    lab_detail.userInteractionEnabled = YES;
    [self.view addSubview:lab_detail];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lab_detailClick)];
    [lab_detail addGestureRecognizer:tap];
    
//    img_register_lottery
    UIImageView *img_bottom_logo = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 168)/2, lab_detail.bottom + 82, 168, 117)];
    img_bottom_logo.image = [UIImage imageNamed:@"img_register_lottery"];
    [self.view addSubview:img_bottom_logo];
    
    self.txt_user_num.delegate = self;
    self.txt_user_pwd.delegate = self;
    self.txt_user_pwd_two.delegate = self;
    
}
#pragma 前往登录
- (void)lab_detailClick{
    [self.navigationController popViewControllerAnimated:YES];
}
//改变下划线
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.txt_user_num) {
        self.lab_user_num_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_two_line.backgroundColor = CCC_COLOR;
    } else if (textField == self.txt_user_pwd) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_pwd_two_line.backgroundColor = CCC_COLOR;
    }else if (textField == self.txt_user_pwd_two) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_two_line.backgroundColor = TWO_NINW_A_7_FF;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
