//
//  LTForgetPwdVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/19.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "LTForgetPwdVC.h"

@interface LTForgetPwdVC ()<UITextFieldDelegate>

@property (nonatomic,strong)UITextField *txt_user_num;// 账户
@property (nonatomic,strong)UITextField *txt_user_pwd;// 密码
@property (nonatomic,strong)UITextField *txt_user_code;// 验证码
@property (nonatomic,strong)UIButton *btnVerificationCode;//获取验证码

@property (nonatomic,strong)UILabel *lab_user_num_line;// 账户---下划线
@property (nonatomic,strong)UILabel *lab_user_pwd_line;// 密码---下划线
@property (nonatomic,strong)UILabel *lab_user_code_line;// 验证码---下划线
@end

@implementation LTForgetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"忘记密码";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWithFrame];
}

- (void)initWithFrame{
    
    self.txt_user_num = [[UITextField alloc]initWithFrame:CGRectMake(26, 30, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_num.placeholder = @"请输入账号";
    self.txt_user_num.textColor = NINE_COLOR;
    self.txt_user_num.font = MFont(15);
    NSAttributedString *attrString_user_num = [[NSAttributedString alloc] initWithString:@"请输入账号" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_num.font}];
         self.txt_user_num.attributedPlaceholder = attrString_user_num;
    [self.view addSubview:self.txt_user_num];
    //TODO:账户名称
     UIView *view_user_num= [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_num" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_num.leftView = view_user_num;
    self.txt_user_num.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_num_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_num.mj_x, self.txt_user_num.bottom, self.txt_user_num.width, 1)];
    self.lab_user_num_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_num_line];
    
    self.txt_user_code = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_num_line.bottom, SCREEN_WIDTH - 52-90, 50)];
//    self.txt_user_code.placeholder = @"请输入验证码";
    self.txt_user_code.textColor = NINE_COLOR;
    self.txt_user_code.font = MFont(15);
    NSAttributedString *attrString_user_code = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_code.font}];
            self.txt_user_code.attributedPlaceholder = attrString_user_code;
    [self.view addSubview:self.txt_user_code];
    //TODO:密码
     UIView *view_user_pwd= [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_code" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_code.leftView = view_user_pwd;
    self.txt_user_code.leftViewMode = UITextFieldViewModeAlways;
    
    self.btnVerificationCode = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnVerificationCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.btnVerificationCode setTitleColor:UIColorHex(0090FF) forState:UIControlStateNormal];
    self.btnVerificationCode.titleLabel.font = MFont(15);
    self.btnVerificationCode.frame = CGRectMake(self.txt_user_code.right,  self.txt_user_num.bottom, 90, 50);
    [self.view addSubview:self.btnVerificationCode];
    //下划线
    self.lab_user_code_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_num.mj_x, self.txt_user_code.bottom, self.txt_user_num.width, 1)];
    self.lab_user_code_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_code_line];
    
    self.txt_user_pwd = [[UITextField alloc]initWithFrame:CGRectMake(26, self.lab_user_code_line.bottom, SCREEN_WIDTH - 52, 50)];
//    self.txt_user_pwd.placeholder = @"重置密码";
    self.txt_user_pwd.textColor = NINE_COLOR;
    self.txt_user_pwd.font = MFont(15);
    NSAttributedString *attrString_user_pwd = [[NSAttributedString alloc] initWithString:@"重置密码" attributes: @{NSForegroundColorAttributeName:CCC_COLOR,NSFontAttributeName:self.txt_user_pwd.font}];
    self.txt_user_pwd.attributedPlaceholder = attrString_user_pwd;
    [self.view addSubview:self.txt_user_pwd];
    //TODO:第二遍密码
       UIView *view_user_pwd_two= [XYTTools addSuperView:CGRectMake(0, 0, 49, 26) addImageName:@"icon_login_user_pwd" addImgFrame:CGRectMake(14, 0, 21, 26)];
    self.txt_user_pwd.leftView = view_user_pwd_two;
    self.txt_user_pwd.leftViewMode = UITextFieldViewModeAlways;
    //下划线
    self.lab_user_pwd_line = [[UILabel alloc]initWithFrame:CGRectMake(self.txt_user_pwd.mj_x, self.txt_user_pwd.bottom, self.txt_user_pwd.width, 1)];
    self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
    [self.view addSubview:self.lab_user_pwd_line];
    
    UIButton *btn_sure = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_sure setBackgroundImage:[UIImage imageNamed:@"icon_login_btn_bg"] forState:UIControlStateNormal];
    [btn_sure setTitle:@"确定" forState:UIControlStateNormal];
    [btn_sure setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    btn_sure.frame = CGRectMake(26, self.lab_user_pwd_line.bottom + 19, SCREEN_WIDTH - 52, 49);
    [self.view addSubview:btn_sure];
    
    
    self.txt_user_num.delegate = self;
    self.txt_user_code.delegate = self;
    self.txt_user_pwd.delegate = self;
}
//改变下划线
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.txt_user_num) {
        self.lab_user_num_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_code_line.backgroundColor = CCC_COLOR;
    } else if (textField == self.txt_user_pwd) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = TWO_NINW_A_7_FF;
        self.lab_user_code_line.backgroundColor = CCC_COLOR;
    }else if (textField == self.txt_user_code) {
        self.lab_user_num_line.backgroundColor = CCC_COLOR;
        self.lab_user_pwd_line.backgroundColor = CCC_COLOR;
        self.lab_user_code_line.backgroundColor = TWO_NINW_A_7_FF;
    }
    return YES;
}
@end
