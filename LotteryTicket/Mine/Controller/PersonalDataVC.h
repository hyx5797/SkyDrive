//
//  PersonalDataVC.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalDataVC : XYTBaseViewController

@end

//cell
@interface PersonalDataVCell : UITableViewCell
@property (nonatomic,strong)UIImageView *img_icon;
@property (nonatomic,strong)UILabel *lab_title;
@property (nonatomic,strong)UILabel *lab_detail;
@end

//头像的cell
@interface PersonalData_ImgVCell : UITableViewCell

@property (nonatomic,strong)UIImageView *img_icon;
@property (nonatomic,strong)UILabel *lab_title;
@property (nonatomic,strong)UIImageView *img_photo;

@end
NS_ASSUME_NONNULL_END
