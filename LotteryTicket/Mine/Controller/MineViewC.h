//
//  MineViewC.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/7/30.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineViewC : XYTBaseViewController

@end

//cell
@interface MineViewTableVCell : UITableViewCell

@property (nonatomic,strong)UIImageView *img_icon;
@property (nonatomic,strong)UILabel *lab_cell_title;

@end
NS_ASSUME_NONNULL_END
