//
//  MyBankCardVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "MyBankCardVC.h"
#import "AddBankVC.h"
@interface MyBankCardVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView * tableView;

@end

@implementation MyBankCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的银行卡";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    //导航栏右边的按钮
    UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(navbarRightItemClick)];
    self.navigationItem.rightBarButtonItem = right_Item;
}
#pragma 导航栏事件
- (void)navbarRightItemClick{
    [self tabHiddenOrShow];
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
//        CGFloat tabH = TabBarHeight;
        CGFloat ht = SCREEN_HEIGHT-34 ;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 130.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableFooterView:[self createFooterViewFrame]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"MyBankCardTableVCell";
    
    //从tableView的一个队列里获取一个cell
    MyBankCardTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[MyBankCardTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma 表格的尾部视图
- (UIView *)createFooterViewFrame{
    UIView *view_footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    view_footer.backgroundColor = [UIColor whiteColor];
    //内容视图
    UIView *view_content = [[UIView alloc]initWithFrame:CGRectMake(11, 20, SCREEN_WIDTH - 22, 55)];
    view_content.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view_content.layer.shadowColor = [UIColor colorWithRed:28/255.0 green:97/255.0 blue:201/255.0 alpha:0.2].CGColor;
    view_content.layer.shadowOffset = CGSizeMake(0,2);
    view_content.layer.shadowOpacity = 1;
    view_content.layer.shadowRadius = 10;
    view_content.layer.cornerRadius = 6;
    [view_footer addSubview:view_content];
    
//    十字架的背景边
    UIView *view_bg_border = [[UIView alloc]initWithFrame:CGRectMake(18, 11, 34, 34)];
    view_bg_border.layer.borderWidth = 1.0;
    view_bg_border.layer.borderColor = UIColorHex(F0F0F0).CGColor;
    [view_content addSubview:view_bg_border];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAddBankClick)];
    [view_content addGestureRecognizer:tap];
    
    UIImageView *img_icon = [[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 19, 19)];
    img_icon.image = [UIImage imageNamed:@"icon_mine_bank_jia"];
    img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [view_bg_border addSubview:img_icon];
    
    UILabel *lab_title = [[UILabel alloc]initWithFrame:CGRectMake(view_bg_border.right + 9, 11, view_content.width - view_bg_border.right - 20, 17)];
    lab_title.text = @"添加一张银行卡";
    lab_title.textColor = UIColorHex(333333);
    lab_title.font = RFont(18);
    [view_content addSubview:lab_title];
    
    UILabel *lab_Detail = [[UILabel alloc]initWithFrame:CGRectMake(view_bg_border.right + 9, lab_title.bottom + 5, view_content.width - view_bg_border.right - 20, 12)];
    lab_Detail.text = @"储存卡仅支持一张银行卡绑定";
    lab_Detail.textColor = NINE_COLOR;
    lab_Detail.font = RFont(12);
    [view_content addSubview:lab_Detail];
    
    return view_footer;
}

#pragma 添加银行卡
- (void)tapAddBankClick{
    AddBankVC *vc = [AddBankVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end

//TODO:cell
@implementation MyBankCardTableVCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.contentView.backgroundColor = [UIColor whiteColor];
    UIImageView *img_bg = [[UIImageView alloc]initWithFrame:CGRectMake(11, 12, SCREEN_WIDTH - 22, 118)];
    img_bg.backgroundColor = RANDOMCOLOR;
    [self.contentView addSubview:img_bg];
    
    UIImageView *img_bank_icon = [[UIImageView alloc]initWithFrame:CGRectMake(17, 21, 30, 30)];
    img_bank_icon.backgroundColor = RANDOMCOLOR;
    img_bank_icon.layer.cornerRadius = 15.0;
    img_bank_icon.layer.masksToBounds = YES;
    [img_bg addSubview:img_bank_icon];
    
    UILabel *lab_bank_name = [[UILabel alloc]initWithFrame:CGRectMake(img_bank_icon.right + 11, 20, img_bg.width - img_bank_icon.right - 61, 17)];
    lab_bank_name.text = @"中国工商银行";
    lab_bank_name.textColor = UIColorHex(ffffff);
    lab_bank_name.font = RFont(18);
    [img_bg addSubview:lab_bank_name];
    
    UILabel *lab_bank_type = [[UILabel alloc]initWithFrame:CGRectMake(img_bank_icon.right + 11, lab_bank_name.bottom +4, img_bg.width - img_bank_icon.right - 61, 12)];
    lab_bank_type.text = @"储蓄卡";
    lab_bank_type.textColor = UIColorHex(E6E6E6);
    lab_bank_type.font = RFont(12);
    [img_bg addSubview:lab_bank_type];
    
    YYLabel *lab_bank_num = [[YYLabel alloc]initWithFrame:CGRectMake(img_bank_icon.right + 11, img_bank_icon.bottom +22, img_bg.width - img_bank_icon.right - 61, 12)];
    lab_bank_num.text = @"*** **** **** 8768";
    lab_bank_num.textColor = UIColorHex(E6E6E6);
    lab_bank_num.textVerticalAlignment = 1;
    lab_bank_num.font = RFont(12);
    [img_bg addSubview:lab_bank_num];
    
    UIButton *btn_choose = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_choose.backgroundColor = RANDOMCOLOR;
    btn_choose.frame = CGRectMake(img_bg.width - 43, 16, 15, 15);
    [img_bg addSubview:btn_choose];
    
    return self;
}

@end
