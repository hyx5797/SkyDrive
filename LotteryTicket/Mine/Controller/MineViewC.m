//
//  MineViewC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/7/30.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "MineViewC.h"
#import "LoginViewC.h"
#import "LTLoginVC.h"
#import "LTButton.h"
#import <QuartzCore/QuartzCore.h>
#import "FundManagementVC.h"
#import "PersonalDataVC.h"
#import "ChangePwdVC.h"
#import "MyBankCardVC.h"
#import "NoteRecordVC.h"
@interface MineViewC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;

@property (nonatomic,strong)UIImageView *img_photo;//用户头像
@property (nonatomic,strong)UILabel *lab_username;//用户昵称
@property (nonatomic,strong)UILabel *lab_email;//用户邮箱
@property (nonatomic,strong)UILabel *lab_account_yue;//账户余额
@property (nonatomic,strong)NSArray *array_img;
@property (nonatomic,strong)NSArray *array_str;
@end

@implementation MineViewC
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
//}
//
//- (void)viewDidDisappear:(BOOL)animated {
//    [super viewDidDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITE_COLOR;
    self.title = @"我的";
    self.array_img = @[@"icon_mine_personaldata",@"icon_mine_changepassword",@"icon_mine_bankaccount",@"icon_mine_theme_skin"];
    self.array_str=@[@"个人资料",@"修改密码",@"银行账户",@"更换主题"];
    [self.view addSubview:self.tableView];
    //导航栏右边的按钮
       UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
       UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(btn_right_mine_click)];
       self.navigationItem.rightBarButtonItem = right_Item;
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
                CGFloat tabH = TabBarHeight;
//        CGFloat stauHt = StatusBarHeight;
        CGFloat ht = SCREEN_HEIGHT - tabH;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 47.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableHeaderView:[self createHeaderViewFame]];
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.array_img count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"MineViewTableVCell";
    
    //从tableView的一个队列里获取一个cell
    MineViewTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[MineViewTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.img_icon.image = [UIImage imageNamed:self.array_img[indexPath.row]];
    cell.lab_cell_title.text = self.array_str[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
            PersonalDataVC *vc = [PersonalDataVC new];
            [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
            ChangePwdVC *vc = [ChangePwdVC new];
            [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 2){
        MyBankCardVC *vc = [MyBankCardVC new];
         [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
        
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.tableView.contentOffset.y <= 0) {
        self.tableView.bounces = NO;
//        NSLog(@"禁止下拉");
    }
}
#pragma  创建头部视图
- (UIView *)createHeaderViewFame{
//      CGFloat stauHt = StatusBarHeight;
//    CGFloat navHt = NaviBarHeight;
    UIView *header_bg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 240)];
    header_bg.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:header_bg];
    //上面的蓝色部分
    UIImageView *img_bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, header_bg.width, 163)];
    img_bg.image = [UIImage imageNamed:@"img_mine_header_bg"];
    img_bg.contentMode = UIViewContentModeScaleAspectFill;
    img_bg.backgroundColor = STATUS_BAR_BGCOLOR;
    img_bg.userInteractionEnabled = YES;
    [header_bg addSubview:img_bg];
//    //标题
//    UILabel *lab_nav_title = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100)/2, stauHt,100 , 44)];
//    lab_nav_title.text = @"我的";
//    lab_nav_title.textColor = [UIColor whiteColor];
//    lab_nav_title.font = MFont(19);
//    lab_nav_title.textAlignment = NSTextAlignmentCenter;
//    [img_bg addSubview:lab_nav_title];
//    //有侧边栏
//    UIButton * btn_right = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_right.frame = CGRectMake(img_bg.width - 44-15, stauHt, 44, 44);
//    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateNormal];
//    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateSelected];
//    btn_right.contentHorizontalAlignment =UIControlContentHorizontalAlignmentRight;
//    btn_right.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    [btn_right addTarget:self action:@selector(btn_right_mine_click) forControlEvents:UIControlEventTouchUpInside];
//    [img_bg addSubview:btn_right];
    
    //内容
    UIView *view_content = [[UIView alloc]initWithFrame:CGRectMake(15, 66, header_bg.width - 30, 176)];
    view_content.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view_content.layer.shadowColor = [UIColorHex(001626) colorWithAlphaComponent:0.08].CGColor;
    view_content.layer.shadowOffset = CGSizeMake(0,0);
    view_content.layer.shadowOpacity = 1;
    view_content.layer.shadowRadius = 7;
    view_content.layer.cornerRadius = 12.5;
    [header_bg addSubview:view_content];
    
    UIImageView *img_photo = [[UIImageView alloc]initWithFrame:CGRectMake((header_bg.width - 86)/2, 19, 79, 79)];
    img_photo.backgroundColor = RANDOMCOLOR;
    view_content.layer.shadowColor = [UIColorHex(007EFF) colorWithAlphaComponent:0.4].CGColor;
    view_content.layer.shadowOffset = CGSizeMake(0,0);
    view_content.layer.shadowOpacity = 1;
    view_content.layer.shadowRadius = 12;
    img_photo.layer.cornerRadius = 43.0;
    img_photo.layer.masksToBounds = YES;
    [header_bg addSubview:img_photo];
    self.img_photo = img_photo;
    
    self.lab_username = [[UILabel alloc]initWithFrame:CGRectMake(0, 52.0, view_content.width, 17)];
    self.lab_username.text = @"哈哈哈哈";
    self.lab_username.font = MFont(17);
    self.lab_username.textColor = SIX_COLOR;
    self.lab_username.textAlignment = NSTextAlignmentCenter;
    [view_content addSubview:self.lab_username];

    self.lab_email = [[UILabel alloc]initWithFrame:CGRectMake(0,self.lab_username.bottom + 9, view_content.width, 15)];
    self.lab_email.text = @"余额 : 293.89";
    self.lab_email.font = MFont(14);
    self.lab_email.textColor = SIX_COLOR;
    self.lab_email.textAlignment = NSTextAlignmentCenter;
    [view_content addSubview:self.lab_email];
    
    NSArray *btnArray = @[@"资金明细",@"下注记录",@"优惠记录",@"我的消息"];
    NSArray *btn_imgArray = @[@"icon_mine_detailsoffunds",@"icon_mine_noterecord",@"icon_mine_preferentialrecord",@"icon_mine_mynews"];
    for(int a = 0; a < btnArray.count; a++){
        LTButton *btn = [LTButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(a*(view_content.width/4),  self.lab_email.bottom + 17, view_content.width/4, 64);
        btn.tag = a;
        [btn setTitle:btnArray[a] forState:UIControlStateNormal];
        [btn setTitleColor:THREE_COLOR forState:UIControlStateNormal];
        btn.titleLabel.font = RFont(15);
        [btn setImage:[UIImage imageNamed:btn_imgArray[a]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_mine_FourBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view_content addSubview:btn];
    }
    
    return header_bg;
}
//侧边栏
- (void)btn_right_mine_click{
    [self tabHiddenOrShow];
}

#pragma 资金明细那一系列
- (void)btn_mine_FourBtnClick:(UIButton *)btn{
    if (btn.tag ==0) {
        //资金明细
        self.tabBarController.selectedIndex = 2;
    }else if(btn.tag ==1){
        NoteRecordVC *vc = [NoteRecordVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end

//Cell
@implementation MineViewTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      self.contentView.backgroundColor = [UIColor whiteColor];
    [self  initWithCellFrame];
    return self;
}
- (void)initWithCellFrame{
    
    self.img_icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 21, 24, 24)];
    self.img_icon.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.contentView addSubview:self.img_icon];
    
    self.lab_cell_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right+20, 19, 120, 14)];
    self.lab_cell_title.textColor = THREE_COLOR;
    self.lab_cell_title.font = RFont(15);
    self.img_icon.centerY = self.lab_cell_title.centerY;
    [self.contentView addSubview:self.lab_cell_title];
    //箭头
    UIImageView *img_more = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 24, 15, 9, 16)];
    img_more.image = [UIImage imageNamed:@"icon_mine_more"];
      img_more.centerY = self.lab_cell_title.centerY;
    [self.contentView addSubview:img_more];
    
    UILabel *lab_line = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right+21, 51, SCREEN_WIDTH - self.img_icon.right-21, 1)];
    lab_line.backgroundColor = UIColorHex(E6E6E6);
    [self.contentView addSubview:lab_line];
}
@end


