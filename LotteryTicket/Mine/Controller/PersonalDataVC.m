//
//  PersonalDataVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "PersonalDataVC.h"

@interface PersonalDataVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSMutableArray *data_array;
@property (nonatomic,strong)NSMutableArray *data_Detail_array;
@property (nonatomic,strong)NSMutableArray *data_img_array;
@end

@implementation PersonalDataVC
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
//}
//
//- (void)viewDidDisappear:(BOOL)animated {
//    [super viewDidDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人资料";
    self.view.backgroundColor = [UIColor whiteColor];
    self.data_array = [[NSMutableArray alloc]initWithObjects:@"账号",@"昵称",@"支付密码",@"头像", nil];
    self.data_Detail_array = [[NSMutableArray alloc]initWithObjects:@"12342131jjjj",@"***楦",@"******",@"", nil];
    self.data_img_array =  [[NSMutableArray alloc]initWithObjects:@"icon_personal_account",@"icon_personal_username",@"icon_personal_paypwd",@"icon_personal_photo", nil];
    [self createPersonalDataHeaderView];
    [self.view addSubview:self.tableView];
    //导航栏右边的按钮
    UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(navbarRightItemClick)];
    self.navigationItem.rightBarButtonItem = right_Item;
}
#pragma 导航栏事件
- (void)navbarRightItemClick{
    [self tabHiddenOrShow];
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        CGFloat navH = NaviBarHeight;
        CGFloat ht = SCREEN_HEIGHT -106-navH ;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 106, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.data_array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"PersonalData_ImgVCell";
        
        //从tableView的一个队列里获取一个cell
        PersonalData_ImgVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[PersonalData_ImgVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.img_icon.image = [UIImage imageNamed:self.data_img_array[indexPath.row]];
        cell.lab_title.text = self.data_array[indexPath.row];
        return cell;
    }else{
            //给每个cell设置ID号（重复利用时使用）
            static NSString *cellID = @"PersonalDataVCell";
        
            //从tableView的一个队列里获取一个cell
            PersonalDataVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
            //判断队列里面是否有这个cell 没有自己创建，有直接使用
            if (cell == nil) {
                //没有,创建一个
                cell = [[PersonalDataVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.img_icon.image = [UIImage imageNamed:self.data_img_array[indexPath.row]];
            cell.lab_title.text = self.data_array[indexPath.row];
            cell.lab_detail.text = self.data_Detail_array[indexPath.row];
           return cell;
    }
}
#pragma 个人资料头部
- (void)createPersonalDataHeaderView{
//    CGFloat navHt = NaviBarHeight;
//    CGFloat stauHt = StatusBarHeight;
    
    UIImageView *img_Header = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 106)];
    img_Header.image = [UIImage imageNamed:@"img_minedarta_bground"];
    img_Header.userInteractionEnabled = YES;
    [self.view addSubview:img_Header];
//    //返回按钮
//    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    backBtn.frame = CGRectMake(15, stauHt, 44, 44);
//    [backBtn setImage:[UIImage imageNamed:@"icon_login_back"] forState:UIControlStateNormal];
//    [backBtn setImage:[UIImage imageNamed:@"icon_login_back"] forState:UIControlStateSelected];
//    backBtn.contentHorizontalAlignment =UIControlContentHorizontalAlignmentLeft;
//    backBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    [backBtn addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
//    [img_Header addSubview:backBtn];
//    //标题
//    UILabel *lab_nav_title = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100)/2, stauHt,100 , 44)];
//    lab_nav_title.text = @"个人资料";
//    lab_nav_title.textColor = [UIColor whiteColor];
//    lab_nav_title.font = MFont(19);
////    RFont(18);
//    lab_nav_title.textAlignment = NSTextAlignmentCenter;
//    [img_Header addSubview:lab_nav_title];
//    //有侧边栏
//    UIButton * btn_right = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_right.frame = CGRectMake(img_Header.width - 44-15, stauHt, 44, 44);
//    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateNormal];
//    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateSelected];
//    btn_right.contentHorizontalAlignment =UIControlContentHorizontalAlignmentRight;
//    btn_right.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    [btn_right addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
//    [img_Header addSubview:btn_right];

    UIImageView *img_photo = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 48, 48)];
    img_photo.backgroundColor = [UIColor whiteColor];
    img_photo.layer.cornerRadius = 24.0;
    img_photo.layer.masksToBounds = YES;
    [img_Header addSubview:img_photo];
    
    UILabel *lab_username = [[UILabel alloc]initWithFrame:CGRectMake(img_photo.right + 8, img_photo.top + 9, img_Header.width - img_photo.right - 30, 16)];
    lab_username.text = @"上午好，10254655khg";
    lab_username.font = RFont(14);
    lab_username.textColor = [UIColor whiteColor];
    [img_Header addSubview:lab_username];
    
    
    UILabel *lab_email = [[UILabel alloc]initWithFrame:CGRectMake(img_photo.right + 8,lab_username.bottom + 5, img_Header.width - img_photo.right - 30, 15)];
    lab_email.text = @"余额：0.000元";
    lab_email.font = RFont(15);
    lab_email.textColor = [UIColor whiteColor];
    [img_Header addSubview:lab_email];
}
//- (void)onBack{
//    [self.navigationController popViewControllerAnimated:YES];
//}
@end


#pragma Cell
@implementation PersonalDataVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      self.contentView.backgroundColor = [UIColor whiteColor];
    self.img_icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 14, 23, 23)];
    self.img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.img_icon];
    
    self.lab_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right + 10, 18,100, 17)];
    self.lab_title.textColor = SIX_COLOR;
    self.lab_title.font = RFont(15);
    [self.contentView addSubview:self.lab_title];
    
    self.lab_detail = [[UILabel alloc]initWithFrame:CGRectMake(self.lab_title.right + 30, 16,SCREEN_WIDTH - self.lab_title.right - 70, 18)];
    self.lab_detail.textColor = NINE_COLOR;
    self.lab_detail.font = RFont(15);
    self.lab_detail.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.lab_detail];
    
    //箭头
          UIImageView *img_more = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 24, 15, 9, 16)];
          img_more.image = [UIImage imageNamed:@"icon_mine_more"];
            img_more.centerY = self.lab_detail.centerY;
          [self.contentView addSubview:img_more];
    return self;
}
- (void)setImg_icon:(UIImageView *)img_icon{
    _img_icon = img_icon;
}
-(void)setLab_title:(UILabel *)lab_title{
    _lab_title = lab_title;
}
- (void)setLab_detail:(UILabel *)lab_detail{
    _lab_detail = lab_detail;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end

#pragma 头像的cell
@implementation PersonalData_ImgVCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      self.contentView.backgroundColor = [UIColor whiteColor];
    self.img_icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 14, 23, 23)];
    self.img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.img_icon];
    
    self.lab_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right + 10, 18,100, 17)];
    self.lab_title.textColor = SIX_COLOR;
    self.lab_title.font = RFont(15);
    [self.contentView addSubview:self.lab_title];
    
    self.img_photo = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 75, 6.5,37, 37)];
    self.img_photo.backgroundColor = RANDOMCOLOR;
    self.img_photo.layer.cornerRadius = 18.5;
    self.img_photo.layer.masksToBounds = YES;
    [self.contentView addSubview:self.img_photo];
    
    //箭头
       UIImageView *img_more = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 24, 15, 9, 16)];
       img_more.image = [UIImage imageNamed:@"icon_mine_more"];
         img_more.centerY = self.img_photo.centerY;
       [self.contentView addSubview:img_more];
    return self;
}
- (void)setImg_icon:(UIImageView *)img_icon{
    _img_icon = img_icon;
}
-(void)setLab_title:(UILabel *)lab_title{
    _lab_title = lab_title;
}
-(void)setImg_photo:(UIImageView *)img_photo{
    _img_photo = img_photo;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
