//
//  leftViewTbCell.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/4.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//按钮+名称
@interface leftViewTbCell : UITableViewCell

@property (nonatomic,strong)UIImageView *img_to_cell;
@property (nonatomic,strong)UILabel *lab_to_cell_title;

@end


//按钮+名称+金额
@interface leftViewMoreTbCell : UITableViewCell

@property (nonatomic,strong)UIImageView *img_to_cell;
@property (nonatomic,strong)UILabel *lab_to_cell_title;
@property (nonatomic,strong)UILabel *lab_to_cell_money;

@end
NS_ASSUME_NONNULL_END
