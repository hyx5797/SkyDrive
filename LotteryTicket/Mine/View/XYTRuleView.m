//
//  XYTRuleView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/14.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "XYTRuleView.h"

@implementation XYTRuleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = RANDOMCOLOR;
    
    UILabel *labTitle = [[UILabel alloc]initWithFrame:CGRectMake(50, 120, 300, 100)];
    labTitle.backgroundColor = [UIColor whiteColor];
    labTitle.text = @"游戏规则啊";
    labTitle.textColor = [UIColor blackColor];;
    
    [self addSubview:labTitle];
    
    return self;
}

@end
