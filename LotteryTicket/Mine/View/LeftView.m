//
//  LeftView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/7/30.
//  Copyright © 2019 hyx. All rights reserved.
//


#import "LeftView.h"
#import "leftViewTbCell.h"

#define VIEW_WIDTH self.bounds.size.width
#define VIEW_HEIGHT self.bounds.size.height

@interface LeftView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)NSMutableArray *dataSourceAry;
@property (nonatomic,strong)NSMutableArray *dataSourceAry_img;

@end

@implementation LeftView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *array0 = @[@"今日输赢",@"未结算"];
        NSArray *array1 = @[@"今日已结",@"下注记录",@"开奖结果",@"游戏规则"];
        NSArray *array2 = @[@"主题皮肤",@"退出"];
        self.dataSourceAry = [[NSMutableArray alloc]initWithObjects:array0,array1,array2,nil];
        NSArray *img0 = @[@"icon_mine_win_lose",@"icon_mine_unsettlement"];
        NSArray *img1 = @[@"icon_mine_today_closed",@"icon_mine_bet_record",@"icon_mine_lottery_result",@"icon_mine_game_rules"];
        NSArray *img2 = @[@"icon_mine_theme_skin",@"icon_mine_login_out"];
        self.dataSourceAry_img = [[NSMutableArray alloc]initWithObjects:img0,img1,img2,nil];
        [self initWithTableView];
    }
    return self;
}
- (void)initWithTableView{
    CGFloat statusHt = StatusBarHeight;
    UIView *view_status = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, statusHt)];
    view_status.backgroundColor =STATUS_BAR_BGCOLOR;
    [self addSubview:view_status];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, statusHt, VIEW_WIDTH, VIEW_HEIGHT - statusHt) style:UITableViewStylePlain];
    tableView.rowHeight = 52.0;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableView];
    if (@available(iOS 11.0, *)) {
        tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;

    }

}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section ==0) {
        static NSString *cellID = @"leftViewMoreTbCell";
        //从tableView的一个队列里获取一个cell
        leftViewMoreTbCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[leftViewMoreTbCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.img_to_cell.image = [UIImage imageNamed:self.dataSourceAry_img[indexPath.section][indexPath.row]];
        cell.lab_to_cell_title.text = self.dataSourceAry[indexPath.section][indexPath.row];
        cell.lab_to_cell_money.text = @"0.00";
        return cell;
    }else{
        static NSString *cellID = @"leftViewTbCell";
        //从tableView的一个队列里获取一个cell
        leftViewTbCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[leftViewTbCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.image = [UIImage imageNamed:self.dataSourceAry_img[indexPath.section][indexPath.row]];
        cell.lab_to_cell_title.text = self.dataSourceAry[indexPath.section][indexPath.row];
        return cell;
    }
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSourceAry[section] count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSourceAry count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 179;
    }else if (section == 1){
        return 15;
    }else{
        return 1;
    }
//    return section == 0?179:1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView *oneHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 179)];
        oneHeaderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_mine_bground"]];
        
        //头像
        UIImageView *imgUserPhoto = [[UIImageView alloc]initWithFrame:CGRectMake((oneHeaderView.width - 51)/2, 25, 51, 51)];
        imgUserPhoto.layer.cornerRadius = 25.5;
        imgUserPhoto.layer.masksToBounds = YES;
        imgUserPhoto.backgroundColor = [UIColor whiteColor];
        [oneHeaderView addSubview:imgUserPhoto];

        //账户金额
        UILabel *labUserMoney = [[UILabel alloc]initWithFrame:CGRectMake(0, imgUserPhoto.bottom + 10, VIEW_WIDTH , 12)];
        labUserMoney.text = @"￥2300";
        labUserMoney.textColor =[UIColor whiteColor];
        labUserMoney.font =  [UIFont fontWithName:@"PingFang-SC-Bold" size: 14];
        labUserMoney.textAlignment =NSTextAlignmentCenter;
        [oneHeaderView addSubview:labUserMoney];
        
        UIButton *btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn_login setBackgroundImage:[UIImage imageNamed:@"icon_mine_login"] forState:UIControlStateNormal];
        btn_login.frame = CGRectMake(37, labUserMoney.bottom+ 25, 61, 31);
        [oneHeaderView addSubview:btn_login];
        
        UIButton *btn_register = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn_register setBackgroundImage:[UIImage imageNamed:@"icon_mine_registered"] forState:UIControlStateNormal];
        btn_register.frame = CGRectMake(btn_login.right + 27, labUserMoney.bottom+ 25, 61, 31);
        [oneHeaderView addSubview:btn_register];
        
        return oneHeaderView;
    }else if(section ==1){
        UIView *twoHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 15)];
        twoHeaderView.backgroundColor = CCC_COLOR;
        
        UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 14)];
        topView.backgroundColor = WHITE_COLOR;
        [twoHeaderView addSubview:topView];
        
        return twoHeaderView;
    }else{
        UIView *threeHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 1)];
        threeHeaderView.backgroundColor = CCC_COLOR;
        return threeHeaderView;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        if ([self.delegate respondsToSelector:@selector(didClickChildButton:)]) {
            [self.delegate didClickChildButton:self.dataSourceAry[indexPath.section][indexPath.row]];
        }
}

@end

#pragma mark - 自定义tabBar按钮
@implementation TabButton

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, VIEW_HEIGHT * 0.1, VIEW_WIDTH, VIEW_HEIGHT * 0.5);
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.titleLabel.frame = CGRectMake(0, VIEW_HEIGHT * 0.6, VIEW_WIDTH, VIEW_HEIGHT * 0.2);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}

@end

