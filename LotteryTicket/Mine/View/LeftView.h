//
//  LeftView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/7/30.
//  Copyright © 2019 hyx. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol LeftViewDelegate <NSObject>
/**
 点击侧边栏按钮调用方法(必写)

 @param currentTitle 当前标题
 */
@required
- (void)didClickChildButton:(NSString *)currentTitle;

@end

@interface LeftView : UIView

@property(nonatomic, strong) NSArray *itemArray;
@property(nonatomic, strong) id <LeftViewDelegate>delegate;

@end

@interface TabButton : UIButton

@end
