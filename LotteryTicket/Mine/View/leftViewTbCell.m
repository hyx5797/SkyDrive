//
//  leftViewTbCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/4.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "leftViewTbCell.h"

@implementation leftViewTbCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      self.contentView.backgroundColor = [UIColor whiteColor];
    self.img_to_cell = [[UIImageView alloc]initWithFrame:CGRectMake(19, 13, 24, 24)];
    self.img_to_cell.contentMode = UIViewContentModeScaleAspectFit;
//    self.img_to_cell.backgroundColor = RANDOMCOLOR;
    [self.contentView addSubview:self.img_to_cell];
    
    self.lab_to_cell_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_to_cell.right + 15, self.img_to_cell.top + 6, self.width - self.img_to_cell.right - 20, 14)];
    self.lab_to_cell_title.text = @"今日已结";
    self.lab_to_cell_title.textColor = SIX_COLOR;
    self.lab_to_cell_title.font = MFont(14);
    [self.contentView addSubview:self.lab_to_cell_title];
    
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


//按钮+名称+金额
@implementation leftViewMoreTbCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.img_to_cell = [[UIImageView alloc]initWithFrame:CGRectMake(19, 19, 24, 24)];
    self.img_to_cell.contentMode = UIViewContentModeScaleAspectFit;
//    self.img_to_cell.backgroundColor = RANDOMCOLOR;
    [self.contentView addSubview:self.img_to_cell];
    
    self.lab_to_cell_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_to_cell.right + 15,15, self.width - self.img_to_cell.right - 20, 14)];
    self.lab_to_cell_title.text = @"今日已结";
    self.lab_to_cell_title.textColor = SIX_COLOR;
    self.lab_to_cell_title.font = MFont(14);
    [self.contentView addSubview:self.lab_to_cell_title];
    
    self.lab_to_cell_money= [[UILabel alloc]initWithFrame:CGRectMake(self.img_to_cell.right + 15,self.lab_to_cell_title.bottom + 5, self.width - self.img_to_cell.right - 20, 14)];
    self.lab_to_cell_money.text = @"0.00";
    self.lab_to_cell_money.textColor = SIX_COLOR;
    self.lab_to_cell_money.font = MFont(14);
    [self.contentView addSubview:self.lab_to_cell_money];
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
