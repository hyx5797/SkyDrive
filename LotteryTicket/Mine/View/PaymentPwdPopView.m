//
//  PaymentPwdPopView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/17.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "PaymentPwdPopView.h"
#import "JHVerificationCodeView.h"
@interface PaymentPwdPopView()

@property (nonatomic,copy)void (^complate)(void);
@property (nonatomic, strong) UIButton *btnCancle; //返回到第一次输入密码
@property (nonatomic,strong)UILabel *labDetail;//1、请输入支付密码2、请再次输入支付密码
@property (nonatomic,strong)JHVerificationCodeView *view_payPwd_one;
@property (nonatomic,strong)JHVerificationCodeView *view_payPwd_two;
@property (nonatomic,strong)UILabel *lab_worn;//提示密码不一致

@end
@implementation PaymentPwdPopView

+ (instancetype)sharedInstance{
    
    static PaymentPwdPopView * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
    });
    return instance;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        //上半部分添加透明button
        
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        //半透明视图
        self.whiteView = [[UIView alloc] init];
        self.whiteView.backgroundColor =UIColorHex(FAFCFF);
        self.whiteView.frame = CGRectMake(29, (SCREEN_HEIGHT - 200)/2, SCREEN_WIDTH - 58, 200);
        self.whiteView.layer.cornerRadius = 8.0;
        self.whiteView.layer.masksToBounds = YES;
        [self addSubview:self.whiteView];

        //绑定银行卡
        UILabel *lab_title = [[UILabel alloc]initWithFrame:CGRectMake((self.whiteView.width -92)/2, 10, 92, 16)];
        lab_title.text = @"绑定银行卡";
        lab_title.textColor = UIColorHex(444444);
        lab_title.font = RFont(17);
        lab_title.textAlignment = NSTextAlignmentCenter;
        [self.whiteView addSubview:lab_title];
        
        self.btnCancle = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnCancle.backgroundColor = RANDOMCOLOR;
        self.btnCancle.frame = CGRectMake(0, 0, 40, 40);
        self.btnCancle.hidden =YES;
        [self.btnCancle addTarget:self action:@selector(btnCancleBack) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteView addSubview:self.btnCancle];
        
       self.labDetail = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_title.bottom + 27, self.whiteView.width, 16)];
        self.labDetail.text = @"请输入支付密码";//
        self.labDetail.textColor = SIX_COLOR;
        self.labDetail.font = RFont(17);
        self.labDetail.textAlignment = NSTextAlignmentCenter;
        [self.whiteView addSubview:self.labDetail];
        
        JHVCConfig *config     = [[JHVCConfig alloc] init];
        config.inputBoxNumber  = 6;
        config.inputBoxSpacing = 10;
        config.inputBoxWidth   = 35;
        config.inputBoxHeight  = 28;
        config.tintColor       = NINE_COLOR;
        config.secureTextEntry = YES;
        config.inputBoxColor   = [UIColor clearColor];
        config.font            = [UIFont boldSystemFontOfSize:20];
        config.textColor       = NINE_COLOR;
//        config.inputType       = JHVCConfigInputType_Alphabet;
        config.keyboardType = UIKeyboardTypeNumberPad;
        config.inputBoxBorderWidth  = 1;
        config.showUnderLine = YES;
        config.underLineSize = CGSizeMake(35, 1);
        config.underLineColor =NINE_COLOR;
        config.underLineHighlightedColor =NINE_COLOR;
          //第二次
        self.view_payPwd_two =
        [[JHVerificationCodeView alloc] initWithFrame:CGRectMake(25,  self.labDetail .bottom + 30, self.whiteView.width -50, 23) config:config];
         self.view_payPwd_two.finishBlock = ^(NSString *code) {

        };
         self.view_payPwd_two.inputBlock = ^(NSString *code) {
            NSLog(@"example 4 code:%@",code);
        };
        self.view_payPwd_two.hidden = YES;
        [self.whiteView addSubview:self.view_payPwd_two];
        
        self.view_payPwd_one =
        [[JHVerificationCodeView alloc] initWithFrame:CGRectMake(25,  self.labDetail .bottom + 30, self.whiteView.width -50, 23) config:config];
        self.view_payPwd_one.finishBlock = ^(NSString *code) {
            self.btnCancle.hidden = NO;
            self.view_payPwd_one.hidden = YES;
             self.view_payPwd_two.hidden = NO;
            self.labDetail.text = @"请再次输入支付密码";
        };
        self.view_payPwd_one.inputBlock = ^(NSString *code) {
            NSLog(@"example 4 code:%@",code);
        };
        [self.whiteView addSubview:self.view_payPwd_one];
        
        self.lab_worn = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view_payPwd_two.bottom + 14, self.whiteView.width, 13)];
         self.lab_worn.textAlignment = NSTextAlignmentCenter;
        self.lab_worn.text = @"密码与上一次不一致，请重新输入";
        self.lab_worn.textColor = UIColorHex(F10101);
        self.lab_worn.font = LFont(11);
        [self.whiteView addSubview:self.lab_worn];
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btnCloseClick)];
        [self addGestureRecognizer:tap];
    }
    return self;
}
#pragma 返回到第一次输入密码界面
- (void)btnCancleBack{
    self.btnCancle.hidden = YES;
    self.view_payPwd_one.hidden = NO;
     self.view_payPwd_two.hidden = YES;
    self.labDetail.text = @"请输入支付密码";
}
- (void)showWithUnityID:(NSInteger )count  Complate:(void (^)(void))complate{
    if (self.superview) {
        return;
    }

    CGFloat ht = count *45+35 +136;
    self.whiteView.frame = CGRectMake(20, (SCREEN_HEIGHT -  ht)/2, SCREEN_WIDTH - 40, ht) ;

    //
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.whiteView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.whiteView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:nil];
    
}
#pragma mark - 关闭
- (void)btnCloseClick{
    [self dismiss];
}

#pragma mark - 确认打赏回调
- (void)sureBtnClick{
    if (self.complate) {
        self.complate();
    }
    [self dismiss];
}

-(void)dismiss
{
    if (!self.superview) {
        return;
    }
    [UIView animateWithDuration:0.15 animations:^{
        self.whiteView.xmg_y = SCREEN_HEIGHT;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.complate = nil;
    }];
}
@end
