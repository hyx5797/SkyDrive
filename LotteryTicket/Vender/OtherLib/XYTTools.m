//
//  XYTTools.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/13.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "XYTTools.h"
#import <objc/runtime.h>

@implementation XYTTools

+ (NSDictionary*)getObjectData:(id)obj

{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    unsigned int propsCount;
    
    objc_property_t *props = class_copyPropertyList([obj class], &propsCount);
    
    for(int i = 0;i < propsCount; i++)
        
    {
        
        objc_property_t prop = props[i];
        
        
        
        NSString *propName = [NSString stringWithUTF8String:property_getName(prop)];
        
        id value = [obj valueForKey:propName];
        
        if(value == nil)
            
        {
            
            value = [NSNull null];
            
        }
        
        else
            
        {
            
            value = [self getObjectInternal:value];
            
        }
        
        [dic setObject:value forKey:propName];
        
    }
    
    return dic;
    
}




+ (void)print:(id)obj

{
    
    NSLog(@"%@", [self getObjectData:obj]);
    
}



+ (NSData*)getJSON:(id)obj options:(NSJSONWritingOptions)options error:(NSError**)error

{
    
    return [NSJSONSerialization dataWithJSONObject:[self getObjectData:obj] options:options error:error];
    
}

+ (id)getObjectInternal:(id)obj

{
    
    if([obj isKindOfClass:[NSString class]]
       
       || [obj isKindOfClass:[NSNumber class]]
       
       || [obj isKindOfClass:[NSNull class]])
        
    {
        
        return obj;
        
    }
    
    
    
    if([obj isKindOfClass:[NSArray class]])
        
    {
        
        NSArray *objarr = obj;
        
        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:objarr.count];
        
        for(int i = 0;i < objarr.count; i++)
            
        {
            
            [arr setObject:[self getObjectInternal:[objarr objectAtIndex:i]] atIndexedSubscript:i];
            
        }
        
        return arr;
        
    }
    
    
    
    if([obj isKindOfClass:[NSDictionary class]])
        
    {
        
        NSDictionary *objdic = obj;
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:[objdic count]];
        
        for(NSString *key in objdic.allKeys)
            
        {
            
            [dic setObject:[self getObjectInternal:[objdic objectForKey:key]] forKey:key];
            
        }
        
        return dic;
        
    }
    
    return [self getObjectData:obj];
    
}
/*
 计算时间戳--未来一个时间距离当前时间的倒计时
 aTimeString:当前时间
 bTimeString:结束时间
 */
+(NSString *)getNowTimeWithString:(NSString *)aTimeString unitlBtimeString:(NSString *)bTimeString{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //时间戳转标准时间
    NSTimeInterval time_b=[bTimeString doubleValue]  / 1000;//因为时差问题要加8小时 == 28800 sec
    NSDate *stampDate_b= [NSDate dateWithTimeIntervalSince1970:time_b];
    // 截止时间date格式
    NSTimeInterval time_a =[aTimeString doubleValue]/1000;
    NSDate *stampDate_a = [NSDate dateWithTimeIntervalSince1970:time_a];
//    NSDate  *nowDate = [NSDate date];
//    // 当前时间字符串格式
//    NSString *nowDateStr = [formater stringFromDate:nowDate];
//    // 当前时间date格式
//    nowDate = [formater dateFromString:nowDateStr];
    
    NSTimeInterval timeInterval =[stampDate_b timeIntervalSinceDate:stampDate_a];
    
    int days = (int)(timeInterval/(3600*24));
    int hours = (int)((timeInterval-days*24*3600)/3600);
    int minutes = (int)(timeInterval-days*24*3600-hours*3600)/60;
    int seconds = timeInterval-days*24*3600-hours*3600-minutes*60;
    
    NSString *dayStr;NSString *hoursStr;NSString *minutesStr;NSString *secondsStr;
    //天
//    dayStr = [NSString stringWithFormat:@"%d",days];
//    //小时
//    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<10)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    //秒
    if(seconds < 10)
        secondsStr = [NSString stringWithFormat:@"0%d", seconds];
    else
        secondsStr = [NSString stringWithFormat:@"%d",seconds];
    if (hours<=0&&minutes<=0&&seconds<=0) {
        return @"00:00";
    }
    if (days) {
        return [NSString stringWithFormat:@"%@:%@",minutesStr,secondsStr];
    }
    return [NSString stringWithFormat:@"%@:%@",minutesStr,secondsStr];
}
/*
 1、UIlabel中、多种颜色、多种大小、内容
 */
+(NSMutableAttributedString *)getDiffientLabel:(NSString *)strOne addOneColor:(UIColor *)colorOne addOneFont:(UIFont *)fontOne  addTwoStr:(NSString *)strTwo addTwoColor:(UIColor *)colorTwo addTwoFont:(UIFont *)fontTwo{
    
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    NSMutableAttributedString *one = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:strOne]];
    one.font =fontOne;
    one.color = colorOne;
    [text appendAttributedString:one];
    NSMutableAttributedString *two = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:strTwo]];
    two.font =fontTwo;
    two.color = colorTwo;
    [text appendAttributedString:two];
    return text;
}
//当前日期
+(NSString *)currentdateInterval
{
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)([datenow timeIntervalSince1970]*1000)];
    return timeSp;
}

//根据小时、分钟、秒、=====返回字符串
+ (NSString *)getHour:(NSInteger)hour addMinute:(NSInteger )minute addSecond:(NSInteger )second{
     NSString *strHour = @"";
     NSString *strMinute = @"";
    NSString *strSecond = @"";
    NSString *strTime = @"";
    if (hour==0) {
        strHour = @"";
    }else{
        strHour = hour<10?[NSString stringWithFormat:@"0%li",hour]:[NSString stringWithFormat:@"%li",hour];
    }
    if (minute==0) {
        strMinute = @"00";
    }else{
        strMinute = minute<10?[NSString stringWithFormat:@"0%li",minute]:[NSString stringWithFormat:@"%li",minute];
    }
    if (second==0) {
        strSecond = @"00";
    }else{
        strSecond = second<10?[NSString stringWithFormat:@"0%li",second]:[NSString stringWithFormat:@"%li",second];
    }
    if (strHour.length == 0 && strMinute.length ==0 && strSecond.length == 0) {
        strTime = @"00:00";
    }
   else if (strHour.length == 0 ) {
       strTime = [NSString stringWithFormat:@"%@:%@",strMinute,strSecond];
    }else{
        strTime = [NSString stringWithFormat:@"%@:%@:%@",strHour,strMinute,strSecond];
    }
    return strTime;
}

//根据高度计算内容的宽度
+ (CGFloat)getWidthWithContent:(NSString *)content height:(CGFloat)height font:(UIFont *)font{
    CGRect rect = [content boundingRectWithSize:CGSizeMake(MAXFLOAT, height)
                                        options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil
                   ];
    //因为这里获取的是准确的值，通过向上取整，解决外面计算过程中精度丢失的问题
    int width = ceilf(rect.size.width);
    return (CGFloat)width;
}

//根据宽度计算内容的高度
+ (CGFloat)getHeightWithContent:(NSString *)content width:(CGFloat)width font:(UIFont *)font{
    CGRect rect = [content boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                        options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil
                   ];
    //因为这里获取的是准确的值，通过向上取整，解决外面计算过程中精度丢失的问题(masonry中设置高度，有时候UI文字内容显示不全的问题，其实就是差那么一点点高度)
    int height = ceilf(rect.size.height);
    return (CGFloat)height;
}
//判断一个字符串中是否都是数字
+ (BOOL)isPureInt:(NSString *)string{
    
    NSScanner* scan = [NSScanner scannerWithString:string];
    
    int val;
    
    return [scan scanInt:&val] && [scan isAtEnd];
}

//返回一个视图 、传进来一个图标、图片的位置、整体视图的位置【为了让文本框的左边视图与右边内容之间有空隙】
+ (UIView *)addSuperView:(CGRect )superFrame addImageName:(NSString*)imgName addImgFrame:(CGRect)imgFrame{
    UIView *view_one = [[UIView alloc]initWithFrame:superFrame];
    view_one.backgroundColor = [UIColor whiteColor];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:imgFrame];
    img.image = [UIImage imageNamed:imgName];
    img.contentMode = UIViewContentModeScaleAspectFit;
    [view_one addSubview:img];
    return view_one;
}
@end
