//
//  XYTTools.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/13.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XYTTools : NSObject
//通过对象返回一个NSDictionary，键是属性名称，值是属性值。

+ (NSDictionary*)getObjectData:(id)obj;

//将getObjectData方法返回的NSDictionary转化成JSON

+ (NSData*)getJSON:(id)obj options:(NSJSONWritingOptions)options error:(NSError**)error;

//直接通过NSLog输出getObjectData方法返回的NSDictionary

+ (void)print:(id)obj;

+ (id)getObjectInternal:(id)obj;

/*
 计算时间戳--未来一个时间距离当前时间的倒计时
 aTimeString:当前时间
 bTimeString:结束时间
*/
+(NSString *)getNowTimeWithString:(NSString *)aTimeString unitlBtimeString:(NSString *)bTimeString;

/*
 1、UIlabel中、多种颜色、多种大小、内容
 */
+(NSMutableAttributedString *)getDiffientLabel:(NSString *)strOne addOneColor:(UIColor *)colorOne addOneFont:(UIFont *)fontOne  addTwoStr:(NSString *)strTwo addTwoColor:(UIColor *)colorTwo addTwoFont:(UIFont *)fontTwo;

+(NSString *)currentdateInterval;

//根据小时、分钟、秒、=====返回字符串
+ (NSString *)getHour:(NSInteger)hour addMinute:(NSInteger )minute addSecond:(NSInteger )second;

//根据高度计算内容的宽度
+ (CGFloat)getWidthWithContent:(NSString *)content height:(CGFloat)height font:(UIFont *)font;

//根据宽度计算内容的高度
+ (CGFloat)getHeightWithContent:(NSString *)content width:(CGFloat)width font:(UIFont *)font;
//判断一个字符串中是否都是数字
+ (BOOL)isPureInt:(NSString *)string;

//返回一个视图 、传进来一个图标、图片的位置、整体视图的位置【为了让文本框的左边视图与右边内容之间有空隙】
+ (UIView *)addSuperView:(CGRect )superFrame addImageName:(NSString*)imgName addImgFrame:(CGRect)imgFrame;
@end

NS_ASSUME_NONNULL_END
