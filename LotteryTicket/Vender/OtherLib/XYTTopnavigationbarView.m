//
//  XYTTopnavigationbarView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/14.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "XYTTopnavigationbarView.h"

@interface XYTTopnavigationbarView()

@property (nonatomic,strong)UILabel *lab_Title;

@end
@implementation XYTTopnavigationbarView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame: frame];
    CGFloat staHt = StatusBarHeight;
    CGFloat navHt = NaviBarHeight;
    
    UIView *navBar_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, navHt)];
    navBar_view.backgroundColor = STATUS_BAR_BGCOLOR;
    [self addSubview:navBar_view];
    
    self.lab_Title = [[UILabel alloc]initWithFrame:CGRectMake(80, staHt, navBar_view.width - 160, navHt - staHt)];
    self.lab_Title.text = @"开奖结果";
    self.lab_Title.font = MFont(18);
    self.lab_Title.textColor = [UIColor whiteColor];
    self.lab_Title.textAlignment = NSTextAlignmentCenter;
    [navBar_view addSubview:self.lab_Title];
    
    return self;
}
- (void)setStrViewtTitle:(NSString *)strViewtTitle{
    self.lab_Title.text = strViewtTitle;
}
@end
