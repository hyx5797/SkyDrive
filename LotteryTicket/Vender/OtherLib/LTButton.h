//
//  LTButton.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/20.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
//图片在上。文字在下的btn--个人中心
NS_ASSUME_NONNULL_BEGIN

@interface LTButton : UIButton


@end

//资金明细里面的按钮
@interface UPdownButton : UIButton

@property (nonatomic,strong)UIImageView *btn_imgView;
@property (nonatomic,strong)UILabel *btn_title;

@end
NS_ASSUME_NONNULL_END
