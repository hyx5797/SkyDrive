//
//  UnderLineTextField.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/2.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 带有下划线的文本框

NS_ASSUME_NONNULL_BEGIN

@interface UnderLineTextField : UITextField

@end

NS_ASSUME_NONNULL_END
