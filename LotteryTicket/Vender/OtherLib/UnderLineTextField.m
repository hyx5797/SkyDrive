//
//  UnderLineTextField.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/2.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UnderLineTextField.h"

@implementation UnderLineTextField

- (void)drawRect:(CGRect)rect {
//    [[UITextField appearance] setTintColor:UIColorHex(29A7FF)];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, UIColorHex(cccccc).CGColor);
    
    CGContextFillRect(context, CGRectMake(0, CGRectGetHeight(self.frame) - 1, CGRectGetWidth(self.frame), 1));
    
}

@end
