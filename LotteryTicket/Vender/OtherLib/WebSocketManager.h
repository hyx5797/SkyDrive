//
//  WebSocketManager.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/8.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebSocketManager : NSObject


+ (instancetype)helper;

//开启连接
- (void)connectWithURLString:(NSString *)urlString;
//发送数据
- (void)sendData:(id)data;
//关闭连接
- (void)closeWebSocket;


@end
