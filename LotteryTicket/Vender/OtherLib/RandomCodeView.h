//
//  RandomCodeView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/29.
//  Copyright © 2019 hyx. All rights reserved.
//
typedef void(^changeCaptchaBlock)();

#import <UIKit/UIKit.h>
#pragma 随机验证码的生成
NS_ASSUME_NONNULL_BEGIN

@interface RandomCodeView : UIView

@property (nonatomic, retain) NSArray *changeArray; //字符素材数组

@property (nonatomic, retain) NSMutableString *changeString;  //验证码的字符串

@property (nonatomic, copy) changeCaptchaBlock changeCaptchaBlock; // 每次刷新验证码后的操作，若无此需求，可忽略此属性

@end

NS_ASSUME_NONNULL_END
