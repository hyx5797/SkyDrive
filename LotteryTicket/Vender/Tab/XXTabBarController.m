//
//  XXTabBarController.m
//  VensAppiOS
//
//  Created by iOS Coder on 2017/9/28.
//  Copyright © 2017年 iOS Coder. All rights reserved.
//

#import "XXTabBarController.h"
#import "MineViewC.h"
#import "HomePageVC.h"
#import "LeftView.h"
#import "MenuHallVC.h"
#import "UncertaintyVC.h"
#import "LTLoginVC.h"
#import "FundManagementVC.h"
#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

// 侧边栏的宽度
#define LEFT_WIDTH (SCREEN_WIDTH/5) *4

@interface XXTabBarController () <UITabBarDelegate, UITabBarControllerDelegate,LeftViewDelegate>

@property(nonatomic, strong) LeftView *lefeView;
@property(nonatomic, strong) UIView *bgView;
@property (assign, nonatomic,getter=isHidden)  BOOL hidden;
// tabBar 的标题+图片字典数组
//@property(nonatomic, strong) NSMutableArray *tabItems;

@end

@implementation XXTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.tintColor = STATUS_BAR_BGCOLOR;
    self.tabBar.barTintColor = WHITE_COLOR;//背景颜色
    self.tabBar.translucent = NO;
    self.delegate = self;
    
    // init child vc
    NSMutableArray *array = [NSMutableArray array];
    HomePageVC *onevc = [[HomePageVC alloc] init];
    HomePageVC *twovc = [[HomePageVC alloc] init];
    FundManagementVC *threevc = [[FundManagementVC alloc] init];
    MineViewC *fourvc = [[MineViewC alloc] init];
    
    [self setUpChildViewController:onevc array:array title:@"首页" imageName:@"tab_home_page_normal" selectImageName:@"tab_home_page_selected"];
    [self setUpChildViewController:twovc array:array title:@"游戏" imageName:@"tab_game_normal" selectImageName:@"tab_game_selected"];
    [self setUpChildViewController:threevc array:array title:@"资金" imageName:@"tab_capital_normal" selectImageName:@"tab_capital_selected"];
    [self setUpChildViewController:fourvc array:array title:@"我的" imageName:@"tab_my_normal" selectImageName:@"tab_my_selected"];
    
    self.viewControllers = array;
    self.selectedIndex = 0;

    
}
- (void)setUpChildViewController:(UIViewController *)viewController array:(NSMutableArray *)array title:(NSString *)title imageName:(NSString *)imageName selectImageName:(NSString *)selectImageName
{
    
    RTRootNavigationController *navVC = [[RTRootNavigationController alloc] initWithRootViewController:viewController];
    navVC.tabBarItem.image=[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    navVC.tabBarItem.selectedImage=[[UIImage imageNamed:selectImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    navVC.tabBarItem.title = title;
    [array addObject:navVC];

    //创建手势
//    UISwipeGestureRecognizer *swipeRightGes = [[UISwipeGestureRecognizer alloc]init];
//    swipeRightGes.numberOfTouchesRequired = 1;//点击几次开启手势一般都是1或者2次
//    swipeRightGes.direction = UISwipeGestureRecognizerDirectionRight;//那么我们要说明我们需要什么手势，我这里选择的是右手势
//    //    调用下面的手势具体操作
//    [swipeRightGes addTarget:self action:@selector(tabHiddenOrShow)];
//    [viewController.view addGestureRecognizer:swipeRightGes];
}

- (void)tabHiddenOrShow
{
    self.hidden = !self.isHidden;
    
    if (self.lefeView == nil) {
        self.lefeView = [[LeftView alloc] initWithFrame:CGRectMake(-LEFT_WIDTH, 0, LEFT_WIDTH, SCREEN_HEIGHT)];
        self.lefeView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:self.lefeView];
    }
    if (self.bgView == nil) {
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT)];
        self.bgView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        [self.bgView addGestureRecognizer:tap];
    }
    
    CGRect leftFrame = self.lefeView.frame;
    if (self.isHidden == YES) {
        leftFrame.origin.x = -LEFT_WIDTH;
        [self.bgView removeFromSuperview];
    } else {
        [[UIApplication sharedApplication].keyWindow insertSubview:self.bgView belowSubview:self.lefeView];
        leftFrame.origin.x = 0;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.lefeView.frame = leftFrame;
        [self.view setNeedsLayout];
    }];
}

- (void)tapClick:(UITapGestureRecognizer *)tap
{
    [self tabHiddenOrShow];
}


#pragma mark - LeftViewDelegate
-(void)didClickChildButton:(NSString *)currentTitle{
    NSLog(@"当前选择的是===%@",currentTitle);
        [self tabHiddenOrShow];
        self.selectedIndex = 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
