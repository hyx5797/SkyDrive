//
//  XYTBaseViewController.h
//  XiaoWu
//
//  Created by BRAINDESIGN on 2019/5/29.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChoiceofGameTypesView.h"
NS_ASSUME_NONNULL_BEGIN

@interface XYTBaseViewController : UIViewController
@property (assign, nonatomic,getter=isHidden)  BOOL hidden; //左侧边视图
@property (assign, nonatomic,getter=isHidden_type)  BOOL hidden_type;//游戏分类

@property (nonatomic,strong)UIButton *btnBack;

- (void)tabHiddenOrShow;
//游戏的分类
- (void)gameTypeShowOrHidden;

@end

NS_ASSUME_NONNULL_END
