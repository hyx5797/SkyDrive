//
//  XYTBaseViewController.m
//  XiaoWu
//
//  Created by BRAINDESIGN on 2019/5/29.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "XYTBaseViewController.h"
#import "LeftView.h"
#import "UncertaintyVC.h"
#import "OverTodayVC.h"
#import "NoteRecordVC.h"
#import "OpeningResultVC.h"
#import "LTLoginVC.h"

#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

// 侧边栏的宽度(SCREEN_WIDTH/5) *4
//SCREEN_WIDTH/2
#define LEFT_WIDTH 224
#define CENTER_WIDTH (SCREEN_WIDTH/5) *4

@interface XYTBaseViewController ()<LeftViewDelegate>

@property(nonatomic, strong) LeftView *lefeView;
@property(nonatomic, strong) UIView *bgView;


@property (nonatomic,strong)ChoiceofGameTypesView *game_Type_view;
@property(nonatomic, strong) UIView *bg_type_View;



@end

@implementation XYTBaseViewController

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
//}
//
//- (void)viewDidDisappear:(BOOL)animated {
//    [super viewDidDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
//}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.isHidden_type == NO) {
        [self gameTypeShowOrHidden];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    self. btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.btnBack setImage:[UIImage imageNamed:@"navigation_white_back"] forState:UIControlStateNormal];
//    CGFloat staHt = self.navigationController.navigationBar.frame.size.height;
//    self.btnBack.frame = CGRectMake(15, staHt,50, 50 );
//    self.btnBack.hidden = YES;
//    [self.btnBack addTarget:self action:@selector(btnBackClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:self.btnBack];
    [self setHidden:YES];
    [self setHidden_type:YES];
}

- (void)tabHiddenOrShow
{
    self.hidden = !self.isHidden;
    if (self.lefeView == nil) {
        self.lefeView = [[LeftView alloc] initWithFrame:CGRectMake( SCREEN_WIDTH -LEFT_WIDTH, 0, LEFT_WIDTH, SCREEN_HEIGHT)];
        self.lefeView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:self.lefeView];
    }
    if (self.bgView == nil) {
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT)];
        self.bgView.backgroundColor = [UIColorHex(000000) colorWithAlphaComponent:0.4];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        [self.bgView addGestureRecognizer:tap];
    }
    
    CGRect leftFrame = self.lefeView.frame;
    if (self.isHidden == YES) {
        leftFrame.origin.x =SCREEN_WIDTH;
        [self.bgView removeFromSuperview];
    } else {
        [[UIApplication sharedApplication].keyWindow insertSubview:self.bgView belowSubview:self.lefeView];
        leftFrame.origin.x = SCREEN_WIDTH -LEFT_WIDTH;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.lefeView.frame = leftFrame;
        [self.view setNeedsLayout];
    }];
}
- (void)tapClick:(UITapGestureRecognizer *)tap
{
    [self tabHiddenOrShow];
}


#pragma mark - LeftViewDelegate
-(void)didClickChildButton:(NSString *)currentTitle{
    [self tabHiddenOrShow];
    if ([currentTitle isEqualToString:@"未结算"]) {
        UncertaintyVC *vc = [UncertaintyVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([currentTitle isEqualToString:@"今日已结"]) {
        OverTodayVC *vc = [OverTodayVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([currentTitle isEqualToString:@"下注记录"]) {
        NoteRecordVC *vc = [NoteRecordVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([currentTitle isEqualToString:@"开奖结果"]) {
        OpeningResultVC *vc = [OpeningResultVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([currentTitle isEqualToString:@"退出"]) {
        LTLoginVC *vc = [LTLoginVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


//游戏的分类
- (void)gameTypeShowOrHidden{
    
    CGFloat navHt = NaviBarHeight;
    self.hidden_type = !self.isHidden_type;
    if (self.game_Type_view == nil) {
        self.game_Type_view = [[ChoiceofGameTypesView alloc] initWithFrame:CGRectMake( SCREEN_WIDTH -CENTER_WIDTH, navHt, CENTER_WIDTH, SCREEN_HEIGHT-navHt)];
        self.game_Type_view.backgroundColor = UIColorHex(ffffff);
        [[UIApplication sharedApplication].keyWindow addSubview:self.game_Type_view];
    }
    
    if (self.bg_type_View == nil) {
        self.bg_type_View = [[UIView alloc] initWithFrame:CGRectMake(0, navHt, SCREEN_WIDTH,SCREEN_HEIGHT - navHt)];
        self.bg_type_View.backgroundColor =[UIColorHex(000000) colorWithAlphaComponent:0.4];
//        [UIColor colorWithWhite:0.3 alpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(taptype_ViewClick:)];
        [self.bg_type_View addGestureRecognizer:tap];
    }
    CGRect leftFrame = self.game_Type_view.frame;
    if (self.isHidden_type == YES) {
        leftFrame.origin.x =SCREEN_WIDTH;
        [self.bg_type_View removeFromSuperview];
    } else {
        [[UIApplication sharedApplication].keyWindow insertSubview:self.bg_type_View belowSubview:self.game_Type_view];
        leftFrame.origin.x = SCREEN_WIDTH -CENTER_WIDTH;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.game_Type_view.frame = leftFrame;
        [self.view setNeedsLayout];
    }];
}

- (void)taptype_ViewClick:(UITapGestureRecognizer *)tap
{
    [self gameTypeShowOrHidden];
}
@end
