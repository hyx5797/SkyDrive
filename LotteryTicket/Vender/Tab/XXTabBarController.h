//
//  XXTabBarController.h
//  VensAppiOS
//
//  Created by iOS Coder on 2017/9/28.
//  Copyright © 2017年 iOS Coder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XXTabBarController : UITabBarController

@property (nonatomic, assign) NSInteger lastSelectedIndex;//上一次选中的下标;

@end
