//
//  XXTabBarController.m
//  VensAppiOS
//
//  Created by iOS Coder on 2017/9/28.
//  Copyright © 2017年 iOS Coder. All rights reserved.
//

#import "MBProgressHUD+MJ.h"

@implementation MBProgressHUD (MJ)
#pragma mark 显示信息
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.margin = 15.0;
    hud.label.text = text;
//    hud.label.textColor = [UIColor whiteColor];
    // 设置图片
//    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
    // 再设置模式
    hud.mode = MBProgressHUDModeText;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    hud.bezelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    hud.backgroundView.backgroundColor = [UIColor clearColor];
    hud.contentColor = [UIColor whiteColor];
    // 1秒之后再消失
    [hud hideAnimated:YES afterDelay:1.5];
//    [hud hide:YES afterDelay:0.7];
}


+ (void)showProgressWithTitle:(NSString *)title toView:(UIView *)view{
    
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    hud.label.text = title;
    
    // 再设置模式
    hud.mode = MBProgressHUDModeIndeterminate;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    hud.bezelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    
    hud.backgroundView.backgroundColor = [UIColor clearColor];
    
    hud.contentColor = WHITE_COLOR;
//    // 1秒之后再消失
//    [hud hideAnimated:YES afterDelay:50];
    
}




#pragma mark 显示错误信息
+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:@"error.png" view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:@"success.png" view:view];
}

#pragma mark 显示一些信息
+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
//    NSMutableArray *refreshingImages = [NSMutableArray array];
//    for (NSUInteger i = 1; i<=16; i++) {
//        UIImage *orignImage = [UIImage imageNamed:[NSString stringWithFormat:@"loading%zd", i]];
//        [refreshingImages addObject:orignImage];
//    }
    
    UIImageView * customImageView = [[UIImageView alloc] init];
//    [customImageView setAnimationImages:refreshingImages];
    customImageView.backgroundColor = UIColorHex(345233);
    customImageView.animationDuration = 3.0;
    customImageView.animationRepeatCount = 0;
    [customImageView startAnimating];
    hud.customView = customImageView;
    
    hud.mode = MBProgressHUDModeCustomView;
    hud.label.text = message;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // YES代表需要蒙版效果
    hud.dimBackground = YES;
    
    hud.bezelView.backgroundColor = [UIColor clearColor];
    hud.backgroundView.backgroundColor = [UIColor clearColor];
    [hud hideAnimated:YES afterDelay:30];
    return hud;
}

+ (void)showSuccess:(NSString *)success
{
    [self showSuccess:success toView:nil];
}

+ (void)showError:(NSString *)error
{
    [self showError:error toView:[UIApplication sharedApplication].keyWindow];
}

+ (MBProgressHUD *)showMessage:(NSString *)message
{
    return [self showMessage:message toView:nil];
}

+ (void)hideHUDForView:(UIView *)view
{
    if (!view) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    
    [self hideHUDForView:view animated:YES];
}

+ (void)hideHUD
{
    [self hideHUDForView:nil];
}
@end
