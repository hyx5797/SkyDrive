//
//  UIScrollView+Refrush.h
//  GeckoLive
//
//  Created by iMac on 2017/3/20.
//  Copyright © 2017年 Gecko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Refrush)

@property (nonatomic,assign)NSInteger indexPage;

//推荐用这个
- (void)addRefrushHeaderWithTarget:(id)target Selector:(SEL)sel BeganRefrush:(BOOL)refrush;
- (void)addRefrushHeaderWithTarget:(id)target Selector:(SEL)sel;

- (void)endRefrushDataCount:(NSInteger)dataCount addFooter:(BOOL)isAddFooter NoContent:(void (^)(void))noContent;

- (void)addRefrushFooterWithTarget:(id)target Selector:(SEL)sel;

- (void)endRefrush;

//用来保存刷新的方法 外部不能直接设置
//调用addRefrushHeaderWithTarget: Selector:即可
@property (nonatomic,weak)id tagret;
@property (nonatomic,assign)SEL sel;

@end
