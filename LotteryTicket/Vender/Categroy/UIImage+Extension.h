//
//  UIImage+Extension.h
//  VensAppiOS
//
//  Created by iOS Coder on 2018/4/17.
//  Copyright © 2018年 iOS Coder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

/**
 * 传入图片的名称,返回一张可拉伸不变形的图片
 *
 * @param imageName 图片名称
 *
 * @return 可拉伸图片
 */
+ (UIImage *)resizableImageWithName:(NSString *)imageName;

+ (UIImage *)resizableImageWithUIImage:(UIImage *)imageName;

+ (UIImage *)coreBlurImage:(UIImage *)image
            withBlurNumber:(CGFloat)blur;

@end
