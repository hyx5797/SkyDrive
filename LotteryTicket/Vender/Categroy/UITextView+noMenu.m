//
//  UITextView+noMenu.m
//  VensAppiOS
//
//  Created by UIDesigner on 2018/1/15.
//  Copyright © 2018年 iOS Coder. All rights reserved.
//

#import "UITextView+noMenu.h"

@implementation UITextView (noMenu)

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
//    if (action == @selector(paste:))//禁止粘贴
//        return NO;
//    if (action == @selector(select:))// 禁止选择
//        return NO;
//    if (action == @selector(selectAll:))// 禁止全选
//        return NO;
//    return [super canPerformAction:action withSender:sender];
    if ([UIMenuController sharedMenuController]){
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}


@end
