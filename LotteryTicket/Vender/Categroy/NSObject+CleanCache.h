//
//  NSObject+CleanCache.h
//  VTravel
//
//  Created by lanouhn on 16/8/2.
//  Copyright © 2016年 lanouhn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CleanCache)

// 1.获取缓存
//需要提供一个对象方法,外界去调用对象方法.
- (void)getFileCacheSizeWithPath:(NSString *)path completion:(void (^)(NSInteger total))completion;
// 2.清除缓存
- (void)removeCacheWithCompletion:(void (^)())completion;
// 3.缓存路径
- (NSString *)cachePath;

@end
