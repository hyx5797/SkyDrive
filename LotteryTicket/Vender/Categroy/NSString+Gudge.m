//
//  NSString+Gudge.m
//  约吧互动
//
//  Created by iMac on 16/6/29.
//  Copyright © 2016年 xingzhe. All rights reserved.
//

#import "NSString+Gudge.h"

@implementation NSString (Gudge)
- (BOOL)telephone
{
    NSString * MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)\\d{8}$";
    NSString * cm = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    NSString * cu = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    NSString * ct = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    NSString * phs = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    NSPredicate *regexmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regexcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cm];
    NSPredicate *regexcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cu];
    NSPredicate *regexct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", ct];
    NSPredicate *regexphs = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phs];
    
    return  [regexmobile evaluateWithObject:self]   ||
    [regexphs evaluateWithObject:self]      ||
    [regexct evaluateWithObject:self]       ||
    [regexcu evaluateWithObject:self]       ||
    [regexcm evaluateWithObject:self];
}
- (BOOL)password

{
    NSString *passWordRegex = @"^[a-zA-Z0-9]{6,20}+$";
    
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    
   return [passWordPredicate evaluateWithObject:self];
    
}

-(BOOL)verificationCode{
    
    NSString *passWordRegex = @"^\\d{6}$";
    
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    
    return [passWordPredicate evaluateWithObject:self];
}

- (BOOL)email
{
    NSString *emailEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailEx];
    return [regExPredicate evaluateWithObject:[self lowercaseString]];
}

- (BOOL)emptyOrWhitespace
{
    return self == nil || !([self length] > 0) || [[self trimmedString] length] == 0 || self == NULL;
}

- (NSString *)trimmedString
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
-(NSString *)time:(NSInteger )timestamp
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *dataStr = [formatter stringFromDate:date];
    return dataStr;
}
@end
