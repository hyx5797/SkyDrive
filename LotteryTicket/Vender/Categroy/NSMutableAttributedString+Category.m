//
//  NSMutableAttributedString+ColorString.m
//  eHealthCare
//
//  Created by jamkin on 16/9/2.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "NSMutableAttributedString+Category.h"
#import<CoreText/CoreText.h>

@implementation NSMutableAttributedString (Category)

+(instancetype)createColorString:(NSString *)dainamicStr withExcision:(NSString *)excisionStr dainmaicColor:(UIColor *)dColor excisionColor:(UIColor *)exColor{
    
    NSString *str=dainamicStr;
    
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc]initWithString:str];
    
    NSArray *textArray=[str componentsSeparatedByString:excisionStr];
    
    for (int i=0;i<textArray.count;i++) {
        
        NSRange range=[str rangeOfString:textArray[i]];
        
        [attri addAttribute:NSForegroundColorAttributeName value:dColor range:range];
        
    }
    NSRange range1=[str rangeOfString:excisionStr];
    
    [attri addAttribute:NSForegroundColorAttributeName value:exColor range:range1];
    
    return attri;
    
}

//+(instancetype)content:(NSString *)tent wtihLineSpacing:(NSInteger)spacing withSpacing:(CGFloat)jspacing{
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:tent];
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle setLineSpacing:spacing];
//    
//    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [tent length])];
//    
//    long number = jspacing;
//    CFNumberRef num = CFNumberCreate(kCFAllocatorDefault,kCFNumberSInt8Type,&number);
//    [attributedString addAttribute:(id)kCTKernAttributeName value:(__bridge id)num range:NSMakeRange(0,[tent length])];
//    CFRelease(num);
//    
//    return attributedString;
//}

@end
