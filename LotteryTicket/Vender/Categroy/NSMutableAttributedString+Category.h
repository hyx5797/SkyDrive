//
//  NSMutableAttributedString+ColorString.h
//  eHealthCare
//
//  Created by jamkin on 16/9/2.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (Category)

/**
 *传入一个字符串  转为一个变色的字符串
 *dainamicStr 元字符串
 *excisionStr 要截取的字符串
 *dainmaicColor 不变的颜色
 *exColor 要变的颜色
 **/
+(instancetype)createColorString:(NSString *)dainamicStr withExcision:(NSString *)excisionStr dainmaicColor:(UIColor *)dColor excisionColor:(UIColor *)exColor;

///*调整行间距 字间距 行间距*/
//+(instancetype)content:(NSString *)tent wtihLineSpacing:(NSInteger)spacing withSpacing:(CGFloat)jspacing;


@end
