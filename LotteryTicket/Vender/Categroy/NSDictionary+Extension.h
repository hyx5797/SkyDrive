//
//  NSDictionary+Extension.h
//  VensAppiOS
//
//  Created by iOS Coder on 2018/5/22.
//  Copyright © 2018年 iOS Coder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

@end
