//
//  NSString+Extension.h
//  XZ_WeChat
//
//  Created by 郭现壮 on 16/9/27.
//  Copyright © 2016年 gxz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (NSString *)emoji;

- (CGSize)sizeWithMaxWidth:(CGFloat)width andFont:(UIFont *)font;

- (NSString *)originName;

+ (NSString *)currentName;

- (NSString *)firstStringSeparatedByString:(NSString *)separeted;
-(NSString *)convertToJsonData:(NSDictionary *)dict;//字典转换成string

- (CGSize)czh_sizeWithFont:(UIFont *)font maxH:(CGFloat)maxH;
- (CGSize)czh_sizeWithFont:(UIFont *)font maxW:(CGFloat)maxW;
- (CGSize)czh_sizeWithFont:(UIFont *)font;

@end
