//
//  NSString+Gudge.h
//  约吧互动
//
//  Created by iMac on 16/6/29.
//  Copyright © 2016年 xingzhe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Gudge)
//判断是否为手机号
-(BOOL)telephone;
//是否为邮箱
-(BOOL)email;
//字符串是否为空
-(BOOL)emptyOrWhitespace;
//去除空格
-(NSString *)trimmedString;

-(BOOL)password;
-(BOOL)verificationCode;

//转换时间戳
-(NSString *)time:(NSInteger )timestamp;
@end
