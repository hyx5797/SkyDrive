//
//  UIScrollView+Refrush.m
//  GeckoLive
//
//  Created by iMac on 2017/3/20.
//  Copyright © 2017年 Gecko. All rights reserved.
//

#import "UIScrollView+Refrush.h"
#import <objc/runtime.h>

static void *indexPage1 = &indexPage1;
static void *tagret3 = &tagret3;
static void *sel3 = &sel3;

@implementation UIScrollView (Refrush)

-(void)setIndexPage:(NSInteger)indexPage
{
    objc_setAssociatedObject(self, &indexPage1, @(indexPage), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(NSInteger)indexPage
{
    return [objc_getAssociatedObject(self, &indexPage1) integerValue];
}
- (void)setTagret:(id)tagret{
    objc_setAssociatedObject(self, &tagret3, tagret, OBJC_ASSOCIATION_ASSIGN);
}
- (id)tagret{
    return objc_getAssociatedObject(self, &tagret3);
}
- (void)setSel:(SEL)sel{
    objc_setAssociatedObject(self, &sel3, NSStringFromSelector(sel), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (SEL)sel{
    return NSSelectorFromString(objc_getAssociatedObject(self, &sel3));
}

- (void)addRefrushHeaderWithTarget:(id)target Selector:(SEL)sel{
    self.tagret = target;
    self.sel = sel;
//    __weak typeof(self) wself = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.indexPage = 0;//默认设置为1
        if ([self.tagret respondsToSelector:sel]) {
            ((void (*)(id, SEL))objc_msgSend)(self.tagret,self.sel);
        }
    }];
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    header.automaticallyChangeAlpha = YES;
    // 隐藏时间
//    header.lastUpdatedTimeLabel.hidden = YES;
    // 隐藏状态
//    header.stateLabel.hidden = YES;
    // 马上进入刷新状态
    [header beginRefreshing];
    // 设置header
    self.mj_header = header;
}
- (void)addRefrushHeaderWithTarget:(id)target Selector:(SEL)sel BeganRefrush:(BOOL)refrush{
    self.tagret = target;
    self.sel = sel;
//    __weak typeof(self) wself = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.indexPage = 0;//默认设置为1
        if ([self.tagret respondsToSelector:sel]) {
            ((void (*)(id, SEL))objc_msgSend)(self.tagret,self.sel);
        }
    }];
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    header.automaticallyChangeAlpha = YES;
    // 隐藏时间
    //    header.lastUpdatedTimeLabel.hidden = YES;
    // 隐藏状态
    //    header.stateLabel.hidden = YES;
    // 马上进入刷新状态
    if (refrush) {
        [header beginRefreshing];
    }
    // 设置header
    self.mj_header = header;
}

- (void)addRefrushFooterWithTarget:(id)target Selector:(SEL)sel{
    self.tagret = target;
    self.sel = sel;
//    __weak typeof(self) wself = self;
    MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.indexPage++;
        if ([self.tagret respondsToSelector:sel]) {
            ((void (*)(id, SEL))objc_msgSend)(self.tagret,self.sel);
        }
    }];
    footer.refreshingTitleHidden = YES;
    footer.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    self.mj_footer = footer;
}

- (void)endRefrushDataCount:(NSInteger)dataCount addFooter:(BOOL)isAddFooter NoContent:(void (^)())noContent{
    if (self.indexPage == 0) {//表示下拉刷新
        [self.mj_header endRefreshing];
        if (dataCount) {
            if (isAddFooter) {
                [self addRefrushFooterWithTarget:self.tagret Selector:self.sel];
            }
        }else{
            !noContent ?: noContent();
        }
    }else{
        if (dataCount) {
            [self.mj_footer endRefreshing];
        }else{
            [self.mj_footer endRefreshingWithNoMoreData];
        }
    }
}

- (void)endRefrush{
    if ([self.mj_header isRefreshing]) {
        [self.mj_header endRefreshing];
    }
    if ([self.mj_footer isRefreshing]) {
        [self.mj_footer endRefreshing];
    }
}



@end
