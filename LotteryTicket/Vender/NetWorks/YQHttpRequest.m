//
//  YQHttpRequest.m
//  QIKE
//
//  Created by Qdong365 on 16/1/27.
//  Copyright © 2016年 Qdong365. All rights reserved.
//

#import "YQHttpRequest.h"

@interface YQHttpRequest ()
@property (nonatomic,assign)BOOL isUpdatePushKey;

@end

@implementation YQHttpRequest


static YQHttpRequest *singleton = nil;
+ (YQHttpRequest *)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[YQHttpRequest alloc]init];
    });
    return singleton;
}

static AFHTTPSessionManager *shareNetworkManager = nil;

+ (AFHTTPSessionManager *)shareNetworkManager{
    static dispatch_once_t onceToken2;
    dispatch_once(&onceToken2, ^{
        NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",BASEURL]];
        shareNetworkManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        // 设置请求格式
        shareNetworkManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        // 设置返回格式
        shareNetworkManager.responseSerializer = [AFJSONResponseSerializer serializer];
      
        shareNetworkManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
       
        shareNetworkManager.requestSerializer.timeoutInterval = 5.0f;
    });
    return shareNetworkManager;
}

//新方法新增 succ参数
+ (void)get:(NSDictionary*)para url:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail
{
     __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        
        [[weakSelf.class shareNetworkManager] GET:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                if ([[responseObject allKeys]containsObject:@"serverTime"]) {
                    [UD setObject:responseObject[@"serverTime"] forKey:SERVER_TIME];
                    [UD synchronize];
                }
                success([responseObject[@"code"] boolValue],responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"GET请求错误码:%ld---%@",(long)httpResponse.statusCode,error);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if ([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
            
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//新方法新增 succ参数
+ (void)post:(NSDictionary*)para url:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail
{
     __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        [[weakSelf.class shareNetworkManager] POST:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success([responseObject[@"State"] boolValue],responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"POST请求错误码:%ld---%ld",(long)httpResponse.statusCode,error.code);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            if ([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//新方法新增 传完整的url
+ (void)get:(NSDictionary*)para withUrl:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail
{
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        AFHTTPSessionManager  *manager = [[AFHTTPSessionManager alloc] init];
        // 设置请求格式
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        //        // 设置返回格式
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        
        manager.requestSerializer.timeoutInterval = 5.0f;
        
        [manager GET:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success([responseObject[@"State"] boolValue],responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"GET请求错误码:%ld---%@",(long)httpResponse.statusCode,error);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}


//旧的方法
+ (void)getData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail
{
     __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        
        [[weakSelf.class shareNetworkManager] GET:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"GET请求错误码:%ld---%@",(long)httpResponse.statusCode,error);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else{
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//旧的方法
+ (void)postData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail
{
    
    __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        
        [[weakSelf.class shareNetworkManager] POST:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"POST请求错误码:%ld---%ld",(long)httpResponse.statusCode,error.code);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if ([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}
//PUT
+ (void)putData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail
{
    __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        [[weakSelf.class shareNetworkManager]PUT:url parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"POST请求错误码:%ld---%ld",(long)httpResponse.statusCode,error.code);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if ([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}
//DELETE
+ (void)deleteData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail
{
     __weak typeof(self) weakSelf = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        [[weakSelf.class shareNetworkManager]DELETE:url parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
//            NSLog(@"POST请求错误码:%ld---%ld",(long)httpResponse.statusCode,error.code);
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if ([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//post参数放入Body
+ (void)postBodyData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *requestUrl = [NSString stringWithFormat:@"%@%@", BASEURL, url];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone]; //不设置会报-1016或者会有编码问题
        manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
        manager.responseSerializer = [AFHTTPResponseSerializer serializer]; //不设置会报 error 3840
        manager.requestSerializer.timeoutInterval = 5.0f;
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil]];
        
        if ([UD objectForKey:TOKEN] != nil) {
            NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
            for (NSString *httpHeaderField in headerFieldValueDictionary.allKeys) {
                NSString *value = headerFieldValueDictionary[httpHeaderField];
                [requestSerializer setValue:value forHTTPHeaderField:httpHeaderField];
            }
        }
        
        NSMutableURLRequest *request = [requestSerializer requestWithMethod:@"POST" URLString:requestUrl parameters:nil error:nil];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSData *param  = [body dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:param];
        [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error)
          {
              if (responseObject) {
                  if ([url containsString:@"/user/collection"]) {
                      //喜欢的选定和取消特殊处理
                      success(responseObject);
                  }else{
                      NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                      success(dic);
                  }
              }
          }] resume];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//PUt参数放入Body
+ (void)putBodyData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *requestUrl = [NSString stringWithFormat:@"%@%@", BASEURL, url];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone]; //不设置会报-1016或者会有编码问题
        manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
        manager.responseSerializer = [AFHTTPResponseSerializer serializer]; //不设置会报 error 3840
        manager.requestSerializer.timeoutInterval = 5.0f;
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil]];
        NSMutableURLRequest *request = [requestSerializer requestWithMethod:@"PUT" URLString:requestUrl parameters:nil error:nil];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSData *param  =[body dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:param];
        [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error)
          {
              if (responseObject) {
                  NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                  success(dic);
              }
              
          }] resume];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
    
    
}
//放在header里面传Tocken、get方法
+ (void)getHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
    //    AFHTTPRequestSerializer *requestSerializer =  [AFHTTPRequestSerializer serializer];
    if ([url containsString:@"/user/collection"]) {
        //喜欢判定-特殊处理
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else {
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json",nil];
    manager.requestSerializer.timeoutInterval = 5.0f;

    NSString *tokenStr = @"";
    if([UD objectForKey:TOKEN] != nil){
        tokenStr = [UD objectForKey:TOKEN];
    }
    
    NSDictionary *headerFieldValueDictionary = @{@"Authorization":tokenStr,@"Apptype":@"IOS"};
    for (NSString *httpHeaderField in headerFieldValueDictionary.allKeys) {
        NSString *value = headerFieldValueDictionary[httpHeaderField];
        [requestSerializer setValue:value forHTTPHeaderField:httpHeaderField];
    }

    manager.requestSerializer = requestSerializer;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *baseU= [NSString stringWithFormat:@"%@%@",BASEURL,url];
        [manager GET:baseU parameters:para  progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(responseObject);
            }
        
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSString *codeError = [NSString stringWithFormat:@"%@",error];
                
                if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
                {
                    NSLog(@"yes");
//                    [HYXTools exitLogin];//退出登录
                    if (fail) {
                        fail(nil);
                    }
                }else if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                    NSLog(@"YES");
                    if (fail) {
                        fail(nil);
                    }
//                    [HYXTools createAlertView];
                }
                else
                {
                    NSLog(@"no");
                    if (fail) {
                        fail(error);
                    }
                }
              
            }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//放在header里面传Tocken、post方法
+ (void)postHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    manager.requestSerializer.timeoutInterval = 5.0f;
    NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
    if (headerFieldValueDictionary != nil) {
        for (NSString *httpHeaderField in headerFieldValueDictionary.allKeys) {
            NSString *value = headerFieldValueDictionary[httpHeaderField];
            [requestSerializer setValue:value forHTTPHeaderField:httpHeaderField];
        }
    }
    manager.requestSerializer = requestSerializer;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *baseU= [NSString stringWithFormat:@"%@%@",BASEURL,url];
        [manager POST:baseU parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
            {
                NSLog(@"yes");
//                [HYXTools exitLogin];//退出登录
                if (fail) {
                    fail(nil);
                }
            }else if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }
            else
            {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
    
}
//post参数放入Body、并加入header
+ (void)postBodyOrHeaderData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
        NSString *requestUrl = [NSString stringWithFormat:@"%@%@", BASEURL, url];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//        manger.requestSerializer =  [AFJSONRequestSerializer serializer];
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone]; //不设置会报-1016或者会有编码问题
        manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
        manager.requestSerializer.timeoutInterval = 5.0f;
        // 设置返回格式
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];

        NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
        NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:requestUrl parameters:nil error:nil];

        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setAllHTTPHeaderFields:headerFieldValueDictionary];
        NSData *param  =[body dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:param];

        [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
            [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error)
              {
                  NSString *codeError = [NSString stringWithFormat:@"%@",error];
                  
                  if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
                  {
                      NSLog(@"yes");
//                      [HYXTools exitLogin];//退出登录
                      if (fail) {
                          fail(nil);
                      }
                  }else{
                      success(responseObject);
                  }
                  
              }] resume];

        } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}

//put参数放入Body、并加入header
+ (void)putBodyOrHeaderData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@", BASEURL, url];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone]; //不设置会报-1016或者会有编码问题
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
    // 设置返回格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    manager.requestSerializer.timeoutInterval = 5.0f;
    NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:requestUrl parameters:nil error:nil];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setAllHTTPHeaderFields:headerFieldValueDictionary];
    NSData *param  =[body dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:param];
    
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error)
          {
              
              NSString *codeError = [NSString stringWithFormat:@"%@",error];
              
              if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
              {
                  NSLog(@"yes");
//                  [HYXTools exitLogin];//退出登录
                  if (fail) {
                      fail(nil);
                  }
              }else{
                  success(responseObject);
              }
              
          }] resume];
        
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
}
//放在header里面传Tocken、put方法
+ (void)putHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    manager.requestSerializer.timeoutInterval = 5.0f;
    NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
    if (headerFieldValueDictionary != nil) {
        for (NSString *httpHeaderField in headerFieldValueDictionary.allKeys) {
            NSString *value = headerFieldValueDictionary[httpHeaderField];
            [requestSerializer setValue:value forHTTPHeaderField:httpHeaderField];
        }
    }
    manager.requestSerializer = requestSerializer;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *baseU= [NSString stringWithFormat:@"%@%@",BASEURL,url];
        [manager PUT:baseU parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (success)
                {
                    success(responseObject);
                }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
            {
                NSLog(@"yes");
//                [HYXTools exitLogin];//退出登录
                if (fail) {
                    fail(nil);
                }
            }else if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }
            else
            {
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
    
}
//放在header里面传Tocken、delete方法
+ (void)deleteHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer =  [AFJSONRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    manager.requestSerializer.timeoutInterval = 5.0f;
    NSDictionary *headerFieldValueDictionary = @{@"Authorization":[UD objectForKey:TOKEN]};
    if (headerFieldValueDictionary != nil) {
        for (NSString *httpHeaderField in headerFieldValueDictionary.allKeys) {
            NSString *value = headerFieldValueDictionary[httpHeaderField];
            [requestSerializer setValue:value forHTTPHeaderField:httpHeaderField];
        }
    }
    manager.requestSerializer = requestSerializer;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        NSString *baseU= [NSString stringWithFormat:@"%@%@",BASEURL,url];
        [manager DELETE:baseU parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success)
            {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSString *codeError = [NSString stringWithFormat:@"%@",error];
            
            if([codeError rangeOfString:@"Status Code: 403"].location != NSNotFound)//_roaldSearchText
            {
                NSLog(@"yes");
//                [HYXTools exitLogin];//退出登录
                if (fail) {
                    fail(nil);
                }
            }else if([codeError rangeOfString:@"The Internet connection appears to be offline"].location != NSNotFound){
                NSLog(@"YES");
                if (fail) {
                    fail(nil);
                }
//                [HYXTools createAlertView];
            }else{
                NSLog(@"no");
                if (fail) {
                    fail(error);
                }
            }
        }];
      
    } noNet:^{
        if (fail) {
            fail(nil);
        }
//        [HYXTools createAlertView];
    }];
    
}
//判断有没有网络
- (void)setReachabilityStatusChangeBlockHaveNet:(void (^)(void))haveNet noNet:(void (^)())noNet{
    
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus != AFNetworkReachabilityStatusNotReachable) {
        if (haveNet) {
            haveNet();
        }
    }else{
        if (noNet) {
            noNet();
        }
    }
}
/*
//上传单张或者多张图片
- (void)postImages:(NSDictionary*)para imageArray:(NSArray *)imageArray url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail{
    __weak typeof(self) wself = self;
    [[self.class shareInstance] setReachabilityStatusChangeBlockHaveNet:^{
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        // 设置请求格式
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        // 设置返回格式
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.requestSerializer.timeoutInterval = 30.0f;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        NSString *imgName = para[@"imgName"];
        if (!imgName) {
            imgName = @"image";
        }
        // 显示进度
        //外面不需要传url进来，因为都是一样的
        NSString * lastURL = @"file/upload.do";
        if (arrImagedata.count > 1) {
            lastURL = @"file/multipleUpload.do";
        }
        
        [manager POST:lastURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            // 上传 多张图片
            NSString * Name = @"file";
            NSString * fileName;
            if (arrImagedata.count > 1) {
                Name = @"files";
            }
            for(NSInteger i = 0; i < arrImagedata.count; i++)
            {
                NSData * imageData = arrImagedata[i];
                // 上传filename
                fileName = [NSString stringWithFormat:@"%@_%ld.jpg",imgName,(long)i];
                [formData appendPartWithFileData:imageData name:Name fileName:fileName mimeType:@"image/jpeg"];
            }
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            NSLog(@"上传 ：%lld, %lld",uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
            if (uploadProgress.completedUnitCount == uploadProgress.totalUnitCount) {
                NSLog(@"发布上传完成");
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success){
                success(responseObject);
            }
            
            if (![responseObject[@"State"] integerValue]) {
                if (responseObject[@"Msg"]) {
                    [MBProgressHUD showError:responseObject[@"Msg"]];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            NSLog(@"POST请求错误码:%ld---%ld",(long)httpResponse.statusCode,error.code);
            if (fail) {
                fail(error);
            }
        }];
    } noNet:^{
        if (fail) {
            fail(nil);
        }
        [HYXTools createAlertView];
    }];
}
*/
//网络监听
+ (void)networkMonitoring {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case -1:
                NSLog(@"未知网络");
                break;
            case 0:
                NSLog(@"网络不可达");
                break;
            case 1:
                NSLog(@"GPRS网络");
                break;
            case 2:
                NSLog(@"wifi网络");
                break;
            default:
                break;
        }
        if(status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            NSLog(@"有网");
        }else {
            NSLog(@"没有网");
//            [HYXTools createAlertView];
        }
    }];
}


@end
