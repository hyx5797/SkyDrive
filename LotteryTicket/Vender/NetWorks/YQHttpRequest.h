//
//  YQHttpRequest.h
//  QIKE
//
//  Created by Qdong365 on 16/1/27.
//  Copyright © 2016年 Qdong365. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YQHttpRequest : NSObject

@property (nonatomic,copy)NSString * httpSession;


+ (YQHttpRequest *)shareInstance;

///get请求(new) edit by wjx 8-3
+ (void)get:(NSDictionary*)para url:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail;
//传完整的url
+ (void)get:(NSDictionary*)para withUrl:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail;

///post请求(mew) edit by wjx 8-3
+ (void)post:(NSDictionary*)para url:(NSString*)url successed:(void (^)(BOOL succ, id responseDic))success fail:(void (^)(NSError *error))fail;

///get请求(old)
+ (void)getData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
///post请求(old)
+ (void)postData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//DELETE
+ (void)deleteData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//PUT
+ (void)putData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
/**
 判断有没有网络
 */
- (void)setReachabilityStatusChangeBlockHaveNet:(void (^)())haveNet noNet:(void (^)())noNet;

//放在header里面传Tocken、get方法
+ (void)getHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;

//放在header里面传Tocken、post方法
+ (void)postHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;

//放在header里面传Tocken、put方法
+ (void)putHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//放在header里面传Tocken、delete方法
+ (void)deleteHeaderData:(NSDictionary*)para url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
/**
 上传图片
 */
- (void)postImages:(NSDictionary*)para imageArray:(NSArray *)imageArray url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//参数放入Body
+ (void)postBodyData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//PUt参数放入Body
+ (void)putBodyData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//post参数放入Body、并加入header
+ (void)postBodyOrHeaderData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;
//put参数放入Body、并加入header
+ (void)putBodyOrHeaderData:(NSString*)body url:(NSString*)url success:(void (^)(id responseDic))success fail:(void (^)(NSError *error))fail;

//网络监听
+ (void)networkMonitoring;

@end
