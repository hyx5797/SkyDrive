//
//  Header.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/7/30.
//  Copyright © 2019 hyx. All rights reserved.
//

#ifndef Header_h
#define Header_h


#define VERSION_INFO_CURRENT @"currentversion"
#define UD [NSUserDefaults standardUserDefaults]
#define NC [NSNotificationCenter defaultCenter]

#define iPadDevice (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT   ([UIScreen mainScreen].bounds.size.height)
#define COLOR(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define UIColorHex(_hex_) [UIColor colorWithHexString:((__bridge NSString *)CFSTR(#_hex_))]
#define FITSIZE(s,m,l) [HYXTools g_fitFloat:@[@(s),@(m),@(l)]]
#define STATUS_BAR_BGCOLOR UIColorHex(3984e8)
#define NINE_COLOR UIColorHex(999999)
#define SIX_COLOR UIColorHex(666666)
#define THREE_COLOR UIColorHex(333333)
#define WHITE_COLOR COLOR(248,255,255)
#define CCC_COLOR UIColorHex(cccccc)
#define TWO_NINW_A_7_FF UIColorHex(29A7FF)

#define LIGHTCOLOR COLOR(217,77,55)
#define MAIN_COLOR UIColorHex(161719);
#define DB9E62 [UIColor colorWithHexString:@"#26E9BD"]
#define TEXT_GRAY_COLOR [UIColor colorWithHexString:@"#999999"]
#define SELECTED_COLOR COLOR(255,255,255)
#define BACKGROUNDCOLOR UIColorHex(f5f5f5)
#define NORMAL_COLOR [UIColor colorWithHexString:@"#333333"]
#define RANDOMCOLOR COLOR(random()%255, random()%255, random()%255)


#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IPHONE_4  (IS_IPHONE && SCREEN_MAX_LENGTH == 480.0)
#define IPHONE_5  (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE_6  (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IPHONE_4and5  (IS_IPHONE && SCREEN_WIDTH  == 320.0)
#define IPHONEX_NO (IPHONE_4 || IPHONE_5 ||IPHONE_6 || IPHONE_6P)

#define BASEURL  @"http://bigcat.nat300.top/"//服务器网-后端
#define BASEURL_IMG  @"http://bigcat.nat300.top/issue_number/basic/app/"//服务器网-图片

#define BASEURL1 @"http://192.168.31.118:8080/tbkss/"//内网


#define isIos6  ([[[UIDevice currentDevice] systemVersion] floatValue]>=6.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
#define isIos7  ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
#define isIos8  ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
#define isIos9 ([[[UIDevice currentDevice] systemVersion] floatValue]>=9.0 )
#define isIos10 ([[[UIDevice currentDevice] systemVersion] floatValue]>=10.0)

#define LFont(s) [UIFont fontWithName:@"PingFangSC-Light" size:s]
#define RFont(s) [UIFont fontWithName:@"PingFangSC-Regular" size:s]
#define MFont(s) [UIFont fontWithName:@"PingFangSC-Medium" size:s]
#define SFont(s) [UIFont fontWithName:@"PingFangSC-Semibold" size:s]


//打印
#ifdef DEBUG
#define NSLog(...) NSLog(@"%s 第%d行 \n %@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define NSLog(...)
#endif


//导航栏的高度(包含状态栏)
#define NaviBarHeight  SCREEN_HEIGHT >= 812 ? 88 : 64
//状态栏的高度
#define StatusBarHeight  SCREEN_HEIGHT >= 812 ? 44 : 20
//标签栏的高度
#define TabBarHeight  SCREEN_HEIGHT >= 812 ? 83 : 49
//是否是iPhoneX
#define isIphoneX  SCREEN_HEIGHT >= 812 ? 1 : 0
//根据设计宽高比例(如iphone8)，获取新的宽和高
#define ReSize_UIHeight(float)   ((float)/375.0f*SCREEN_WIDTH)
#define ReSize_UIWidth(float)  ((float)/667.0f*SCREEN_HEIGHT)
//获取对应的宽和高
#define New_UIHeight(float)  ((float)/667.0f*SCREEN_HEIGHT)
#define New_UIWidth(float)  ((float)/375.0f*SCREEN_WIDTH)


//判空
//字符串是否为空
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
//数组是否为空
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
//字典是否为空
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys.count == 0)

#define DEFAULT_PHOTO   [UIImage imageNamed:@"xyt_user_photo"]
#define DEFAULT_IMG  [UIImage imageWithColor:RANDOMCOLOR] //首页默认的游戏图标

#define UIDS @"id "//用户的id
#define UNAME @"name "//用户的名称
#define UPHONE @"phone  "//用户
#define UPHOTO @"photo"//头像
#define SEX @"sex"//性别  1男 2女 0未知
#define FENSNUM @"fansNum"  //粉丝数量
#define U_BIRTHDAY @"birthday"  //生日
#define U_PRIVINCE @"province"  //地址--省
#define U_CITY @"city"  //地址--市
#define U_HOBBY @"hobby"  //爱好



#define TOKEN @"token"//

#define SERVER_TIME @"serverTime"//服务器时间

//接口名称
#define LOTTERY_TITLES @"lottery/titles" //首页的游戏数据


#endif /* Header_h */

