//
//  GameHome_rightCollectViewCell.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/27.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GameHome_rightCollectViewCell : UICollectionViewCell

@property (nonatomic,strong)UIButton *btn_rightCell;
@property (nonatomic,strong)UILabel *lab_current_id;//当前这个的id

@end

NS_ASSUME_NONNULL_END
