//
//  Popup_ListOfNotesTableVCell.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Popup_ListOfNotesTableVCell : UITableViewCell

@property (nonatomic,strong)UILabel *lab_take_laws ; //玩法
@property (nonatomic,strong)UILabel *lab_Odds ; //赔率
@property (nonatomic,strong)UITextField *txt_money ; //金额
@property (nonatomic,strong)UIButton *btn_delete ; //删除

@end


//批量修改
@interface Popup_ListOfNotesTableVCell_one : UITableViewCell

@property (nonatomic,strong)UILabel *lab_take_laws ; //玩法---------
@property (nonatomic,strong)UILabel *lab_Odds ; //赔率
@property (nonatomic,strong)UITextField *txt_money ; //金额---------
@property (nonatomic,strong)UIButton *btn_delete ; //删除

@end


NS_ASSUME_NONNULL_END
