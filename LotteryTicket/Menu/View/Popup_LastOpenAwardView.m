//
//  Popup_LastOpenAwardView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "Popup_LastOpenAwardView.h"
#import "TPopup_LastOpenAwardTableVCell.h"
@interface Popup_LastOpenAwardView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)UIImageView *img_choose_Menu;//选中下面的箭头
@property (nonatomic,strong)UIButton *selectedBtn;//选中

@end
@implementation Popup_LastOpenAwardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
        
        UIView *view_white = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 286)];
        view_white.backgroundColor = [UIColor whiteColor];
        [self addSubview:view_white];
        
        YYLabel *lab_Issue_number_title =  [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, 117, 40)];
        lab_Issue_number_title.text = @"期号";
        lab_Issue_number_title.textAlignment = NSTextAlignmentCenter;
        lab_Issue_number_title.font = MFont(13);
        lab_Issue_number_title.textColor = STATUS_BAR_BGCOLOR;
        [view_white addSubview:lab_Issue_number_title];
        //号码、大小、单双、总和
        NSArray *title_Array = @[@"号码",@"大小",@"单双",@"总和"];
        for (int i=0; i<title_Array.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(lab_Issue_number_title.right +( i*60), 0, 60, 40);
            [btn setTitle:title_Array[i] forState:UIControlStateNormal];
            [btn setTitleColor:SIX_COLOR forState:UIControlStateNormal];
            btn.contentHorizontalAlignment = 1;
            btn.titleLabel.font = MFont(13);
            [view_white addSubview:btn];
            btn.tag = i;
            [btn addTarget:self action:@selector(btnMoreChooseMenuClick:) forControlEvents:UIControlEventTouchUpInside];
            if (i==0) {
                btn.selected = YES;
                self.selectedBtn = btn;
            }
        }
        UIView *view_bg = [[UIView alloc]initWithFrame:CGRectMake(0, lab_Issue_number_title.bottom, view_white.width, 15)];
        view_bg.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
        [view_white addSubview:view_bg];
        
        UIImageView *img_bottom = [[UIImageView alloc]initWithFrame:CGRectMake(lab_Issue_number_title.right,lab_Issue_number_title.bottom+9
                                                                               , 30, 7)];
        img_bottom.image = [UIImage imageNamed:@"img_Check_past_winners"];
        self.img_choose_Menu = img_bottom;
        [view_white addSubview:img_bottom];
        
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 55, self.frame.size.width, view_white.height - 55) style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.rowHeight = 45.0;
        self.tableView.showsVerticalScrollIndicator = NO;
        [self.tableView setTableFooterView:[self createFooterViewFrame]];
        [self.tableView registerClass:[TPopup_LastOpenAwardTableVCell class] forCellReuseIdentifier:@"TPopup_LastOpenAwardTableVCell"];
        [view_white addSubview:self.tableView];
        
        //底部灰色的视图
        UIView *view_bottom_bg = [[UIView alloc]initWithFrame:CGRectMake(0, view_white.bottom,  self.frame.size.width,  self.frame.size.height - 280)];
        view_bottom_bg.backgroundColor = [UIColor clearColor];
        [self addSubview:view_bottom_bg];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHIddenView)];
        [view_bottom_bg addGestureRecognizer:tap];
    }
    return self;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TPopup_LastOpenAwardTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPopup_LastOpenAwardTableVCell" forIndexPath:indexPath];
    return cell;
}
- (void)tapHIddenView{
    if (self.tapActionBlock) {
        self.tapActionBlock();
    }
}

#pragma 创建一个尾部视图----查看更多
- (UIView *)createFooterViewFrame{
    UIView *view_footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 42)];
    view_footer.backgroundColor = [UIColor whiteColor];
    
    UILabel *lab_line = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH - 15, 1)];
    lab_line.backgroundColor = COLOR(231, 231, 231);
    [view_footer addSubview:lab_line];
    //查看所有
    UIButton *btn_look_more = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_look_more setTitle:@"查看更多" forState:UIControlStateNormal];
    [btn_look_more setTitleColor:UIColorHex(F9FCFF) forState:UIControlStateNormal];
    btn_look_more.titleLabel.font = MFont(13);
    btn_look_more.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_look_more.frame = CGRectMake((view_footer.width -74)/2, lab_line.bottom + 9, 74, 22);
    btn_look_more.layer.cornerRadius = 11.0;
    btn_look_more.layer.masksToBounds = YES;
    [btn_look_more addTarget:self action:@selector(btn_look_more_click) forControlEvents:UIControlEventTouchUpInside];
    [view_footer addSubview:btn_look_more];
    
    return view_footer;
}
#pragma 查看更多
- (void)btn_look_more_click{
    if (self.lookMoreBlock) {
        self.lookMoreBlock();
    }
}
#pragma btnMoreChooseMenuClick
- (void)btnMoreChooseMenuClick:(UIButton *)btn{
    if (btn.tag == self.selectedBtn.tag) {
        return;
    }
    self.selectedBtn.selected = NO;
    btn.selected = YES;
    self.selectedBtn = btn;
    [UIView animateWithDuration:0.25 animations:^{
        self.img_choose_Menu.centerX = btn.centerX - 12;
        
    }];
}
@end
