//
//  Popup_LastOpenAwardView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 往期开奖号
NS_ASSUME_NONNULL_BEGIN

@interface Popup_LastOpenAwardView : UIView

@property (nonatomic, copy) void (^tapActionBlock)(void);//背景点击事件回调

@property (nonatomic, copy) void (^lookMoreBlock)(void);//查看更多
@end

NS_ASSUME_NONNULL_END
