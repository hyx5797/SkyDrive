//
//  Popup_ListOfNotesTableVCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "Popup_ListOfNotesTableVCell.h"

@implementation Popup_ListOfNotesTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    
    self.lab_take_laws = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 135, 45)];
    self.lab_take_laws.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.lab_take_laws.layer.borderWidth =0.5;
    self.lab_take_laws.text = @"第一球 4";
    self.lab_take_laws.textColor = UIColorHex(555555);
    self.lab_take_laws.font = MFont(15);
    self.lab_take_laws.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_take_laws];
    
    
    self.lab_Odds = [[UILabel alloc]initWithFrame:CGRectMake(self.lab_take_laws.right, 0, 95, 45)];
    self.lab_Odds.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.lab_Odds.layer.borderWidth =0.5;
    self.lab_Odds.text = @"@9.95";
    self.lab_Odds.textColor = STATUS_BAR_BGCOLOR;
    self.lab_Odds.font = MFont(15);
    self.lab_Odds.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_Odds];
    
    UIView *view_money = [[UIView alloc]initWithFrame:CGRectMake(self.lab_Odds.right, 0, 95, 45)];
    view_money.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    view_money.layer.borderWidth =0.5;
    [self.contentView addSubview:view_money];
    
    self.txt_money = [[UITextField alloc]initWithFrame:CGRectMake(8, 7, 65, 32)];
    self.txt_money.layer.borderColor = STATUS_BAR_BGCOLOR.CGColor;
     self.txt_money.layer.borderWidth =1.0;
    self.txt_money.font = MFont(15);
    self.txt_money.textColor = UIColorHex(555555);
    self.txt_money.keyboardType = UIKeyboardTypeDecimalPad;
    [view_money addSubview:self.txt_money];
    
    self.btn_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn_delete.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.btn_delete.layer.borderWidth =0.5;
    [self.btn_delete setImage:[UIImage imageNamed:@"icon_notes_popup_delete"] forState:UIControlStateNormal];
    CGFloat wd = SCREEN_WIDTH - 40 - 135 - 95*2;
    self.btn_delete.frame = CGRectMake(view_money.right, 0, wd, 45);
    [self.contentView addSubview:self.btn_delete];
    
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end



//批量修改
@implementation Popup_ListOfNotesTableVCell_one
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    
    self.lab_take_laws = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 135, 35)];
    self.lab_take_laws.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.lab_take_laws.layer.borderWidth =0.5;
    self.lab_take_laws.text = @"批量修改";
    self.lab_take_laws.textColor = UIColorHex(555555);
    self.lab_take_laws.font = MFont(15);
    self.lab_take_laws.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_take_laws];
    
    
    self.lab_Odds = [[UILabel alloc]initWithFrame:CGRectMake(self.lab_take_laws.right, 0, 95, 35)];
    self.lab_Odds.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.lab_Odds.layer.borderWidth =0.5;
    self.lab_Odds.text = @"";
    self.lab_Odds.textColor = STATUS_BAR_BGCOLOR;
    self.lab_Odds.font = MFont(15);
    self.lab_Odds.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_Odds];
    
    UIView *view_money = [[UIView alloc]initWithFrame:CGRectMake(self.lab_Odds.right, 0, 95, 35)];
    view_money.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    view_money.layer.borderWidth =0.5;
    [self.contentView addSubview:view_money];
    
    self.txt_money = [[UITextField alloc]initWithFrame:CGRectMake(8, 7, 65, 22)];
    self.txt_money.layer.borderColor = STATUS_BAR_BGCOLOR.CGColor;
    self.txt_money.layer.borderWidth =1.0;
    self.txt_money.font = MFont(15);
    self.txt_money.textColor = UIColorHex(555555);
    self.txt_money.keyboardType = UIKeyboardTypeDecimalPad;
    [view_money addSubview:self.txt_money];
    
    self.btn_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn_delete.layer.borderColor = UIColorHex(E6E6E6).CGColor;
    self.btn_delete.layer.borderWidth =0.5;
    CGFloat wd = SCREEN_WIDTH - 40 - 135 - 95*2;
    self.btn_delete.frame = CGRectMake(view_money.right, 0, wd, 35);
    [self.contentView addSubview:self.btn_delete];
    
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
