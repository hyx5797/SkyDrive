//
//  HomebotCollectionView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "HomebotCollectionView.h"
#import "IODifferentTypeGamesModel.h"
@interface HomebotCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>

/**数据源*/
@property (nonatomic, strong) NSArray *itemAry;

/**界面*/
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign) NSInteger aryCount;


@end

static NSString *cellIdentifier = @"HomebotCollectionCell";
@implementation HomebotCollectionView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addContentView];
    }
    return self;
}

// 加载视图
- (void)addContentView {
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    CGFloat cellW = SCREEN_WIDTH/3;
    layout.itemSize = CGSizeMake(cellW, 105);
    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    
    _collectionView.backgroundColor = [UIColor whiteColor];
    
    _collectionView.dataSource = self;
    
    _collectionView.delegate = self;
    
    _collectionView.scrollEnabled = NO;
    
    [_collectionView registerClass:[HomebotCollectionCell class] forCellWithReuseIdentifier:cellIdentifier];
    
    [self addSubview:_collectionView];
    
}

#pragma mark -- collection view delegate & datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomebotCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    IODifferentTypeGamesModel *model = [self.itemAry objectAtIndex:indexPath.row];
    cell.modelGameType = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
     IODifferentTypeGamesModel *model = [self.itemAry objectAtIndex:indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendSelectAryCount:indexRow:addModel:)]) {
        
        [self.delegate sendSelectAryCount:self.aryCount indexRow:indexPath.row addModel:model];
    }
}

#pragma mark -- 刷新数据
- (void)loadCollectionViewDataWithAry:(NSArray *)ary aryCount:(NSInteger)aryCount {
    
    self.aryCount = aryCount;
    self.itemAry = [NSArray modelArrayWithClass:[IODifferentTypeGamesModel class] json:ary];
    [self.collectionView reloadData];
    
}

@end
