//
//  HomebotCollectionCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "HomebotCollectionCell.h"
@interface HomebotCollectionCell()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIImageView *titleImg;

@end

@implementation HomebotCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addContentView];
    }
    return self;
}

- (void)addContentView {
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    //    self.contentView.layer.cornerRadius=5;
    //    self.contentView.layer.shadowColor=[UIColor grayColor].CGColor;//阴影的颜色
    //    self.contentView.layer.shadowOffset=CGSizeMake(5, 5);//阴影偏移量
    //    self.contentView.layer.shadowOpacity=0.8;//阴影的透明度
    //    self.contentView.layer.shadowRadius=5;//阴影的圆角
    
    CGFloat view_W = self.contentView.frame.size.width;
    CGFloat view_H= self.contentView.frame.size.height;
    
    UIImageView *img_content = [[UIImageView alloc]initWithFrame:CGRectMake(0, 16, 52, 52)];
    img_content.contentMode = UIViewContentModeScaleAspectFit;
    img_content.image = DEFAULT_IMG;
    img_content.layer.cornerRadius = 26.0;
    img_content.layer.masksToBounds = YES;
    img_content.centerX = self.contentView.centerX;
    self.titleImg = img_content;
    [self.contentView addSubview:img_content];
    
    UILabel *lab_CurrentTitle = [[UILabel alloc]init];
    lab_CurrentTitle.frame=CGRectMake(0, img_content.bottom+7, view_W, 15);
    lab_CurrentTitle.textColor = SIX_COLOR;
    lab_CurrentTitle.textAlignment=NSTextAlignmentCenter;
    lab_CurrentTitle.font=RFont(14);
//    lab_CurrentTitle.text = @"北京赛车(PK10)";
    self.titleLab = lab_CurrentTitle;
    [self.contentView addSubview:lab_CurrentTitle];
    
    UILabel *lab_right_line = [[UILabel alloc]initWithFrame:CGRectMake(view_W-1, 1, 1, view_H - 1)];
    lab_right_line.backgroundColor = CCC_COLOR;
    [self.contentView addSubview:lab_right_line];
    
    UILabel *lab_bottom_line = [[UILabel alloc]initWithFrame:CGRectMake(0, view_H - 1, view_W, 1)];
    lab_bottom_line.backgroundColor = CCC_COLOR;
    [self.contentView addSubview:lab_bottom_line];
}
-(void)setModelGameType:(IODifferentTypeGamesModel *)modelGameType{
    _modelGameType = modelGameType;
    [self.titleImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURL_IMG, modelGameType.icon_uri]] placeholder:DEFAULT_IMG];
    self.titleLab.text = modelGameType.title;
}
@end
