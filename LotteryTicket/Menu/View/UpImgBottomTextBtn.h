//
//  UpImgBottomTextBtn.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpImgBottomTextBtn : UIButton

@property (nonatomic,strong)UIImageView *img_btnImage;
@property (nonatomic,strong)UILabel *lab_btnTitle;

@end

NS_ASSUME_NONNULL_END
