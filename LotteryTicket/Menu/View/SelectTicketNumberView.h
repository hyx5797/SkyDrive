//
//  SelectTicketNumberView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Masonry.h"
NS_ASSUME_NONNULL_BEGIN

@protocol SelectTicketNumberViewDelegate <NSObject>

- (void)sendBettingNumber:(CGFloat)beetingNumber;

@end

@interface SelectTicketNumberView : UIView<UITextFieldDelegate>{
    UIView *backView;
    UIButton *hasSelectedBtn;//已经选中的金额选项
    NSDecimalNumber *bettingNumber;//下注金额
}
@property(nonatomic,strong)UIView *view_Mask;//遮罩视图
@property(nonatomic,strong)UIScrollView *numberChoiceScrView;//选择金额的选项
@property(nonatomic,strong)UIButton *resetBtn;//重置
@property(nonatomic,strong)UITextField *inputNumberTxtF;//下注金额
@property(nonatomic,strong)UILabel *balanceLabel;//余额
@property(nonatomic,strong)UIButton *bettingBtn;//投注

@property(nonatomic,weak)id<SelectTicketNumberViewDelegate> delegate;

@property(nonatomic,strong)NSMutableArray *numberBtnMutAry;//存储金额选项的
@end

NS_ASSUME_NONNULL_END
