//
//  Popup_ListOfNotesView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 下注清单
NS_ASSUME_NONNULL_BEGIN

@interface Popup_ListOfNotesView : UIView

@property (nonatomic, strong) UIView *blackviewtemp;
@property (nonatomic, strong) UIButton *dismissBtn;
@property (nonatomic, strong) UIView *whiteView;
@property (nonatomic, strong) UIImageView *imgPhoto;
@property (nonatomic, strong) UIButton *btnCancle;

@property (nonatomic, copy) void (^tapActionBlock)(void);//图片点击事件回调


+ (instancetype)sharedInstance;

- (void)showWithUnityID:(NSInteger )count Complate:(void (^)(void))complate;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
