//
//  HomebotCollectionView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomebotCollectionCell.h"

NS_ASSUME_NONNULL_BEGIN
@protocol HomebotCollectionViewDelegate <NSObject>

- (void)sendSelectAryCount:(NSInteger )aryCount indexRow:(NSInteger )row addModel:(IODifferentTypeGamesModel *)model;

@end


@interface HomebotCollectionView : UIView

@property (nonatomic, weak) id<HomebotCollectionViewDelegate> delegate;

- (void)loadCollectionViewDataWithAry:(NSArray *)ary aryCount:(NSInteger )aryCount;

@end

NS_ASSUME_NONNULL_END
