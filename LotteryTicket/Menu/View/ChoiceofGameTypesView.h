//
//  ChoiceofGameTypesView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/2.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 更多游戏分类的选择
NS_ASSUME_NONNULL_BEGIN

@interface ChoiceofGameTypesView : UIView

@end


//分组头部的标题
@interface HeaderView : UICollectionReusableView
/** titleLabel */
@property (nonatomic,strong) UILabel *titleLabel;


@end


//集合视图的Cell
@interface ChoiceofGameTypesCollectCell:UICollectionViewCell

@end
NS_ASSUME_NONNULL_END
