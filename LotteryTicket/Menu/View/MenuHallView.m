//
//  MenuHallView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "MenuHallView.h"

@implementation MenuHallView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];

    self.backgroundColor = [UIColor whiteColor];
    [self createHallViewHeader];
    return self;
}
#pragma 创建头部的期数
- (void)createHallViewHeader{
    CGFloat navHt = NaviBarHeight;
    //导航栏
    XYTTopnavigationbarView *top_nav_bar_view = [[XYTTopnavigationbarView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, navHt)];
    [self addSubview:top_nav_bar_view];
    
    UIView *view_header_hall = [[UIView alloc]initWithFrame:CGRectMake(0, top_nav_bar_view.bottom, SCREEN_WIDTH, 100)];
    view_header_hall.backgroundColor = [UIColor whiteColor];
    [self addSubview:view_header_hall];
        //上一期
    YYLabel *lab_Last_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, 150, 60)];
    lab_Last_issue.text = @"11131601期";
    lab_Last_issue.textAlignment = NSTextAlignmentCenter;
//    lab_Last_issue.backgroundColor = [UIColor redColor];
    [view_header_hall addSubview:lab_Last_issue];
    //上一期号数
    YYLabel *lab_Last_issue_num = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Last_issue.right, 0, SCREEN_WIDTH - 170, 30)];
    lab_Last_issue_num.text = @"11131601";
//    lab_Last_issue_num.backgroundColor = [UIColor blueColor];
    [view_header_hall addSubview:lab_Last_issue_num];
    //上一期大小双龙
    YYLabel *lab_Last_issue_bigOrSmall = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Last_issue.right, lab_Last_issue_num.bottom, SCREEN_WIDTH - 170, 30)];
    lab_Last_issue_bigOrSmall.text = @"单、大、和";
//    lab_Last_issue_bigOrSmall.backgroundColor = [UIColor greenColor];
    [view_header_hall addSubview:lab_Last_issue_bigOrSmall];
    //过渡线
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Last_issue.bottom -1, SCREEN_WIDTH, 1)];
    labLine.backgroundColor = [UIColor grayColor];
    [view_header_hall addSubview:labLine];
    
    //下一期
    YYLabel *lab_Next_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0, lab_Last_issue.bottom, 150, 40)];
    lab_Next_issue.text = @"77731601期";
    lab_Next_issue.textAlignment = NSTextAlignmentCenter;
//    lab_Next_issue.backgroundColor = [UIColor greenColor];
    [view_header_hall addSubview:lab_Next_issue];
    
    //封盘
    YYLabel *lab_Sealed_disk = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Next_issue.right , lab_Last_issue.bottom, 100, 40)];
    lab_Sealed_disk.text = @"封盘：00:54";
    lab_Sealed_disk.textAlignment = NSTextAlignmentCenter;
//    lab_Sealed_disk.backgroundColor = [UIColor redColor];
    [view_header_hall addSubview:lab_Sealed_disk];
    //开奖
    YYLabel *lab_Lottery= [[YYLabel alloc]initWithFrame:CGRectMake(lab_Sealed_disk.right + 20, lab_Last_issue.bottom, 100, 40)];
    lab_Lottery.text = @"开奖：01:02";
    lab_Lottery.textAlignment = NSTextAlignmentCenter;
//    lab_Lottery.backgroundColor = [UIColor redColor];
    [view_header_hall addSubview:lab_Lottery];
    
    //声音
    UIButton *btn_voice = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 30,  lab_Last_issue.bottom, 30, 40)];
    btn_voice.backgroundColor = [UIColor redColor];
    [btn_voice addTarget:self  action:@selector(btn_voiceClick) forControlEvents:UIControlEventTouchUpInside];
    
    [view_header_hall addSubview:btn_voice];
}

//Click_HallBlock
-(void)btn_voiceClick{
    if (self.Click_HallBlock) {
        self.Click_HallBlock();
    }
}

@end
