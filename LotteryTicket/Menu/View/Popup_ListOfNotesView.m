//
//  Popup_ListOfNotesView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "Popup_ListOfNotesView.h"
#import "Popup_ListOfNotesTableVCell.h"

@interface  Popup_ListOfNotesView()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic,copy)void (^complate)(void);
@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic, strong)UITextField *textFiled;//评论输入框
//记录发布的时候的unityID
@property (nonatomic, assign)NSInteger index;
//合计
@property (nonatomic, strong)UILabel *lab_sum;

@property (nonatomic, strong)UIButton *btn_cancle;
@property (nonatomic, strong)UIButton *btn_submit;

@end
@implementation Popup_ListOfNotesView

+ (instancetype)sharedInstance{
    
    static Popup_ListOfNotesView * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
    });
    return instance;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        //上半部分添加透明button
        
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        //半透明视图
        self.whiteView = [[UIView alloc] init];
        self.whiteView.backgroundColor =UIColorHex(FAFCFF);
        self.whiteView.frame = CGRectMake(20, (SCREEN_HEIGHT - 216)/2, SCREEN_WIDTH - 40, 216);
        self.whiteView.layer.cornerRadius = 10.0;
        self.whiteView.layer.masksToBounds = YES;
        [self addSubview:self.whiteView];
        // 下注视图
        UILabel *lab_current_title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.whiteView.width, 36)];
        lab_current_title.text = @"下注清单";
        lab_current_title.textColor = UIColorHex(555555);
        lab_current_title.font = [UIFont fontWithName:@"PingFang-SC-Bold" size:16];
        lab_current_title.textAlignment = NSTextAlignmentCenter;
        [self.whiteView addSubview:lab_current_title];
        //玩法
        UILabel *lab_current_take_laws = [[UILabel alloc]initWithFrame:CGRectMake(5, lab_current_title.bottom, 135, 30)];
        lab_current_take_laws.backgroundColor = UIColorHex(F9F9F9);
        lab_current_take_laws.text = @"玩法";
        lab_current_take_laws.textColor = UIColorHex(555555);
        lab_current_take_laws.font = MFont(15);
        lab_current_take_laws.textAlignment = NSTextAlignmentCenter;
        lab_current_take_laws.layer.borderColor = UIColorHex(E6E6E6).CGColor;
        lab_current_take_laws.layer.borderWidth =0.5;
        [self.whiteView addSubview:lab_current_take_laws];
        //赔率
        UILabel *lab_current_Odds =[[UILabel alloc]initWithFrame:CGRectMake(lab_current_take_laws.right, lab_current_title.bottom, 95, 30)];
        lab_current_Odds.backgroundColor = UIColorHex(F9F9F9);
        lab_current_Odds.text = @"赔率";
        lab_current_Odds.textColor = UIColorHex(555555);
        lab_current_Odds.font = MFont(15);
        lab_current_Odds.textAlignment = NSTextAlignmentCenter;
        lab_current_Odds.layer.borderColor = UIColorHex(E6E6E6).CGColor;
        lab_current_Odds.layer.borderWidth =0.5;
        [self.whiteView addSubview:lab_current_Odds];
        //金额
        UILabel *lab_current_money= [[UILabel alloc]initWithFrame:CGRectMake(lab_current_Odds.right, lab_current_title.bottom, 95, 30)];
        lab_current_money.backgroundColor = UIColorHex(F9F9F9);
        lab_current_money.text = @"金额";
        lab_current_money.textColor = UIColorHex(555555);
        lab_current_money.font = MFont(15);
        lab_current_money.textAlignment = NSTextAlignmentCenter;
        lab_current_money.layer.borderColor = UIColorHex(E6E6E6).CGColor;
        lab_current_money.layer.borderWidth =0.5;
        [self.whiteView addSubview:lab_current_money];
        
        CGFloat wd =  self.whiteView.width - 10 - 135 - 95*2;
        UILabel *lab_current_demo= [[UILabel alloc]initWithFrame:CGRectMake(lab_current_money.right,lab_current_title.bottom, wd,30)];
        lab_current_demo.backgroundColor = UIColorHex(F9F9F9);
        lab_current_demo.layer.borderColor = UIColorHex(E6E6E6).CGColor;
        lab_current_demo.layer.borderWidth =0.5;
        [self.whiteView addSubview:lab_current_demo];
        
        //table
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 66, self.whiteView.width - 10, self.whiteView.height - 136) style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor whiteColor];
        [self.tableView setTableFooterView:[UIView new]];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.showsVerticalScrollIndicator = false;
        [self.whiteView addSubview:self.tableView];
        
        //合计
        UILabel *lab_sum = [[UILabel alloc]initWithFrame:CGRectMake(5,  self.tableView.bottom,  self.whiteView.width - 10, 34)];
        lab_sum.text = @"【合计】总注数 : 2   总金额 : 40";
        lab_sum.textColor = UIColorHex(555555);
        lab_sum.font = MFont(14);
        self.lab_sum = lab_sum;
        [self.whiteView addSubview:lab_sum];
        
        UIButton *btn_cancle = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_cancle.frame = CGRectMake(self.whiteView.width/2-17-76, lab_sum.bottom, 76, 25);
        [btn_cancle setBackgroundImage:[UIImage imageNamed:@"icon_notes_popup_cancel"] forState:UIControlStateNormal];
        self.btnCancle =btn_cancle;
        [self.btnCancle addTarget:self action:@selector(btnCloseClick) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteView addSubview:btn_cancle];
        
        UIButton *btn_submit = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_submit.frame = CGRectMake(btn_cancle.right + 34, lab_sum.bottom, 76, 25);
        [btn_submit setBackgroundImage:[UIImage imageNamed:@"icon_notes_popup_submit"] forState:UIControlStateNormal];
        self.btn_submit = btn_submit;
        [self.whiteView addSubview:btn_submit];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
        
          [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}
- (void)keyboardWillChange:(NSNotification *)sender
{
    CGFloat durition = [sender.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    CGRect keyboardRect = [sender.userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    
    CGFloat keyboardHeight = keyboardRect.size.height - self.whiteView.height +50;
    
    [UIView animateWithDuration:durition animations:^{
        
        self.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight);
        
    }];
}
//退出键盘UIView

-(void)keyboardWillHide:(NSNotification*)sender{
    
    
    
    CGFloat duration = [sender.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        
        self.transform = CGAffineTransformIdentity;
        
    }];
    
    
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row+1 <=_index) {
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"Popup_ListOfNotesTableVCell";
        //从tableView的一个队列里获取一个cell
        Popup_ListOfNotesTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[Popup_ListOfNotesTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        //批量
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"Popup_ListOfNotesTableVCell_one";
        //从tableView的一个队列里获取一个cell
        Popup_ListOfNotesTableVCell_one *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[Popup_ListOfNotesTableVCell_one alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }
  
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row+1 <=_index) {
        return 45;
    }else{
        return 35;
    }
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _index+1;
}
- (void)showWithUnityID:(NSInteger )count  Complate:(void (^)(void))complate{
    if (self.superview) {
        return;
    }
    _index = count;
    if (count>=4) {
        count = 4;
    }
    CGFloat ht = count *45+35 +136;
    self.whiteView.frame = CGRectMake(20, (SCREEN_HEIGHT -  ht)/2, SCREEN_WIDTH - 40, ht) ;
    self.tableView.height = count *45+35;
    self.lab_sum.mj_y=self.tableView.bottom;
    self.btnCancle.mj_y = self.btn_submit.mj_y = self.lab_sum.bottom;
    [self.tableView reloadData];
    //
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.whiteView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.whiteView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:nil];
    
}

#pragma mark - 关闭
- (void)btnCloseClick{
    [self dismiss];
}

#pragma mark - 确认打赏回调
- (void)sureBtnClick{
    if (self.complate) {
        self.complate();
    }
    [self dismiss];
}

-(void)dismiss
{
    if (!self.superview) {
        return;
    }
    [UIView animateWithDuration:0.15 animations:^{
        self.whiteView.xmg_y = SCREEN_HEIGHT;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.complate = nil;
    }];
}
@end
