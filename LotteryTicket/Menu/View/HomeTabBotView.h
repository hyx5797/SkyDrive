//
//  HomeTabBotView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^HomeTabBotViewBlock)(NSInteger selectNum);

@protocol HomeTabBotViewDelegate <NSObject>

/** 上传选择的视图的对应的高度 */
- (void)sendSelectCollectionViewHeight:(CGFloat )height;

- (void)sendSelectAryCount:(NSInteger )aryCount indexRow:(NSInteger )row addModel:(IODifferentTypeGamesModel *)model;

@end

@interface HomeTabBotView : UIView

/**底部默认视图高度*/
@property (nonatomic, assign) CGFloat defaultHeight;

/**顶部选择按钮*/
@property (nonatomic, strong) NSArray *titleAry;

/**滚动视图数据*/
@property (nonatomic, strong) NSMutableArray *scorllAry;

/**视图高度代理*/
@property (nonatomic, weak) id<HomeTabBotViewDelegate> delegate;

/**左右滑动选择视图返回*/
@property (nonatomic, copy) HomeTabBotViewBlock numBlock;

/**顶部按钮切换，修改底部视图对应显示界面*/
@property (nonatomic, assign) NSInteger topNum;

@end

NS_ASSUME_NONNULL_END
