//
//  GameHome_rightCollectViewCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/27.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "GameHome_rightCollectViewCell.h"

@implementation GameHome_rightCollectViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.contentView.backgroundColor = [UIColor darkGrayColor];
    
    self.btn_rightCell = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn_rightCell setImage:[UIImage imageNamed:@"icon_game_one"] forState:UIControlStateNormal];
    [self.btn_rightCell setTitleColor:UIColorHex(FF9600) forState:UIControlStateNormal];
    [self.btn_rightCell setTitleColor:UIColorHex(FF9600) forState:UIControlStateSelected];
    [self.btn_rightCell setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.btn_rightCell setBackgroundImage:[UIImage imageNamed:@"img_game_selected"] forState:UIControlStateSelected];
    self.btn_rightCell.frame = self.contentView.bounds;
    self.btn_rightCell.backgroundColor = [UIColor whiteColor];
     self.btn_rightCell.titleLabel.font = MFont(15);
      self.btn_rightCell.userInteractionEnabled = NO;
    [self.contentView addSubview:self.btn_rightCell];
    
    
    self.lab_current_id = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.contentView addSubview:self.lab_current_id];
    return self;
}
- (void)setLab_current_id:(UILabel *)lab_current_id{
    _lab_current_id = lab_current_id;
}
@end
