//
//  TPopup_LastOpenAwardTableVCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/28.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "TPopup_LastOpenAwardTableVCell.h"

@interface TPopup_LastOpenAwardTableVCell()

@property (nonatomic,strong)YYLabel *lab_Issue_number;//期号
@property (nonatomic,strong)YYLabel *lab_Winning_numbers;//中奖号码

@end
@implementation TPopup_LastOpenAwardTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.lab_Issue_number = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, 117, 45)];
    self.lab_Issue_number.text = @"190826024期";
    self.lab_Issue_number.textAlignment = NSTextAlignmentCenter;
    self.lab_Issue_number.font = MFont(13);
    self.lab_Issue_number.textColor = UIColorHex(555555);
    [self.contentView addSubview:self.lab_Issue_number];
    
    NSArray *img_Array = @[@"icon_game_one",@"icon_game_two",@"icon_game_three",@"icon_game_one",@"icon_game_three"];
    for (int i=0; i<img_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(self.lab_Issue_number.right +( i*24), 13, 19, 19);
        [btn setBackgroundImage:[UIImage imageNamed:img_Array[i]] forState:UIControlStateNormal];
        [self.contentView addSubview:btn];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
