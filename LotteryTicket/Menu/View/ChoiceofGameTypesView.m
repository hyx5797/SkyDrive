//
//  ChoiceofGameTypesView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/2.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "ChoiceofGameTypesView.h"

@interface ChoiceofGameTypesView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) UICollectionViewFlowLayout *collectionViewFlowLayout;


@end
@implementation ChoiceofGameTypesView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
     [self addSubview:self.collectionView];
    return self;
}
-(UICollectionView *)collectionView{
    if (_collectionView==nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc]init];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-20) collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.delegate = self;// 设置代理
        _collectionView.dataSource = self; // 设置数据源代理
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = UIColorHex(FBFDFF);
        // 注册cell的类型
        // 以代码的方式注册Cell的类型, 表示创建Cell的时候用这个类型来创建
        /*
         第一个参数: Cell的类型
         第二个参数: 重用标志
         */
                [_collectionView registerClass:[ChoiceofGameTypesCollectCell class] forCellWithReuseIdentifier:@"channelID"];

        // 3. 横向间距
        _collectionViewFlowLayout.minimumInteritemSpacing = 5;
        if (IPHONE_6 ||IPHONE_4and5) {
            // 4. 纵向间距
            _collectionViewFlowLayout.minimumLineSpacing = 5;
        }else{
            // 4. 纵向间距
            _collectionViewFlowLayout.minimumLineSpacing = 10;
        }

        
        
        
        //新建headerView类, 继承自UICollectionReusableView
        //注册header
        /*
         第一个参数:header视图对象的类型
         第二个参数:区分是header还是后面的footer
         // UICollectionElementKindSectionHeader表示header类型
         第三个参数:重用标志
         */
        [_collectionView registerClass:[HeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    }
    return _collectionView;
}
-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect{
    NSMutableArray* attributes = [[self.collectionViewFlowLayout layoutAttributesForElementsInRect:rect] mutableCopy];
    
    
    for (UICollectionViewLayoutAttributes *attr in attributes) {
        NSLog(@"%@", NSStringFromCGRect([attr frame]));
    }
    //从第二个循环到最后一个
    for(int i = 1; i < [attributes count]; ++i) {
        //当前attributes
        UICollectionViewLayoutAttributes *currentLayoutAttributes = attributes[i];
        //上一个attributes
        UICollectionViewLayoutAttributes *prevLayoutAttributes = attributes[i - 1];
        //我们想设置的最大间距，可根据需要改
        NSInteger maximumSpacing = 0;
        //前一个cell的最右边
        NSInteger origin = CGRectGetMaxX(prevLayoutAttributes.frame);
        //如果当前一个cell的最右边加上我们想要的间距加上当前cell的宽度依然在contentSize中，我们改变当前cell的原点位置
        //不加这个判断的后果是，UICollectionView只显示一行，原因是下面所有cell的x值都被加到第一行最后一个元素的后面了
        if(origin + maximumSpacing + currentLayoutAttributes.frame.size.width < self.collectionViewFlowLayout.collectionViewContentSize.width) {
            CGRect frame = currentLayoutAttributes.frame;
            frame.origin.x = origin + maximumSpacing;
            currentLayoutAttributes.frame = frame;
        }
    }
    
    return attributes;
}
#pragma mark - 遵守协议, 实现协议方法
/*
 // 返回每一个Cell的对象
 // UICollectionView上面的每一个Cell是UICollectionViewCell类型(或子类型)的对象
 // UICollectionViewCell也是一个视图
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // 从重用队列里面获取
    /*
     第一个参数: 重用id
     第二个参数: cell的位置
     */
    
    // UITableView      -> NSIndexPath:section row
    // UICollectionView -> NSIndexPath:section item
    ChoiceofGameTypesCollectCell *cell = (ChoiceofGameTypesCollectCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"channelID" forIndexPath:indexPath];

    return cell;
    
}
#pragma mark - 在布局对象的代理协议方法中设置header的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(SCREEN_WIDTH, 33);
}
#pragma mark - 在布局对象的代理协议方法中设置footer的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(SCREEN_WIDTH, 0);
}
#pragma mark - 返回header对象 UICollectionViewDataSource的协议方法(也可以用来返回footer对象)
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        // header类型
        
        // 从重用队列里面获取
        HeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        // 设置背景颜色
        header.backgroundColor = [UIColor clearColor];
        
        //        [header addSubview:header.titleLabel];
        // 显示数据
        header.titleLabel.text = [NSString stringWithFormat:@"第%c组", (int)(indexPath.section + 'A')];
        return header;
        
    }
    return nil;
}
//#pragma mark - UICollectionViewDelegate methods
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 4;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // 3.设置整个collectionView的内边距
    CGFloat paddingY = 5;
    CGFloat paddingX = 20;
    if (IPHONE_6 ||IPHONE_4and5 ||isIphoneX) {
            return UIEdgeInsetsMake(paddingY, paddingY, paddingY, paddingY);
    }else{
          return UIEdgeInsetsMake(paddingY, paddingX, paddingY, paddingX);
    }

    
}

//
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // 2.设置每个格子的尺寸
       return CGSizeMake(90, 33);
//    CGFloat nameW = [self withString:indexPath.section==0?self.stringArray1[indexPath.row]:self.stringArray2[indexPath.row]];
    
    
//    return sCGSizeMake((nameW<=80)?80:nameW, 30);
}

@end

//分组头部的标题
@interface HeaderView()

@property (nonatomic,strong)UILabel *left_line;

@property (nonatomic,strong)UILabel *right_line;

@end
@implementation HeaderView
-(UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 80)/2, 10, 80, 13)];
        _titleLabel.textColor = SIX_COLOR;
        _titleLabel.font = MFont(14);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}
-(UILabel *)left_line{
    if (_left_line == nil) {
        _left_line = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 80)/2 - 108, 16, 103, 1)];
        _left_line.backgroundColor = CCC_COLOR;
    }
    return _left_line;
}
-(UILabel *)right_line{
    if (_right_line == nil) {
        _right_line = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 80)/2  + 85, 16, 103, 1)];
        _right_line.backgroundColor = CCC_COLOR;
    }
    return _right_line;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.left_line];
    [self addSubview:self.right_line];
}
@end

//集合视图的Cell
@implementation ChoiceofGameTypesCollectCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.contentView.backgroundColor = UIColorHex(E6E6E6);
    self.contentView.layer.cornerRadius = 5.0;
    self.contentView.layer.masksToBounds = YES;
    
    
    UILabel * lab_title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    lab_title.text = @"重庆欢乐生肖";
    lab_title.textColor = UIColorHex(666666);
    lab_title.textAlignment =NSTextAlignmentCenter;
    lab_title.font = MFont(14);
    [self.contentView addSubview:lab_title];
    return self;
}

@end
