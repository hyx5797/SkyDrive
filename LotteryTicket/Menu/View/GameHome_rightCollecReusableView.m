//
//  GameHome_rightCollecReusableView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/27.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "GameHome_rightCollecReusableView.h"

@implementation GameHome_rightCollecReusableView
-(instancetype)initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    
    
    self.lab_current_title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    self.lab_current_title.text = @"第一球";
    self.lab_current_title.textAlignment = NSTextAlignmentCenter;
    self.lab_current_title.backgroundColor = UIColorHex(F5F5F5);
    self.lab_current_title.textColor = SIX_COLOR;
    self.lab_current_title.font = MFont(14);
    [self addSubview:self.lab_current_title];
    
    
    return self;
}
@end
