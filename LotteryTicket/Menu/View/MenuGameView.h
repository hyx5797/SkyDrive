//
//  MenuGameView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 首页的游戏界面
NS_ASSUME_NONNULL_BEGIN

@interface MenuGameView : UIView

//选中游戏的回调
@property (copy,nonatomic)void (^cellClick_GameBlock)(void);

@end

NS_ASSUME_NONNULL_END
