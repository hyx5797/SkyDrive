//
//  HomeTabBotView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "HomeTabBotView.h"
#import "HomebotCollectionView.h"
@interface HomeTabBotView()<UIScrollViewDelegate, HomebotCollectionViewDelegate>


/** 底部背景滚动视图 */
@property (nonatomic, strong) UIScrollView *bgScrollView;

/** 是否需要监听scrollview 滚动代理 */
@property (nonatomic, assign) NSInteger isNeedScroll;

/** 存储对应界面的高度 */
@property (nonatomic, strong) NSMutableArray *heightAry;



@end
@implementation HomeTabBotView

#pragma mark -- 懒加载视图
- (UIScrollView *)bgScrollView {
    
    if (!_bgScrollView) {
        
        _bgScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _bgScrollView.pagingEnabled = YES;
        _bgScrollView.delegate = self;
        _bgScrollView.pagingEnabled = YES;
        _bgScrollView.scrollsToTop = false;
        _bgScrollView.bounces = false;
        _bgScrollView.showsVerticalScrollIndicator = NO;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
        _bgScrollView.contentSize = CGSizeMake(kScreenWidth,_bgScrollView.frame.size.height);
        _bgScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }
    return _bgScrollView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addcontentView];
    }
    
    return self;
}

// 加载视图
- (void)addcontentView {
    
    _isNeedScroll = 0;
    [self addSubview:self.bgScrollView];
    
    //    TQ_botCollectionView *collecton = [[TQ_botCollectionView alloc] initWithFrame:self.bounds];
    //    [self.bgScrollView addSubview:collecton];
}

#pragma mark -- setter
- (void)setTitleAry:(NSArray *)titleAry {
    
    _titleAry = titleAry;
    self.bgScrollView.contentSize = CGSizeMake(titleAry.count * kScreenWidth, self.frame.size.height);
    self.heightAry = [NSMutableArray array];
    
    [titleAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        CGFloat height = ([self.scorllAry[idx] count]/ 3 ) * 105; // 默认单元格100 高度 + 1 个间隙
        [self.heightAry addObject:@(height)];
        
        HomebotCollectionView *collecton = [[HomebotCollectionView alloc] initWithFrame:CGRectMake(idx *kScreenWidth, 0, kScreenWidth, height > self.defaultHeight ? height : self.defaultHeight )];
        collecton.tag = idx;
        collecton.delegate = self;
        [collecton loadCollectionViewDataWithAry:self.scorllAry[idx] aryCount:idx];
        [self.bgScrollView addSubview:collecton];
        
        
        
        // 设置第一个视图的高度  超过默认高度就重新设置， 没有超过， 设置为默认高度
        
        if (idx == 0) {
            
            if (height > self.defaultHeight) {
                //                collecton.frame = CGRectMake(0, 0, kScreenWidth, height);
                self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, height);
                self.height = height;
            }
            else{
                //                collecton.frame = CGRectMake(0, 0, kScreenWidth, self.defaultHeight);
                self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, self.defaultHeight);
                self.height = self.defaultHeight;
            }
        }
        else {
            
        }
        
    }];
    
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendSelectCollectionViewHeight:)]) {
        
        
        CGFloat height = [self.heightAry.firstObject floatValue];
        
        if (height > self.defaultHeight) {
            
            [self.delegate sendSelectCollectionViewHeight:height];
        }
        else{
            [self.delegate sendSelectCollectionViewHeight:self.defaultHeight];
        }
        
    }
}

// 顶部按钮点击后修改对应的视图
- (void)setTopNum:(NSInteger)topNum {
    
    _topNum = topNum;
    
    _isNeedScroll = 1;
    
    
    [UIView animateWithDuration:0.1 animations:^{
        
        [self.bgScrollView setContentOffset:CGPointMake(topNum * kScreenWidth, 0)];
        
        CGFloat height = [self.heightAry[topNum] floatValue];
        
        UICollectionView *collection = [self.bgScrollView viewWithTag:topNum];
        
        if (height > self.defaultHeight) {
            //            collection.frame = CGRectMake(topNum * kScreenWidth, 0, kScreenWidth, height);
            self.height = height;
            self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, height);
            [self.delegate sendSelectCollectionViewHeight:height];
        }
        else{
            //            collection.frame = CGRectMake(topNum * kScreenWidth, 0, kScreenWidth, self.defaultHeight);
            self.height = self.defaultHeight;
            self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, self.defaultHeight);
            [self.delegate sendSelectCollectionViewHeight:self.defaultHeight];
        }
        
        
    } completion:^(BOOL finished) {
        
        self.isNeedScroll = 0;
    }];
    
    
}


#pragma mark -- scrollview delegate & data
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGPoint offset = scrollView.contentOffset;
    
    NSInteger num = offset.x / kScreenWidth;
    NSLog(@" -------- %ld", num);
    
    if (_isNeedScroll == 0) {
        
        [self refreshLoadScrollCollectionHeightWithNum:num];
        if (self.numBlock) {
            
            self.numBlock(num);
        }
    }
    
}

- (void)refreshLoadScrollCollectionHeightWithNum:(NSInteger )num {
    
    CGFloat height = [self.heightAry[num] floatValue];
    
    UICollectionView *collection = [self.bgScrollView viewWithTag:num];
    
    if (height > self.defaultHeight) {
        //        collection.frame = CGRectMake(num * kScreenWidth, 0, kScreenWidth, height);
        self.height = height;
        self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, height);
        [self.delegate sendSelectCollectionViewHeight:height];
    }
    else{
        //        collection.frame = CGRectMake(num * kScreenWidth, 0, kScreenWidth, self.defaultHeight);
        self.height = self.defaultHeight;
        self.bgScrollView.frame = CGRectMake(0, 0, kScreenWidth, self.defaultHeight);
        [self.delegate sendSelectCollectionViewHeight:self.defaultHeight];
    }
}

- (void)sendSelectAryCount:(NSInteger)aryCount indexRow:(NSInteger)row addModel:(nonnull IODifferentTypeGamesModel *)model{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendSelectAryCount:indexRow:addModel:)]) {
        
        [self.delegate sendSelectAryCount:aryCount indexRow:row addModel:model];
    }
}
@end
