//
//  GameHome_leftTbViewCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/27.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "GameHome_leftTbViewCell.h"

@implementation GameHome_leftTbViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.btn_leftCell = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn_leftCell setImage:[UIImage imageNamed:@"icon_game_dot_normal"] forState:UIControlStateNormal];
    [self.btn_leftCell setImage:[UIImage imageNamed:@"icon_game_dot_selected"] forState:UIControlStateSelected];
    [self.btn_leftCell setTitle:@"  1-5球" forState:UIControlStateNormal];
    [self.btn_leftCell setTitle:@"  1-5球" forState:UIControlStateSelected];
    [self.btn_leftCell setTitleColor:SIX_COLOR forState:UIControlStateNormal];
    [self.btn_leftCell setTitleColor:UIColorHex(FFFEFB) forState:UIControlStateSelected];
    [self.btn_leftCell setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.btn_leftCell setBackgroundImage:[UIImage imageWithColor:UIColorHex(458AE6)] forState:UIControlStateSelected];
    self.btn_leftCell.titleLabel.font = MFont(15);
    self.btn_leftCell.frame = CGRectMake(0, 0, 86, 41);
    self.btn_leftCell.userInteractionEnabled = NO;
    [self.contentView addSubview:self.btn_leftCell];
    return self;
}
-(void)setModel_childs:(ChildsItemModel *)model_childs{
    _model_childs = model_childs;
    [self.btn_leftCell setTitle:[NSString stringWithFormat:@"  %@",model_childs.title] forState:UIControlStateNormal];
    [self.btn_leftCell setTitle:[NSString stringWithFormat:@"  %@",model_childs.title] forState:UIControlStateSelected];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.btn_leftCell.selected = selected;
}

@end
