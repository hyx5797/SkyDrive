//
//  UpImgBottomTextBtn.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UpImgBottomTextBtn.h"

@implementation UpImgBottomTextBtn

- (instancetype)initWithFrame:(CGRect)frame{
    if (self= [super initWithFrame:frame] ) {
        
        CGFloat btnW = self.frame.size.width;
        CGFloat btnH= self.frame.size.height;
        
        self.img_btnImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, btnW, btnH- 30)];
        self.img_btnImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.img_btnImage];
        
        self.lab_btnTitle = [[UILabel alloc]init];
        self.lab_btnTitle.frame=CGRectMake(0, self.img_btnImage.bottom, btnW, 30);
        self.lab_btnTitle.textColor = UIColorHex(000000);
        self.lab_btnTitle.textAlignment=NSTextAlignmentCenter;
        self.lab_btnTitle.font=RFont(15);
        [self addSubview:self.lab_btnTitle];
    }
    return self;
}

@end
