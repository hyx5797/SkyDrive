//
//  GameHome_rightCollecReusableView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/27.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GameHome_rightCollecReusableView : UICollectionReusableView

@property (nonatomic,strong)UILabel *lab_current_title;

@end

NS_ASSUME_NONNULL_END
