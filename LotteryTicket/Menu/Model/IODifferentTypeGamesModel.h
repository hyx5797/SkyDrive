//
//  IODifferentTypeGamesModel.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <Foundation/Foundation.h>
//不同种类的游戏
NS_ASSUME_NONNULL_BEGIN

@interface IODifferentTypeGamesModel : NSObject

@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *code;
@property (nonatomic,strong)NSString *icon_uri;

@end

NS_ASSUME_NONNULL_END
