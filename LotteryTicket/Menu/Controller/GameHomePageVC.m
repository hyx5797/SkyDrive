//
//  GameHomePageVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "GameHomePageVC.h"
#import "LotteryTicket-Swift.h"
#import "GameHome_leftTbViewCell.h"
#import "GameHome_rightCollectViewCell.h"
#import "GameHome_rightCollecReusableView.h"
#import "Popup_LastOpenAwardView.h"
#import "SelectTicketNumberView.h"
#import "Popup_ListOfNotesView.h"
#import "OpeningResultVC.h"
#import "CountDown.h"
#import "UIImageView+WebCache.h"
#import "SDWebImage.h"
@interface GameHomePageVC ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,SelectTicketNumberViewDelegate>
//右侧表格当前是否正在向下滚动（即true表示手指向上滑动，查看下面内容）
@property (nonatomic,assign)bool rightTableIsScrollDown;
@property (nonatomic,assign)CGFloat rightTableLastOffsetY;
@property (nonatomic,strong)NSMutableArray *cellArray;
@property (nonatomic,strong)UITableView *leftTableView; //左边的表格
@property (nonatomic,strong)UICollectionView *collectionView; //右边的集合视图
//@property (nonatomic,assign)NSInteger temprandom;//随机个数
@property (nonatomic,strong)Popup_LastOpenAwardView *view_popupLastOpenAwardView;//往期中奖视图
@property (nonatomic,strong) SelectTicketNumberView *numberView;//筹码
@property (nonatomic,strong)IODifferentPlayGamesModel *model_data;//数据源
@property (nonatomic,strong)ChildsItemModel *model_childs_data;//右边collection数据
//倒计时或者未开盘
@property (nonatomic,strong)YYLabel *lab_Lottery;
@property (strong, nonatomic)  CountDown *count_bet_time;
@property (strong, nonatomic)  CountDown *count_Distance_prize_time;//距离开奖
@end

@implementation GameHomePageVC

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.isHidden_type == NO) {
        [self gameTypeShowOrHidden];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.rightTableIsScrollDown = YES;
    self.cellArray = [NSMutableArray new];
//    [self createHallViewHeader];//创建头部视图
        [self request_lottery];
    [self.view addSubview:self.leftTableView];
    [self.view addSubview:self.collectionView];
    [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]  animated:YES scrollPosition:UITableViewScrollPositionNone];
    _collectionView.contentInset = UIEdgeInsetsMake(0, 2.5, 0, 2.5);
    CGFloat navHt = NaviBarHeight;

    self.numberView = [[SelectTicketNumberView alloc] initWithFrame:CGRectMake(0, _collectionView.bottom, SCREEN_WIDTH, 100)];
    if (isIphoneX) {
        _collectionView.height = SCREEN_HEIGHT-205-navHt - 20;
        self.numberView.mj_y = _collectionView.bottom;
    }
    self.numberView.backgroundColor =UIColorHex(FBFDFF);
    [self.numberView.bettingBtn addTarget:self action:@selector(btnBettingClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: self.numberView];
     self.numberView.delegate = self;
    
    //导航栏右边的按钮
    UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(navbarRightItemClick)];
    self.navigationItem.rightBarButtonItem = right_Item;
    
    self.navigationItem.titleView.backgroundColor = [UIColor redColor];
//    self.navigationItem.titleView = [self createButtonWithTitle:self.modelGameType.title?:@""];
}
#pragma 导航栏事件
- (void)navbarRightItemClick{
    if (self.isHidden_type == NO) {
        [self gameTypeShowOrHidden];
    }
    [self tabHiddenOrShow];
}
#pragma mark ------下注View-------- SelectTicketNumberViewDelegate
- (void)sendBettingNumber:(CGFloat)beetingNumber{
    NSLog(@"下注金额：%.2f",beetingNumber);
}
#pragma 点击投注
- (void)btnBettingClick{
    [[Popup_ListOfNotesView sharedInstance]showWithUnityID:8 Complate:^{
        
    }];
}
#pragma 头部视图A
- (void)createHallViewHeader:(RecordModel *)model{
    _count_bet_time = [[CountDown alloc] init];
    _count_Distance_prize_time = [[CountDown alloc] init];
    UIView *view_header_hall = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 104)];
    view_header_hall.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view_header_hall];
    //上一期
    YYLabel *lab_Last_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, 100, 58)];
    lab_Last_issue.text = [NSString stringWithFormat:@"%@期",model.issueNumber];
    lab_Last_issue.textAlignment = NSTextAlignmentCenter;
    lab_Last_issue.font = MFont(15);
    lab_Last_issue.textColor = STATUS_BAR_BGCOLOR;
    [view_header_hall addSubview:lab_Last_issue];
    //上一期号数
    NSArray *img_Array = [model.resultNumber componentsSeparatedByString:@","];
//  @[@"icon_game_one",@"icon_game_two",@"icon_game_three",@"icon_game_one",@"icon_game_three"];
    for (int i=0; i<img_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(lab_Last_issue.right +( i*27), 9, 22, 22);
//        [btn setBackgroundImage:[UIImage imageNamed:img_Array[i]] forState:UIControlStateNormal];
//            [cell.btn_rightCell sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@@3x.png",BASEURL_IMG,model_TT.showText]] forState:(UIControlState)UIControlStateNormal];
        [btn sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@@3x.png",BASEURL_IMG,img_Array[i]]] forState:UIControlStateNormal];
//        [btn setTitle:img_Array[i] forState:UIControlStateNormal];
//        btn.backgroundColor = RANDOMCOLOR;
        [view_header_hall addSubview:btn];
    }
    //上一期大小双龙
    NSArray *str_Array = @[@"25",@"大",@"单",@"虎",@"牛5"];
    for (int i=0; i<str_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(lab_Last_issue.right +( i*28),35, 25, 22);
        [btn setTitle:str_Array[i] forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(2B74E2) forState:UIControlStateNormal];
        btn.titleLabel.font = RFont(14);
        btn.layer.cornerRadius = 3.0;
        btn.layer.masksToBounds=YES;
        btn.layer.borderColor = UIColorHex(6DA8F7).CGColor;
        btn.layer.borderWidth = 1.0;
        [view_header_hall addSubview:btn];
    }
    //查看往期的箭头
    UIButton *btn_lastOpenAward = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_lastOpenAward setImage:[UIImage imageNamed:@"icon_game_last_normal"] forState:UIControlStateNormal];
        [btn_lastOpenAward setImage:[UIImage imageNamed:@"icon_game_last_selected"] forState:UIControlStateSelected];
    btn_lastOpenAward.frame = CGRectMake(view_header_hall.width - 40, 0, 40, 40);
    [btn_lastOpenAward addTarget:self action:@selector(btn_lastOpenAwardClick:) forControlEvents:UIControlEventTouchUpInside];
    [view_header_hall addSubview:btn_lastOpenAward];
    //下一期 lab_Last_issue.bottom+17
    YYLabel *lab_Next_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0,74, 100, 16)];
    lab_Next_issue.text = [NSString stringWithFormat:@"%@期",model.nextIssueNumber];
    lab_Next_issue.textAlignment = NSTextAlignmentCenter;
    lab_Next_issue.font = MFont(15);
    lab_Next_issue.textColor = STATUS_BAR_BGCOLOR;
    [view_header_hall addSubview:lab_Next_issue];
    
    //文字
    YYLabel *lab_Sealed_disk = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Next_issue.right ,73, 60, 16)];
    lab_Sealed_disk.text = @"截止投注";
    lab_Sealed_disk.textAlignment = NSTextAlignmentCenter;
    lab_Sealed_disk.textColor = UIColorHex(FF9600);
    lab_Sealed_disk.font = MFont(13);
    [view_header_hall addSubview:lab_Sealed_disk];
    
    //倒计时或者未开盘
      __weak __typeof(self) weakSelf= self;
    YYLabel *lab_Lottery= [[YYLabel alloc]initWithFrame:CGRectMake(lab_Sealed_disk.right + 2,72, 100, 20)];
    lab_Lottery.textColor = UIColorHex(FF9600);
    lab_Lottery.layer.borderColor = UIColorHex(FF9E26).CGColor;
    lab_Lottery.layer.borderWidth = 1.0;
    lab_Lottery.layer.cornerRadius = 4.0;
    [_count_bet_time countDownWithStratTimeStamp:[UD objectForKey:SERVER_TIME] finishTimeStamp:model.nextStopLotteryTime completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        self.numberView.view_Mask.hidden = YES;
        NSString *str_one_time =  [XYTTools getHour:hour addMinute:minute addSecond:second];
        lab_Sealed_disk.text = @"截止投注";
        lab_Lottery.text =str_one_time;
        lab_Lottery.width = [XYTTools getWidthWithContent:str_one_time height:20 font:MFont(16)] + 14;
        if ([str_one_time isEqualToString:@"00:00"]) {
              self.numberView.view_Mask.hidden = NO;
//            [XYTTools currentdateInterval]
            [weakSelf.count_Distance_prize_time countDownWithStratTimeStamp:model.nextStopLotteryTime finishTimeStamp:model.nextLotteryTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
                lab_Sealed_disk.text = @"距离开奖";
                 lab_Sealed_disk.textColor = UIColorHex(00B914);
                lab_Lottery.textColor = UIColorHex(00B914);
                lab_Lottery.layer.borderColor = UIColorHex(00B914).CGColor;
                NSString *str_Two_time =  [XYTTools getHour:hour addMinute:minute addSecond:second];
                lab_Lottery.text =str_Two_time;
                lab_Lottery.width = [XYTTools getWidthWithContent:str_Two_time height:20 font:MFont(16)] + 14;
                  if ([str_Two_time isEqualToString:@"00:00"]) {
//                       self.numberView.view_Mask.hidden = YES;
                      lab_Sealed_disk.text = @"开奖中";
                      lab_Sealed_disk.textColor = UIColorHex(FF9600);
                      lab_Sealed_disk.font = MFont(16);
                      lab_Lottery.hidden = YES;
//                      刷新界面
                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                          // 3秒后异步执行这里的代码...
                           [weakSelf request_lottery];
                      });
                  }
            }];
        }
    }];
    lab_Lottery.font = MFont(16);
    lab_Lottery.textAlignment = NSTextAlignmentCenter;
    self.lab_Lottery = lab_Lottery;
    [view_header_hall addSubview:lab_Lottery];
    
    //过渡线
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, view_header_hall.bottom -1, SCREEN_WIDTH, 1)];
    labLine.backgroundColor =CCC_COLOR;
    [view_header_hall addSubview:labLine];
}

-(UITableView *)leftTableView{
    if (!_leftTableView) {
        CGFloat navHt = NaviBarHeight;
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 104, 86, SCREEN_HEIGHT - 205 -navHt) style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            _leftTableView.estimatedRowHeight = 0;
            _leftTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _leftTableView.backgroundColor = [UIColor whiteColor];
        _leftTableView.rowHeight = 41;
        _leftTableView.sectionHeaderHeight = 0;
        _leftTableView.sectionFooterHeight = 0;
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [_leftTableView setTableFooterView:[UIView new]];
//        [_leftTableView registerClass:[GameHome_leftTbViewCell class] forCellReuseIdentifier:@"GameHome_leftTbViewCell"];
    }
    return _leftTableView;
}
-(UICollectionView *)collectionView{

    if (!_collectionView) {
        SBCollectionViewFlowLayout * layout = [SBCollectionViewFlowLayout new];
        layout.minimumLineSpacing = 2;
        layout.minimumInteritemSpacing = 2;
        layout.itemSize =CGSizeMake((SCREEN_WIDTH - 93)/2, 42);
        layout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH - 86, 40);
              CGFloat navHt = NaviBarHeight;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(86, 104, SCREEN_WIDTH - 86, SCREEN_HEIGHT-205-navHt) collectionViewLayout:layout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.allowsMultipleSelection = YES;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = UIColorHex(F5F5F5);
        _collectionView.contentInset = UIEdgeInsetsMake(0, 0, 180, 0);
        [_collectionView registerClass:[GameHome_rightCollecReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GameHome_rightCollecReusableView"];
    }
    return _collectionView;
}

#pragma UItableVIew
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.model_data.games.childs count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"GameHome_leftTbViewCell";
    //从tableView的一个队列里获取一个cell
    GameHome_leftTbViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[GameHome_leftTbViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ChildsItemModel *model_childs = self.model_data.games.childs[indexPath.row];
    cell.model_childs = model_childs;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.cellArray removeAllObjects];

     ChildsItemModel *model_childs = self.model_data.games.childs[indexPath.row];
    self.model_childs_data = model_childs;
    [self.collectionView reloadData];
}
#pragma UicollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.model_childs_data.childs.count ==0) {
        return 1;
    }
    return self.model_childs_data.childs.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.model_childs_data.childs.count ==0) {
        return self.model_childs_data.odds.count;
    }else{
        ChildsItemTwoModel *model = self.model_childs_data.childs[section];
           return model.odds.count;
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [NSString stringWithFormat:@"testCell%li-%li", indexPath.section,indexPath.item];
    [collectionView registerClass:[GameHome_rightCollectViewCell class] forCellWithReuseIdentifier:identifier];
    GameHome_rightCollectViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    if (self.model_childs_data.childs.count ==0) {
        OddsItemModel  *model= self.model_childs_data.odds[indexPath.row];
        NSString *str_title = [XYTTools isPureInt:model.showText?:@""] ? @"":model.showText ;
          [cell.btn_rightCell setTitle:[NSString stringWithFormat:@"  %@%.2lf",str_title,model.odds] forState:UIControlStateNormal];
        cell.lab_current_id.text = model.ID;
        [cell.btn_rightCell sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@@3x.png",BASEURL_IMG,model.showText]] forState:(UIControlState)UIControlStateNormal];
    }else{
        ChildsItemTwoModel *model = self.model_childs_data.childs[indexPath.section];
          OddsItemModel  *model_TT= model.odds[indexPath.row];
         NSString *str_title = [XYTTools isPureInt:model_TT.showText?:@""] ? @"":model_TT.showText ;
        [cell.btn_rightCell setTitle:[NSString stringWithFormat:@"  %@%.2lf",str_title,model_TT.odds] forState:UIControlStateNormal];
        cell.lab_current_id.text = model_TT.ID;
          [cell.btn_rightCell sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@@3x.png",BASEURL_IMG,model_TT.showText]] forState:(UIControlState)UIControlStateNormal];
    }
    NSString *strTile = cell.lab_current_id.text;
    if ([_cellArray containsObject:strTile]) {
        cell.btn_rightCell.selected = YES;
    }else{
          cell.btn_rightCell.selected = NO;
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"集合视图%li-%li",indexPath.section,indexPath.item);
    GameHome_rightCollectViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.btn_rightCell.selected = YES;
      NSString *strTile = cell.lab_current_id.text;
    [_cellArray addObject:strTile];
    NSLog(@"数组中的选中个数--%li",_cellArray.count);
}
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    GameHome_rightCollectViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.btn_rightCell.selected = NO;
      NSString *strTile = cell.lab_current_id.text;
    if ([_cellArray containsObject:strTile]) {
        [_cellArray removeObject:strTile];
    }
    NSLog(@"数组中的剩余的个数--%li",_cellArray.count);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
     UICollectionReusableView *reusableView =nil;
    if (kind ==UICollectionElementKindSectionHeader) {
        
            GameHome_rightCollecReusableView *headerV = (GameHome_rightCollecReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GameHome_rightCollecReusableView" forIndexPath:indexPath];
        if (self.model_childs_data.childs.count ==0) {
            headerV.lab_current_title.text = [NSString stringWithFormat:@"%@",self.model_childs_data.title?:@""];
        }else{
            ChildsItemTwoModel *model = self.model_childs_data.childs[indexPath.section];
            headerV.lab_current_title.text = [NSString stringWithFormat:@"%@",model.title?:@""];
        }
            reusableView = headerV;
    }

    return reusableView;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
#pragma 查看往期中奖号码
- (void)btn_lastOpenAwardClick:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        self.view_popupLastOpenAwardView = [[Popup_LastOpenAwardView alloc]initWithFrame:CGRectMake(0, 104, SCREEN_WIDTH, SCREEN_HEIGHT  - 105)];
        self.view_popupLastOpenAwardView.tapActionBlock = ^{
            btn.selected = !btn.selected;
            self.view_popupLastOpenAwardView.hidden = YES;
        };
        self.view_popupLastOpenAwardView.lookMoreBlock = ^{
            //查看更多
            OpeningResultVC *vc = [OpeningResultVC new];
            [self.navigationController pushViewController:vc animated:YES];
        };
        [self.view addSubview:self.view_popupLastOpenAwardView];
    }else{
        self.view_popupLastOpenAwardView.hidden = YES;
    }
}

//标题栏上面的文字在左、图标再有
- (UIButton *)createButtonWithTitle:(NSString *)title{
    if (@available(iOS 11.0, *)){
        self.navigationItem.titleView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    // 创建标题按钮
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button setImage:[UIImage imageNamed:@"icon_title_drop_down"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_title_drop_up"] forState:UIControlStateSelected];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    button.titleLabel.font = MFont(18);
    [button sizeToFit];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -button.imageView.frame.size.width - button.frame.size.width + button.titleLabel.frame.size.width, 0, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -button.titleLabel.frame.size.width - button.frame.size.width + button.imageView.frame.size.width);
    
    [button addTarget:self action:@selector(newButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}
#pragma 游戏标题的点击
- (void)newButtonAction:(UIButton *)btn{
      btn.selected = !btn.selected;
    if (self.hidden == NO) {
        [self tabHiddenOrShow];
    }
    [self gameTypeShowOrHidden];
}

#pragma 获取上期和本期的期号
-(void)request_lottery{
    [YQHttpRequest get:nil url:[NSString stringWithFormat:@"%@lottery/%@?newest=true",BASEURL,self.modelGameType.code] successed:^(BOOL succ, id responseDic) {
        NSDictionary *dic = responseDic;
        IODifferentPlayGamesModel *model = [IODifferentPlayGamesModel modelWithJSON:dic[@"result"]];
        self.model_data = model;
        [self createHallViewHeader:model.record];
        ChildsItemModel *model_childs = self.model_data.games.childs[0];
        self.model_childs_data = model_childs;
        self.navigationItem.titleView = [self createButtonWithTitle:self.model_data.games.title?:@""];
        [self.leftTableView reloadData];
        [self.collectionView reloadData];
    } fail:^(NSError *error) {
        NSLog(@"=====%@",error);
    }];
}
@end

