//
//  MenuHallOrGameVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "MenuHallOrGameVC.h"
#import "MenuHallView.h" //大厅
#import "MenuGameView.h"
#import "UncertaintyView.h" //未结明细
#import "LeftView.h" //左侧边栏
#import "XYTRuleView.h" //游戏规则
#import "UpImgBottomTextBtn.h"
//#import "MenuGameCollectCell.h"
#import "MenuHallVC.h"

// 侧边栏的宽度
#define LEFT_WIDTH (SCREEN_WIDTH/5) *4

@interface MenuHallOrGameVC ()<UITabBarDelegate, UITabBarControllerDelegate,LeftViewDelegate>

@property(nonatomic, strong) LeftView *lefeView;
@property(nonatomic, strong) UIView *bgView;
@property (assign, nonatomic,getter=isHidden)  BOOL hidden;

@property (nonatomic,strong)MenuHallView *view_hallMenu; //大厅
@property (nonatomic,strong)MenuGameView *view_gameMenu; //游戏
@property (nonatomic,strong)UncertaintyView *view_UncertaintyView; //未结明细
@property (nonatomic,strong)XYTRuleView *view_XYTRuleView;//游戏规则

//@property (nonatomic,strong)UIButton *selectedBtn;//选中

//@property (nonatomic,assign)NSInteger count;//
@end

@implementation MenuHallOrGameVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorHex(EBEBEB);
//    _count = 5;
        [self initWithViewFrame];
}

#pragma 初始化大厅的视图
- (void)initWithViewFrame{
    NSLog(@"选中的下标---%li",self.tabBarController.selectedIndex);
    if (self.tabBarController.selectedIndex == 0) {
        self.view_gameMenu = [[MenuGameView alloc]initWithFrame:self.view.bounds];
        
        self.view_gameMenu.cellClick_GameBlock = ^{
            
            self.view_hallMenu = [[MenuHallView alloc]initWithFrame:self.view.bounds];
            self.view_hallMenu.Click_HallBlock = ^{
                self.view_hallMenu.hidden = YES;
            };
            [self.view addSubview:self.view_hallMenu];
            
        };
        [self.view addSubview:self.view_gameMenu];
    }else if (self.tabBarController.selectedIndex == 1){
        self.view_hallMenu = [[MenuHallView alloc]initWithFrame:self.view.bounds];
        self.view_hallMenu.Click_HallBlock = ^{
            self.view_hallMenu.hidden = YES;
        };
        [self.view addSubview:self.view_hallMenu];
    }else if (self.tabBarController.selectedIndex == 2){
        //未结明细
        self.view_UncertaintyView = [[UncertaintyView alloc]initWithFrame:self.view.bounds];
        [self.view addSubview:self.view_UncertaintyView];
    }else  if (self.tabBarController.selectedIndex == 3){
        [self tabHiddenOrShow];
    }
}
//侧边栏
- (void)tabHiddenOrShow
{
    self.hidden = !self.isHidden;
    
    if (self.lefeView == nil) {
        self.lefeView = [[LeftView alloc] initWithFrame:CGRectMake(-LEFT_WIDTH, 0, LEFT_WIDTH, SCREEN_HEIGHT)];
        self.lefeView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:self.lefeView];
    }
    if (self.bgView == nil) {
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT)];
        self.bgView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        [self.bgView addGestureRecognizer:tap];
    }
    
    CGRect leftFrame = self.lefeView.frame;
    if (self.isHidden == YES) {
        leftFrame.origin.x = -LEFT_WIDTH;
        [self.bgView removeFromSuperview];
    } else {
        [[UIApplication sharedApplication].keyWindow insertSubview:self.bgView belowSubview:self.lefeView];
        leftFrame.origin.x = 0;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.lefeView.frame = leftFrame;
        [self.view setNeedsLayout];
    }];
}

- (void)tapClick:(UITapGestureRecognizer *)tap
{
    [self tabHiddenOrShow];
}


#pragma mark - LeftViewDelegate 侧边栏选中改变视图
-(void)didClickChildButton:(NSString *)currentTitle{
    NSLog(@"当前选择的是===%@",currentTitle);
    [self tabHiddenOrShow];
        //游戏规则
//        self.tabBarController.selectedIndex =3;
        self.view_XYTRuleView = [[XYTRuleView alloc]initWithFrame:self.view.bounds];
        [self.view addSubview:self.view_XYTRuleView];
    
}

//
//#pragma 创建头部所有按钮
//- (void)createGameViewHeader{
//   CGFloat statusHt = StatusBarHeight;
//   CGFloat tabHt = TabBarHeight;
//
//   //最顶部的背景条
//   UIImageView *img_top_bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 210 + statusHt)];
//   img_top_bg.backgroundColor = COLOR(131, 191, 244);
//   [self.view addSubview:img_top_bg];
//
//   //logo介绍
//   UIImageView *img_logo_description = [[UIImageView alloc]initWithFrame:CGRectMake(20, statusHt+25, 130, 30)];
//   img_logo_description.backgroundColor = [UIColor redColor];
//   [img_top_bg addSubview:img_logo_description];
//
//   //用户昵称
//   UILabel *lab_user_name = [[UILabel alloc]initWithFrame:CGRectMake(150, statusHt+25, SCREEN_WIDTH - 170, 30)];
//   lab_user_name.text = @"(试玩)游客";
//   lab_user_name.textAlignment = NSTextAlignmentRight;
//   lab_user_name.font = MFont(17);
//   lab_user_name.textColor = [UIColor whiteColor];
//   [img_top_bg addSubview:lab_user_name];
//
//   //账户余额
//   UIButton *btn_account_money = [UIButton buttonWithType:UIButtonTypeCustom];
//   [btn_account_money setTitle:@"￥2000.00 " forState:UIControlStateNormal];
//   [btn_account_money setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//   btn_account_money.titleLabel.font = RFont(17);
//   btn_account_money.frame = CGRectMake(SCREEN_WIDTH - 150, lab_user_name.bottom, 130, 30);
//   btn_account_money.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//   [img_top_bg addSubview:btn_account_money];
//
//   //欢迎
//   UILabel *lab_welcome = [[UILabel alloc]initWithFrame:CGRectMake(0, btn_account_money.bottom + 5, SCREEN_WIDTH, 50)];
//   lab_welcome.text = @"欢迎光临 Welcome!";
//   lab_welcome.textColor = [UIColor whiteColor];
//   //    lab_welcome.backgroundColor = [UIColor redColor];
//   lab_welcome.textAlignment = NSTextAlignmentCenter;
//   lab_welcome.font = MFont(28);
//   [img_top_bg addSubview:lab_welcome];
//
//   //最近游戏、热门游戏
//   UIView *view_Lately_Popular = [[UIView alloc]initWithFrame:CGRectMake(15, lab_welcome.bottom + 15, SCREEN_WIDTH - 30, 160)];
//   view_Lately_Popular.backgroundColor = [UIColor whiteColor];
//   view_Lately_Popular.layer.cornerRadius = 12.0;
//   view_Lately_Popular.layer.masksToBounds = YES;
//   [self.view addSubview:view_Lately_Popular];
//
//   UIButton *btn_Lately_Popular = [UIButton buttonWithType:UIButtonTypeCustom];
//   [btn_Lately_Popular setTitle:@"最近游戏 " forState:UIControlStateNormal];
//   [btn_Lately_Popular setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//   btn_Lately_Popular.titleLabel.font = RFont(17);
//   btn_Lately_Popular.frame = CGRectMake(5, 0, 130, 30);
//   [view_Lately_Popular addSubview:btn_Lately_Popular];
//
//   //    NSArray *img_Array = @[@"",@"",@"",@""];
//   NSArray *lab_Array = @[@"SG飞艇",@"重庆乐生肖",@"澳洲幸运5",@"极速赛车"];
//   CGFloat btnW = view_Lately_Popular.width/lab_Array.count;
//   for (int i=0; i<lab_Array.count; i++) {
//       UpImgBottomTextBtn *btn = [[UpImgBottomTextBtn alloc]initWithFrame:CGRectMake(i*btnW,btn_Lately_Popular.bottom, btnW, 130)];
//       btn.img_btnImage.backgroundColor = RANDOMCOLOR;
//       btn.lab_btnTitle.text = lab_Array[i];
//       [view_Lately_Popular addSubview:btn];
//   }
//
//
//   //极速、时时、快开、全国、香港、棋牌
//   UIView *view_more_gamer = [[UIView alloc]initWithFrame:CGRectMake(15, view_Lately_Popular.bottom + 5, SCREEN_WIDTH - 30, SCREEN_HEIGHT - view_Lately_Popular.bottom - tabHt - 20 )];
//   view_more_gamer.backgroundColor = [UIColor whiteColor];
//   view_more_gamer.layer.cornerRadius = 12.0;
//   view_more_gamer.layer.masksToBounds = YES;
//   [self.view addSubview:view_more_gamer];
//
//   NSArray *lab_more_Array = @[@"极速",@"时时",@"快开",@"全国",@"香港",@"棋牌"];
//   CGFloat btn_moreW = view_more_gamer.width/lab_more_Array.count;
//   for (int i=0; i<lab_more_Array.count; i++) {
//       UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//       btn.frame = CGRectMake(i*btn_moreW, 0, btn_moreW, 50);
//       [btn setTitle:lab_more_Array[i] forState:UIControlStateNormal];
//       [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//       [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
//       [btn setBackgroundImage:[UIImage imageWithColor:UIColorHex(EBEBEB)] forState:UIControlStateNormal];
//       [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
//       btn.tag = i;
//       [btn addTarget:self action:@selector(btnMoreGameClick:) forControlEvents:UIControlEventTouchUpInside];
//       if (i==0) {
//           btn.selected = YES;
//           self.selectedBtn = btn;
//       }
//       [view_more_gamer addSubview:btn];
//   }
//
//   //CollectionView
//   UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
//   layout.minimumLineSpacing = 8;
//   layout.minimumInteritemSpacing = 8;
//   layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
//   CGFloat cellW = (view_more_gamer.width - 40)/4;
//   layout.itemSize = CGSizeMake(cellW, 130);
//   _collectionView_game = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, view_more_gamer.width, view_more_gamer.height-50) collectionViewLayout:layout];
//   _collectionView_game.backgroundColor = [UIColor whiteColor];
//   _collectionView_game.delegate = self;
//   _collectionView_game.dataSource = self;
//   _collectionView_game.showsVerticalScrollIndicator = NO;
//   _collectionView_game.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
//   [_collectionView_game registerClass:[MenuGameCollectCell class] forCellWithReuseIdentifier:@"MenuGameCollectCell"];
//   [view_more_gamer addSubview:_collectionView_game];
//}
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//   return _count;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//   MenuGameCollectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MenuGameCollectCell" forIndexPath:indexPath];
//
//   return cell;
//}
//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [self.navigationController pushViewController:[MenuHallVC new] animated:YES];
//}
//#pragma 类型的更换
//- (void)btnMoreGameClick:(UIButton *)btn{
//    if(self.selectedBtn == btn){
//
//    }else{
//        btn.selected = YES;
//        self.selectedBtn.selected = NO;
//    }
//    self.selectedBtn = btn;
//    if (self.selectedBtn.tag%2==0) {
//        _count = 5;
//    }
//    else{
//        _count = 10;
//    }
//    [self.collectionView_game reloadData];
//}

@end
