//
//  GameHomePageVC.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GameHomePageVC : XYTBaseViewController

@property (nonatomic,strong)IODifferentTypeGamesModel *modelGameType;

@end

NS_ASSUME_NONNULL_END
