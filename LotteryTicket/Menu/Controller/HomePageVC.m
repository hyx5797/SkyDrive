//
//  HomePageVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/20.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "HomePageVC.h"
#import "NewPagedFlowView.h"
#import "PGIndexBannerSubiew.h"
#import "SPPageMenu.h"
#import "HomeTabBotView.h"
#import "GameHomePageVC.h"
#import "LMJHorizontalScrollText.h"
#import "LTLoginVC.h"
@interface HomePageVC ()<UITableViewDelegate,UITableViewDataSource,NewPagedFlowViewDelegate, NewPagedFlowViewDataSource, UIScrollViewDelegate,SPPageMenuDelegate,HomeTabBotViewDelegate>

@property (nonatomic,strong)UITableView *tableView_game;
// trackerStyle:跟踪器的样式
@property (nonatomic,strong)SPPageMenu *pageMenu;
/**
 *  图片数组
 */
@property (nonatomic, strong) NSMutableArray *imageArray;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic,strong)UIView *footerView;//尾部视图

/**TQ create 属性*/
@property (nonatomic, strong) HomeTabBotView *botView;

/**底部视图默认高度*/
@property (nonatomic, assign) CGFloat botViewDefaultHeight;

/** 是否需要重设scroll 滚动代理 */
@property (nonatomic, assign) NSInteger isNeedScroll;

/** 滚动视图数据结构， 具体的根据你后台接口来， 组装一下 */
@property (nonatomic, strong) NSMutableArray *scorllAry;

@property (nonatomic, strong) LMJHorizontalScrollText * scrollText2_1;

@end

@implementation HomePageVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self request_lottery_titles];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];

    _isNeedScroll = 0;

//    self.scorllAry = [NSMutableArray arrayWithObjects:
//                      @[
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        @{@"title": @"数组1", @"img": @"img"},
//                        ],
//                      @[
//                        @{@"title": @"数组2", @"img": @"img"},
//                        @{@"title": @"数组2", @"img": @"img"},
//                        @{@"title": @"数组2", @"img": @"img"}
//
//                        ],
//                      @[
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"},
//                        @{@"title": @"数组3", @"img": @"img"}
//                        ],
//
//                      @[
//                        @{@"title": @"数组4", @"img": @"img"}
//                        ],
//                      @[
//                        @{@"title": @"数组5", @"img": @"img"}
//                        ],
//                      @[
////                        @{@"title": @"数组6", @"img": @"img"}
//                        ],
//                      @[
//                        @{@"title": @"数组7", @"img": @"img"}
//                        ],
//                      nil];
    
    self.imageArray = [[NSMutableArray alloc]initWithObjects:@"http://pic32.nipic.com/20130809/6029101_114411131320_2.jpg",@"http://img.zcool.cn/community/0157a75d2dc578a80120695cabcf00.jpg@1280w_1l_2o_100sh.jpg",@"http://pic.rmb.bdstatic.com/f54083119edfb83c4cfe9ce2eeebc076.jpeg", nil];
    
    NSLog(@"dajflajsld %d ----- %d ------%d", kScreenHeight - TabBarHeight, kScreenHeight, TabBarHeight );
    _tableView_game = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49) style:UITableViewStylePlain];
    _tableView_game.delegate = self;
    _tableView_game.dataSource = self;
    _tableView_game.showsVerticalScrollIndicator = false;
//    [_tableView_game setTableHeaderView:[self createHeaderView]];
//    [_tableView_game setTableFooterView: [self createrFooterView]];
//    [self.view addSubview:_tableView_game];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    return cell;
}
- (void)btnLoginOutClick{
    LTLoginVC *vc = [LTLoginVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
- (UIView *)createHeaderView{
    CGFloat stuHt = StatusBarHeight;
    CGFloat navHt = NaviBarHeight;
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    headerView.backgroundColor = WHITE_COLOR;
    
    UIView *header_bg = [[UIView alloc]initWithFrame:CGRectMake(0, -stuHt, SCREEN_WIDTH, 133 +navHt + 20)];
    header_bg.backgroundColor = STATUS_BAR_BGCOLOR;
    [headerView addSubview:header_bg];
    
    UIView *bg_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, navHt-stuHt)];
    bg_view.backgroundColor = [UIColor clearColor];
    [headerView addSubview:bg_view];
    //退出
    UIButton *btn_login_out = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_login_out setTitle:@"退出" forState:UIControlStateNormal];
    [btn_login_out setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_login_out.titleLabel.font = MFont(15);
    btn_login_out.frame = CGRectMake(bg_view.width - 60, 0, 60, bg_view.height);
    [btn_login_out addTarget:self action:@selector(btnLoginOutClick) forControlEvents:UIControlEventTouchUpInside];
    [bg_view addSubview:btn_login_out];
    //用户信息
    UIButton *btn_user_name = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_user_name setTitle:@"  13696199309" forState:UIControlStateNormal];
    [btn_user_name setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_user_name.titleLabel.font = MFont(14);
    [btn_user_name setImage:[UIImage imageNamed:@"icon_menu_userphoto"] forState:UIControlStateNormal];
    btn_user_name.frame = CGRectMake(bg_view.width/2, 0, bg_view.width/2 - 60, bg_view.height);
    btn_user_name.contentHorizontalAlignment = 2;
    [bg_view addSubview:btn_user_name];
    
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0,  bg_view.bottom+10, SCREEN_WIDTH, 165)];
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.1;
    pageFlowView.isCarousel = YES;
    pageFlowView.orientation = NewPagedFlowViewOrientationHorizontal;
    pageFlowView.isOpenAutoScroll = YES;
    [pageFlowView reloadData];
    [headerView addSubview:pageFlowView];
    //初始化pageControl
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, pageFlowView.bottom + 12, SCREEN_WIDTH, 8)];
    pageFlowView.pageControl = pageControl;
    pageControl.pageIndicatorTintColor =CCC_COLOR;
    pageControl.currentPageIndicatorTintColor = UIColorHex(FFB600);
    pageControl.numberOfPages = 3;
    [headerView addSubview:pageControl];
    
    //公告的滚动
    _scrollText2_1 = [[LMJHorizontalScrollText alloc] initWithFrame: CGRectMake(50, pageControl.bottom + 5, SCREEN_WIDTH - 50, 20)];
    _scrollText2_1.layer.cornerRadius = 3;
    _scrollText2_1.backgroundColor    = [UIColor whiteColor];
    _scrollText2_1.text               = @"尊敬的会员，您好！我司新增AG类游戏包括【百家乐】   尊敬的会员，您好！我司新增AG类游戏包括【百家乐】";
    _scrollText2_1.textColor          = SIX_COLOR;
    _scrollText2_1.textFont           =MFont(13);
    _scrollText2_1.speed              = 0.03;
    _scrollText2_1.moveDirection      = LMJTextScrollMoveLeft;
    _scrollText2_1.moveMode           = LMJTextScrollContinuous;
    [headerView addSubview:_scrollText2_1];
    
        [_scrollText2_1 move];
    
    UIView *img_notice_icon_bg = [[UIView alloc]initWithFrame:CGRectMake(0, pageControl.bottom + 5, 50, 20)];
    img_notice_icon_bg.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:img_notice_icon_bg];
    //公告的图标
    UIImageView *img_notice_icon= [[UIImageView alloc]initWithFrame:CGRectMake(15,  0, 20, 20)];
    img_notice_icon.image = [UIImage imageNamed:@"icon_menu_voice"];
    img_notice_icon.contentMode = UIViewContentModeScaleAspectFit;
    [img_notice_icon_bg addSubview:img_notice_icon];
    
//    self.dataArr = @[@"生活",@"影视中心",@"交通",@"电视剧",@"军事",@"综艺",@"时时彩"];
    
    // trackerStyle:跟踪器的样式
    SPPageMenu *pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, img_notice_icon_bg.bottom , SCREEN_WIDTH, 44) trackerStyle:SPPageMenuTrackerStyleLine];
    // 传递数组，默认选中第2个
    [pageMenu setItems:self.dataArr selectedItemIndex:0];
    pageMenu.trackerFollowingMode = SPPageMenuTrackerFollowingModeAlways;
    pageMenu.selectedItemTitleColor = UIColorHex(FFB600);
    pageMenu.unSelectedItemTitleColor = UIColorHex(555555);
    pageMenu.selectedItemTitleFont = pageMenu.unSelectedItemTitleFont = MFont(15);
    // 设置代理
    pageMenu.delegate = self;
    self.pageMenu = pageMenu;
    // 给pageMenu传递外界的大scrollView，内部监听self.scrollView的滚动，从而实现让跟踪器跟随self.scrollView移动的效果
    pageMenu.bridgeScrollView = self.scrollView;
    [headerView addSubview:pageMenu];
    _pageMenu = pageMenu;
    
    headerView.height = pageMenu.bottom +5;
    self.botViewDefaultHeight = SCREEN_HEIGHT - headerView.height;
    return headerView;
}
//TODO:创建尾部视图
- (UIView *)createrFooterView{
    self.footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.botViewDefaultHeight)];
    
    HomeTabBotView *view = [[HomeTabBotView alloc] initWithFrame:self.footerView.bounds];
    view.delegate = self;
    view.defaultHeight = self.botViewDefaultHeight;
    view.scorllAry = self.scorllAry;
    view.titleAry = self.dataArr;
    [self.footerView addSubview:view];
    
    view.numBlock = ^(NSInteger selectNum) {
        
        self.isNeedScroll = 1;
        
        // 修改顶部按钮视图切换
        self.pageMenu.selectedItemIndex = selectNum;
        
    };
    
    self.botView = view;
    
    return self.footerView;
}
#pragma mark NewPagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
//    return CGSizeMake(SCREEN_WIDTH - 70, (SCREEN_WIDTH - 70) * 9 / 16);
    
     return CGSizeMake(SCREEN_WIDTH - 70, 165);
}

- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
    
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    
    //    NSLog(@"ViewController 滚动到了第%ld页",pageNumber);
}

#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return self.imageArray.count;
    
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = [flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] init];
        bannerView.tag = index;
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
    }
    //在这里下载网络图片
    [bannerView.mainImageView setImageWithURL:[NSURL URLWithString:self.imageArray[index]] placeholder:[UIImage imageNamed:@"icon_login_header_bg"]];
    //    bannerView.mainImageView.image = self.imageArray[index];
    
    return bannerView;
}
#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedAtIndex:(NSInteger)index {
    NSLog(@"%zd",index);
    
    
}

- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    NSLog(@"%zd--Menu----->%zd",fromIndex,toIndex);
    
    NSLog(@"sekf;sd %d", _isNeedScroll);
    
    if (_isNeedScroll == 1) {
        
        // 底部滚动后， 不需要重设滚动的是的显示视图, 并归零
        _isNeedScroll = 0;
        return;
    }
    
    self.botView.topNum = toIndex;
}

- (void)pageMenu:(SPPageMenu *)pageMenu functionButtonClicked:(UIButton *)functionButton {
    
}
#pragma mark --懒加载
- (NSMutableArray *)imageArray {
    if (_imageArray == nil) {
        _imageArray = [NSMutableArray array];
    }
    return _imageArray;
}

#pragma mark -- TQ home bot delegate
- (void)sendSelectCollectionViewHeight:(CGFloat)height {
    
    [self.tableView_game beginUpdates];
    
    self.footerView.frame = CGRectMake(0, 0, kScreenWidth, height);
    
    self.botView.frame = self.footerView.bounds;
    
    [self.tableView_game setTableFooterView:self.footerView];
    
    [self.tableView_game endUpdates];
    
}

- (void)sendSelectAryCount:(NSInteger)aryCount indexRow:(NSInteger)row addModel:(nonnull IODifferentTypeGamesModel *)model
{
    
    NSLog(@"点击了第%d个数组第%d个数据", aryCount , row );
    GameHomePageVC *vc = [GameHomePageVC new];
    vc.modelGameType = model;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma 首页游戏数据的请求
- (void)request_lottery_titles{
    self.dataArr = [NSMutableArray new];
    self.scorllAry = [NSMutableArray new];
    [YQHttpRequest get:nil url:LOTTERY_TITLES successed:^(BOOL succ, id responseDic) {
        NSDictionary *dic = responseDic;
        if ([dic[@"code"] integerValue] == 0) {
            NSArray *ary_result = dic[@"result"][@"childs"];
            for (int i=0;i<ary_result.count;i++){
                [self.dataArr addObject:[ary_result[i] objectForKey:@"title"]];
                [self.scorllAry addObject:[ary_result[i] objectForKey:@"childs"]];
            }
            [self.tableView_game setTableHeaderView:[self createHeaderView]];
            [self.tableView_game setTableFooterView: [self createrFooterView]];
            [self.view addSubview:self.tableView_game];
        }
    } fail:^(NSError *error) {
        [MBProgressHUD showError:@"服务器连接异常"];
    }];
}
@end
