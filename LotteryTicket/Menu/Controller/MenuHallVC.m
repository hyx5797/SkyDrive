//
//  MenuHallVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/5.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "MenuHallVC.h"
#import <SocketRocket/SRWebSocket.h>

@interface MenuHallVC ()<UITableViewDelegate, UITableViewDataSource,SRWebSocketDelegate>

@property (nonatomic, strong) SRWebSocket *webSocket;

@property (nonatomic, strong) UITableView * leftTableView;

@property (nonatomic, strong) UITableView * rightTableView;
/**
 滑到了第几组
 */
@property (nonatomic, strong) NSIndexPath * currentIndexPath;
@end

@implementation MenuHallVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self webSocketConnect];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.webSocket.delegate = nil;
    [self.webSocket close];
    self.webSocket = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorHex(EBEBEB);
//    [self initWithFrame_];
    _currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.view  addSubview:self.leftTableView];
    [self.view  addSubview:self.rightTableView];
    
}
//Socket需要
//-(void)initWithFrame_{
//    [YQHttpRequest get:nil url:@"http://192.168.31.219:8080/lottery/1001" successed:^(BOOL succ, id responseDic) {
//        NSDictionary *dic = responseDic;
//        NSDictionary *result_resource = dic[@"result"][@"resource"];
//        [self createHallViewHeader:result_resource];
//    } fail:^(NSError *error) {
//        NSLog(@"=====%@",error);
//    }];
//}
//// 初始化
//- (void)webSocketConnect
//{
//    self.webSocket.delegate = self;
//    [self.webSocket close];
//
//    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"ws://192.168.31.219:8088/ws"]]];
//    self.webSocket.delegate = self;
//    self.title = @"Opening Connecting";
//    NSLog(@"------222------");
//    [self.webSocket open];
//}
//
//#pragma mark - SendButton Response
//- (void)sendAction:(id)sender {
//
//    // WebSocket
//    if (self.webSocket) {
////        NSDictionary *dic = @{@"code":@0,@"message":@"success",@"result":@{@"resource":@{@"issueNumber":@"50607258",@"result":@[@"8",@"2",@"2",@"7",@"9"],@"nextIssueNumber":@"50607259",@"nextIssueNumberTime":@1566373740000,@"nextIssueNumberStopTime":@1566373730000,@"lastToDay":@0,@"firstToDay":@0}}};
//
//
//        [self.webSocket send:@"asssss"];
//    }
//}
//
////连接成功
////代理方法实现
//#pragma mark - SRWebSocketDelegate
//- (void)webSocketDidOpen:(SRWebSocket *)webSocket
//{
//    NSLog(@"WebSocket Connect");
//    self.title = @"Connected!";
//}
//
//// 连接失败
//- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
//{
//    NSLog(@"Websocket Failed With Error --- %@", error);
//
//    self.title = @"Connection Failed! (see logs)";
//    self.webSocket = nil;
//}
//
//// 接收到新消息的处理
//- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
//{
//    NSLog(@"接收到Message-----%@",message);
//    NSDictionary *dic = [self dictionaryWithJsonString:message];
//    NSDictionary *result_resource;
//    if ([[dic allKeys] containsObject:@"code"]) {
//        result_resource = [dic[@"result"]objectForKey:@"resource"];
//    }else{
//        result_resource = dic[@"resource"];
//    }
//    [self createHallViewHeader:result_resource];
//}
//- (NSDictionary*)dictionaryWithJsonString:(NSString*)jsonString{
//    if(jsonString ==nil) {        return nil;    }
//     NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError*err;
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData                                                        options:NSJSONReadingMutableContainers                                                          error:&err];    if(err)    {        NSLog(@"json解析失败：%@",err);        return nil;    }    return dic;}
//// 连接关闭
//- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
//{
//    NSLog(@"Closed Reason:%@",reason);
//    self.title = @"Connection Closed! (see logs)";
//    self.webSocket = nil;
//}
//
//- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload
//{
//    NSString *reply = [[NSString alloc] initWithData:pongPayload encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",reply);
//}
//





#pragma mark -- Getter

- (UITableView *)leftTableView{
    
    if (_leftTableView == nil) {
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH / 3.0, SCREEN_HEIGHT - 100) style:UITableViewStyleGrouped];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
    }
    return _leftTableView;
}

- (UITableView *)rightTableView{
    
    if (_rightTableView == nil) {
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 3.0, 100, SCREEN_WIDTH / 3.0 * 2, SCREEN_HEIGHT - 100) style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
    }
    return _rightTableView;
}
#pragma mark -- UITableViewDelegate  UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        return 1;
    }
    return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView) {
        return 44;
    }
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        return 0.01;
    }
    return 30;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        return nil;
    }
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor orangeColor];
    label.text = [NSString stringWithFormat:@"第%ld组",section];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellID"];
    }
    
    if (tableView == _leftTableView) {
        cell.textLabel.text = [NSString stringWithFormat:@"第%ld组",indexPath.section];
        if (indexPath == _currentIndexPath) {
            cell.textLabel.textColor = [UIColor purpleColor];
        }else{
            cell.textLabel.textColor = [UIColor blackColor];
        }
        return cell;
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"第%ld组内的商品",(long)indexPath.section];
    return cell;
}

- (void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (tableView == _leftTableView) {
        _currentIndexPath = indexPath;
        [tableView reloadData];
        //        [_rightTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        //        _isSelected = YES;
    }
    
}

#pragma 创建头部的期数
- (void)createHallViewHeader:(NSDictionary *)dic{
    UIView *view_header_hall = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    view_header_hall.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view_header_hall];
    //上一期
    YYLabel *lab_Last_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, 120, 60)];
    lab_Last_issue.text = [NSString stringWithFormat:@"%@期",dic[@"issueNumber"]];
    lab_Last_issue.textAlignment = NSTextAlignmentCenter;
    [view_header_hall addSubview:lab_Last_issue];
    //上一期号数
    YYLabel *lab_Last_issue_num = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Last_issue.right, 0, SCREEN_WIDTH - 140, 30)];
    NSArray *num_array = dic[@"result"];
    NSString *str_Last_issue_num = @"";
    for (int  i=0; i<num_array.count; i++) {
        str_Last_issue_num=[str_Last_issue_num stringByAppendingFormat:@"%@",num_array[i]];
    }
    lab_Last_issue_num.text = str_Last_issue_num;
    [view_header_hall addSubview:lab_Last_issue_num];
    //上一期大小双龙
    YYLabel *lab_Last_issue_bigOrSmall = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Last_issue.right, lab_Last_issue_num.bottom, SCREEN_WIDTH - 140, 30)];
    lab_Last_issue_bigOrSmall.text = @"单、大、和";
    [view_header_hall addSubview:lab_Last_issue_bigOrSmall];
    //过渡线
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Last_issue.bottom -1, SCREEN_WIDTH, 1)];
    labLine.backgroundColor = UIColorHex(EBEBEB);
    [view_header_hall addSubview:labLine];
    
    //下一期
    YYLabel *lab_Next_issue = [[YYLabel alloc]initWithFrame:CGRectMake(0, lab_Last_issue.bottom, 120, 40)];
    lab_Next_issue.text =  [NSString stringWithFormat:@"%@期",dic[@"nextIssueNumber"]];
    lab_Next_issue.textAlignment = NSTextAlignmentCenter;
    [view_header_hall addSubview:lab_Next_issue];
    
    //封盘
    YYLabel *lab_Sealed_disk = [[YYLabel alloc]initWithFrame:CGRectMake(lab_Next_issue.right , lab_Last_issue.bottom, 100, 40)];
    lab_Sealed_disk.text = @"封盘：00:54";
    lab_Sealed_disk.textAlignment = NSTextAlignmentCenter;
    [view_header_hall addSubview:lab_Sealed_disk];
    //开奖
    YYLabel *lab_Lottery= [[YYLabel alloc]initWithFrame:CGRectMake(lab_Sealed_disk.right + 20, lab_Last_issue.bottom, 100, 40)];
    lab_Lottery.text = @"开奖：01:02";
    lab_Lottery.textAlignment = NSTextAlignmentCenter;
    [view_header_hall addSubview:lab_Lottery];
    
    //声音
    UIButton *btn_voice = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 50,  lab_Last_issue.bottom, 40, 40)];
    btn_voice.backgroundColor = [UIColor redColor];
    [btn_voice addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    [view_header_hall addSubview:btn_voice];
}
@end
