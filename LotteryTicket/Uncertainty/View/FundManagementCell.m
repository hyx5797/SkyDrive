//
//  FundManagementCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "FundManagementCell.h"
@interface FundManagementCell()

@property (nonatomic,strong)UILabel *labTitle;
@property (nonatomic,strong)UILabel *labStatus;
@property (nonatomic,strong)UILabel *labMoney;
@property (nonatomic,strong)UILabel *labTime;

@end

@implementation FundManagementCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    [self initWithViewFrame];
    return self;
}
#pragma 初始化表格cell基本样式
- (void)initWithViewFrame{
    self.labTitle = [[UILabel alloc]initWithFrame:CGRectMake(14, 17, 75, 14)];
    self.labTitle.text = @"XXX详情";
    self.labTitle.font = LFont(14);
    self.labTitle.textColor = UIColorHex(4C4C4C);
    [self.contentView addSubview:self.labTitle];
    
    self.labStatus = [[UILabel alloc]initWithFrame:CGRectMake(self.labTitle.right + 18 , 20, 43, 21)];
    self.labStatus.text = @"状态";
    self.labStatus.font = RFont(17);
    self.labStatus.textAlignment =NSTextAlignmentCenter;
    self.labStatus.textColor = UIColorHex(F9B529);
    self.labStatus.layer.borderColor = UIColorHex(F9B529).CGColor;
    self.labStatus.layer.borderWidth = 1.0;
    [self.contentView addSubview:self.labStatus];
    
    self.labTime = [[UILabel alloc]initWithFrame:CGRectMake(13 , self.labTitle.bottom + 5, 75, 12)];
    self.labTime.text = @"2019-9-15";
    self.labTime.font = RFont(12);
    self.labTime.textColor = NINE_COLOR;
    [self.contentView addSubview:self.labTime];
    
    self.labMoney = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 114 , 20.5, 100, 20)];
    self.labMoney.text = @"+7428.4";
    self.labMoney.font = RFont(18);
    self.labMoney.textColor = UIColorHex(F64F4F);
    self.labMoney.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.labMoney];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
