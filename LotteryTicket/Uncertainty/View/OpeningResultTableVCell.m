//
//  OpeningResultTableVCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/4.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "OpeningResultTableVCell.h"

@implementation OpeningResultTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat W_left = 134;
    YYLabel *lab_Issue_number_title =  [[YYLabel alloc]initWithFrame:CGRectMake(19, 10, W_left - 38, 42)];
    lab_Issue_number_title.text = @"190826024期\n15:30:00";
    lab_Issue_number_title.numberOfLines = 0;
    lab_Issue_number_title.font = MFont(13);
    lab_Issue_number_title.textColor = UIColorHex(555555);
    [self.contentView addSubview:lab_Issue_number_title];
    
    //上一期号数
    NSArray *img_Array = @[@"icon_game_one",@"icon_game_two",@"icon_game_three",@"icon_game_one",@"icon_game_three"];
    for (int i=0; i<img_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(W_left +( i*25), 9, 19, 19);
        [btn setBackgroundImage:[UIImage imageNamed:img_Array[i]] forState:UIControlStateNormal];
        [self.contentView  addSubview:btn];
    }
    //上一期大小双龙
    NSArray *str_Array = @[@"25",@"大",@"单",@"虎",@"牛5"];
    for (int i=0; i<str_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(W_left +( i*28),33, 21, 19);
        [btn setTitle:str_Array[i] forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(2B74E2) forState:UIControlStateNormal];
        btn.titleLabel.font = RFont(12);
        btn.layer.cornerRadius = 3.0;
        btn.layer.masksToBounds=YES;
        btn.layer.borderColor = UIColorHex(6DA8F7).CGColor;
        btn.layer.borderWidth = 1.0;
        [self.contentView  addSubview:btn];
    }
    
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


//大小、单双
@implementation OpeningResultBigOrSingleTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat W_left = 134;
    YYLabel *lab_Issue_number_title =  [[YYLabel alloc]initWithFrame:CGRectMake(19, 10, W_left - 38, 42)];
    lab_Issue_number_title.text = @"190826024期\n15:30:00";
      lab_Issue_number_title.numberOfLines = 0;
    lab_Issue_number_title.font = MFont(13);
    lab_Issue_number_title.textColor = UIColorHex(555555);
    [self.contentView addSubview:lab_Issue_number_title];
    
    //上一期号数
    NSArray *img_Array = @[@"icon_game_one",@"icon_game_two",@"icon_game_three",@"icon_game_one",@"icon_game_three"];
    for (int i=0; i<img_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(W_left +( i*30), 19, 24, 24);
        [btn setBackgroundImage:[UIImage imageNamed:img_Array[i]] forState:UIControlStateNormal];
        [self.contentView  addSubview:btn];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


//总和
@implementation OpeningResultAllSumTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat W_left = 134;
    YYLabel *lab_Issue_number_title =  [[YYLabel alloc]initWithFrame:CGRectMake(19, 10, W_left - 38, 42)];
    lab_Issue_number_title.text = @"190826024期\n15:30:00";
      lab_Issue_number_title.numberOfLines = 0;
    lab_Issue_number_title.font = MFont(13);
    lab_Issue_number_title.textColor = UIColorHex(555555);
    [self.contentView addSubview:lab_Issue_number_title];
    
    //上一期号数
    NSArray *img_Array = @[@"icon_game_num",@"icon_game_three",@"icon_game_one",@"icon_game_three"];
    for (int i=0; i<img_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(W_left +( i*31), 19, 24, 25);
       [btn setBackgroundImage:[UIImage imageNamed:img_Array[i]] forState:UIControlStateNormal];
        [self.contentView  addSubview:btn];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
