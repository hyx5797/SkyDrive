//
//  Popup_DetailsOfRechargeView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Popup_DetailsOfRechargeView : UIView


@property (nonatomic, strong) UIView *blackviewtemp;
@property (nonatomic, strong) UIButton *dismissBtn;
@property (nonatomic, strong) UIView *whiteView;
@property (nonatomic, strong) UIImageView *imgPhoto;
@property (nonatomic, strong) UIButton *btnCancle;
//
//@property (nonatomic, copy) void (^tapActionBlock)(void);//图片点击事件回调
//

+ (instancetype)sharedInstance;

- (void)showWithCurrentImgUrl:(NSString *)url addFileName:(NSString *) strTitle Complate:(void (^)(void))complate;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
