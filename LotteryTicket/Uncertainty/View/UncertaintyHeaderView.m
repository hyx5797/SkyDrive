//
//  UncertaintyHeaderView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UncertaintyHeaderView.h"

@implementation UncertaintyHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //日期、彩种期号、下注明细、下注金额、可赢金额
    CGFloat wD = SCREEN_WIDTH/5.0;
    CGFloat hT = self.bounds.size.height;
    self.backgroundColor = UIColorHex(1464CF);
    
    UIButton *btn_date =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_date setTitle:@"日期" forState:UIControlStateNormal];
    [btn_date setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_date.titleLabel.font = MFont(13);
    btn_date.frame = CGRectMake(0, 0, wD, hT);
    [self addSubview:btn_date];
    
    UIButton *btn_colour_period =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_colour_period setTitle:@"彩种期号" forState:UIControlStateNormal];
    [btn_colour_period setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_colour_period.titleLabel.font = MFont(13);
    btn_colour_period.frame = CGRectMake(btn_date.right, 0, wD, hT);
    [self addSubview:btn_colour_period];
    
    UIButton *btn_bets_detail =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_bets_detail setTitle:@"下注明细" forState:UIControlStateNormal];
    [btn_bets_detail setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_bets_detail.titleLabel.font = MFont(13);
    btn_bets_detail.frame = CGRectMake(btn_colour_period.right, 0, wD, hT);
    [self addSubview:btn_bets_detail];
    
    UIButton *btn_amount_bet =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_amount_bet setTitle:@"下注金额" forState:UIControlStateNormal];
    [btn_amount_bet setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_amount_bet.titleLabel.font = MFont(13);
    btn_amount_bet.frame = CGRectMake(btn_bets_detail.right, 0, wD, hT);
    [self addSubview:btn_amount_bet];
    
    UIButton *btn_win_losses =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_win_losses setTitle:@"可赢金额" forState:UIControlStateNormal];
    [btn_win_losses setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_win_losses.titleLabel.font = MFont(13);
    btn_win_losses.frame = CGRectMake(btn_amount_bet.right, 0, wD, hT);
    [self addSubview:btn_win_losses];
    
}
@end



#pragma 今日已结
@implementation OverTodayHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //游戏名称、笔数、下注金额、输赢金额、详情
    CGFloat wD = SCREEN_WIDTH/8.0;
    CGFloat hT = self.bounds.size.height;
    self.backgroundColor = UIColorHex(1464CF);
    
    UIButton *btn_Game_name =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_Game_name setTitle:@"游戏名称" forState:UIControlStateNormal];
    [btn_Game_name setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_Game_name.titleLabel.font = MFont(14);
    btn_Game_name.frame = CGRectMake(0, 0, wD*2, hT);
    [self addSubview:btn_Game_name];
    
    UIButton *btn_number_pens =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_number_pens setTitle:@"笔数" forState:UIControlStateNormal];
    [btn_number_pens setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_number_pens.titleLabel.font = MFont(14);
    btn_number_pens.frame = CGRectMake(btn_Game_name.right, 0, wD, hT);
    [self addSubview:btn_number_pens];
    
    UIButton *btn_amount_bets=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_amount_bets setTitle:@"下注金额" forState:UIControlStateNormal];
    [btn_amount_bets setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_amount_bets.titleLabel.font = MFont(14);
    btn_amount_bets.frame = CGRectMake(btn_number_pens.right , 0, wD*2, hT);
    [self addSubview:btn_amount_bets];
    
    UIButton *btn_wins_losses=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_wins_losses setTitle:@"输赢金额" forState:UIControlStateNormal];
    [btn_wins_losses setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_wins_losses.titleLabel.font = MFont(14);
    btn_wins_losses.frame = CGRectMake(btn_amount_bets.right , 0, wD*2, hT);
    [self addSubview:btn_wins_losses];
    
    UIButton *btn_details =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_details setTitle:@"详情" forState:UIControlStateNormal];
    [btn_details setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_details.titleLabel.font = MFont(14);
    btn_details.frame = CGRectMake(btn_wins_losses.right, 0, wD, hT);
    [self addSubview:btn_details];
    
}
@end


#pragma 今日已结详情、下注详情
@implementation OverTodayDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //彩种期号、下注明细、下注金额、输赢金额
    CGFloat wD = SCREEN_WIDTH/4.0;
    CGFloat hT = self.bounds.size.height;
    self.backgroundColor = UIColorHex(1464CF);
    
    UIButton *btn_colour_period =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_colour_period setTitle:@"彩种期号" forState:UIControlStateNormal];
    [btn_colour_period setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_colour_period.titleLabel.font = MFont(13);
    btn_colour_period.frame = CGRectMake(0, 0, wD, hT);
    [self addSubview:btn_colour_period];
    
    UIButton *btn_bets_details =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_bets_details setTitle:@"下注明细" forState:UIControlStateNormal];
    [btn_bets_details setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_bets_details.titleLabel.font = MFont(13);
    btn_bets_details.frame = CGRectMake(btn_colour_period.right, 0, wD, hT);
    [self addSubview:btn_bets_details];
    
    UIButton *btn_amount_bets=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_amount_bets setTitle:@"下注金额" forState:UIControlStateNormal];
    [btn_amount_bets setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_amount_bets.titleLabel.font = MFont(13);
    btn_amount_bets.frame = CGRectMake(btn_bets_details.right , 0, wD, hT);
    [self addSubview:btn_amount_bets];
    
    UIButton *btn_wins_losses=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_wins_losses setTitle:@"输赢金额" forState:UIControlStateNormal];
    [btn_wins_losses setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_wins_losses.titleLabel.font = MFont(13);
    btn_wins_losses.frame = CGRectMake(btn_amount_bets.right , 0, wD, hT);
    [self addSubview:btn_wins_losses];
    
}
@end

#pragma 下注记录
@implementation NoteRecordHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //时间、笔数、输赢
    CGFloat wD = SCREEN_WIDTH/3.0;
    CGFloat hT = self.bounds.size.height;
    self.backgroundColor = UIColorHex(1464CF);
    
    UIButton *btn_date =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_date setTitle:@"时间" forState:UIControlStateNormal];
    [btn_date setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_date.titleLabel.font = MFont(13);
    btn_date.frame = CGRectMake(0, 0, wD, hT);
    [self addSubview:btn_date];
    
    UIButton *btn_count =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_count setTitle:@"笔数" forState:UIControlStateNormal];
    [btn_count setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_count.titleLabel.font = MFont(13);
    btn_count.frame = CGRectMake(btn_date.right, 0, wD, hT);
    [self addSubview:btn_count];
    
    UIButton *btn_win_losses =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_win_losses setTitle:@"输赢" forState:UIControlStateNormal];
    [btn_win_losses setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    btn_win_losses.titleLabel.font = MFont(13);
    btn_win_losses.frame = CGRectMake(btn_count.right , 0, wD, hT);
    [self addSubview:btn_win_losses];
    
}
@end
