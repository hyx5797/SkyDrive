//
//  UncertaintyView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/14.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UncertaintyView.h"
#import "UncertaintyTableVCell.h"
#import "UncertaintyHeaderView.h"
#import "UncertaintyFooterView.h"

@interface UncertaintyView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;

@end

@implementation UncertaintyView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    [self createHallViewHeader];
    return self;
}
#pragma 创建头部的期数
- (void)createHallViewHeader{
    CGFloat navHt = NaviBarHeight;
    //导航栏
    XYTTopnavigationbarView *top_nav_bar_view = [[XYTTopnavigationbarView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, navHt)];
    top_nav_bar_view.strViewtTitle = @"未结明细";
    [self addSubview:top_nav_bar_view];
    
    [self addSubview:self.tableView];
    
    UncertaintyFooterView *footerView = [[UncertaintyFooterView alloc]initWithFrame:CGRectMake(0, _tableView.bottom, SCREEN_WIDTH, 200)];
    footerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:footerView];

}

#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        CGFloat navHt = NaviBarHeight;
        CGFloat tabH = TabBarHeight;
        CGFloat ht = SCREEN_HEIGHT - tabH   - 200 ;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, navHt, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 75.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        UncertaintyHeaderView *headerView = [[UncertaintyHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
        [_tableView setTableHeaderView:headerView];
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"UncertaintyTableVCell";
    
    //从tableView的一个队列里获取一个cell
    UncertaintyTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[UncertaintyTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
