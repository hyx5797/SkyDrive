//
//  UncertaintyHeaderView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UncertaintyHeaderView : UIView

@end


#pragma 今日已结
@interface OverTodayHeaderView : UIView

@end

#pragma 今日已结详情、下注详情
@interface OverTodayDetailHeaderView : UIView

@end

#pragma 下注记录
@interface NoteRecordHeaderView : UIView

@end
NS_ASSUME_NONNULL_END
