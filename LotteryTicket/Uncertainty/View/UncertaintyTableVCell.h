//
//  UncertaintyTableVCell.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UncertaintyTableVCell : UITableViewCell

@end


#pragma 今日已结
@interface OverTodayTableVCell : UITableViewCell

@end

#pragma 今日已结详情
@interface OverTodayDetailTableVCell : UITableViewCell

@end

#pragma 下注记录
@interface NoteRecordTableVCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
