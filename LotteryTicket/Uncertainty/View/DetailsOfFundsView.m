//
//  DetailsOfFundsView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "DetailsOfFundsView.h"
#import "FundManagementCell.h"
#import "Popup_DetailsOfRechargeView.h"

@interface DetailsOfFundsView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)UIButton *selectedBtn;//选中
@property (nonatomic,strong)UIView *view_BtnBlueLine;//按钮选择下面的那条线

@end
@implementation DetailsOfFundsView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self createHeaderView];
    [self addSubview:self.tableView];
    return self;
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        //        CGFloat tabH = TabBarHeight;
        CGFloat ht = self.bounds.size.height-50;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 61.0;
        _tableView.layer.shadowColor = [UIColor colorWithRed:57/255.0 green:132/255.0 blue:232/255.0 alpha:0.1].CGColor;
        _tableView.layer.shadowOffset = CGSizeMake(0,2);
        _tableView.layer.shadowOpacity = 1;
        _tableView.layer.shadowRadius = 7;
        _tableView.layer.cornerRadius = 8;
        _tableView.showsVerticalScrollIndicator = false;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"FundManagementCell";
    
    //从tableView的一个队列里获取一个cell
    FundManagementCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[FundManagementCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[Popup_DetailsOfRechargeView  sharedInstance]showWithCurrentImgUrl:@"" addFileName:@"" Complate:^{
        
    }];
}
#pragma 创建头部视图
- (void)createHeaderView{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(14, 0, SCREEN_WIDTH - 28, 50)];
    headerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:headerView];
    
    NSArray *lab_more_Array = @[@"充值记录",@"提款记录",@"红包记录"];
    CGFloat btn_moreW = headerView.width/lab_more_Array.count;
    for (int i=0; i<lab_more_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i*btn_moreW, 0, btn_moreW, 48);
        [btn setTitle:lab_more_Array[i] forState:UIControlStateNormal];
          [btn setTitleColor:SIX_COLOR forState:UIControlStateNormal];
        [btn setTitleColor:STATUS_BAR_BGCOLOR forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageWithColor:UIColorHex(ffffff)] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
        btn.titleLabel.font = RFont(15);
        btn.tag = i;
        [btn addTarget:self action:@selector(btnMoreGameClick:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:btn];
        if (i==0) {
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }else if (i == 2){
             btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
        if (i==0) {
            btn.selected = YES;
            self.selectedBtn = btn;
        }
    }
    //过渡线
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0,  48, headerView.width, 1)];
    labLine.backgroundColor = UIColorHex(E1E3EC);
    [headerView addSubview:labLine];

    UIView * underLine = [[UIView alloc] initWithFrame:CGRectMake((btn_moreW -90)/2, 48, 60, 2)];
    underLine.backgroundColor = STATUS_BAR_BGCOLOR;
    underLine.left =  self.selectedBtn.left;
    [headerView addSubview:underLine];
    self.view_BtnBlueLine = underLine;
}
#pragma 按钮事件
- (void)btnMoreGameClick:(UIButton *)btn{
    if (btn.tag == self.selectedBtn.tag) {
        return;
    }
    self.selectedBtn.selected = NO;
    btn.selected = YES;
    self.selectedBtn = btn;
    [UIView animateWithDuration:0.25 animations:^{
        if (btn.tag == 0) {
            self.view_BtnBlueLine.left = btn.left;
        }else if (btn.tag == 1){
            self.view_BtnBlueLine.centerX = btn.centerX;
        }else{
            self.view_BtnBlueLine.right = btn.right;
        }

    }];
}
@end


