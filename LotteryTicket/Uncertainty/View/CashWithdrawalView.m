//
//  CashWithdrawalView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "CashWithdrawalView.h"

@implementation CashWithdrawalView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
//    COLOR(237, 236, 237);
//    [self createNothingView];
    [self createViewFrame];
    return self;
}
#pragma 没有数据的样式
- (void)createNothingView{
    UIImageView *img_icon = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 206)/2, 50, 206, 178)];
    img_icon.image = [UIImage imageNamed:@"img_nothing_bank_card"];
    img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:img_icon];
    
    UILabel *lab_detail = [[UILabel alloc]initWithFrame:CGRectMake(30, img_icon.bottom + 24, SCREEN_WIDTH - 60, 12)];
    lab_detail.text = @"您还未绑定银行卡哟！";
    lab_detail.font = RFont(12);
    lab_detail.textColor = NINE_COLOR;
    lab_detail.textAlignment =NSTextAlignmentCenter;
    [self addSubview:lab_detail];
    
    UIButton *btn_banding_card = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_banding_card.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_banding_card.frame = CGRectMake((SCREEN_WIDTH - 116)/2, lab_detail.bottom + 11, 116, 21);
    [btn_banding_card setTitle:@"绑定银行卡" forState:UIControlStateNormal];
    [btn_banding_card setTitleColor:UIColorHex(ffffff) forState:UIControlStateNormal];
    btn_banding_card.titleLabel.font = RFont(12);
    btn_banding_card.layer.cornerRadius = 11.0;
    btn_banding_card.layer.masksToBounds = YES;
    [btn_banding_card addTarget:self action:@selector(btnBandingcCardClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_banding_card];
}
//绑定银行卡
-(void)btnBandingcCardClick{
    if (self.btnBandingBankBlock) {
        self.btnBandingBankBlock();
    }
}
#pragma 有数据的样式
- (void)createViewFrame{
    UIView *view_bank_num = [[UIView alloc]initWithFrame:CGRectMake(11, 6, SCREEN_WIDTH - 22, 50)];
    view_bank_num.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view_bank_num.layer.shadowColor = [UIColor colorWithRed:28/255.0 green:97/255.0 blue:201/255.0 alpha:0.1].CGColor;
    view_bank_num.layer.shadowOffset = CGSizeMake(0,4);
    view_bank_num.layer.shadowOpacity = 1;
    view_bank_num.layer.shadowRadius = 10;
    view_bank_num.layer.cornerRadius = 8;
    [self addSubview:view_bank_num];
    
    UIImageView *img_bank_icon = [[UIImageView alloc]initWithFrame:CGRectMake(14, 10, 30, 30)];
    img_bank_icon.backgroundColor = RANDOMCOLOR;
    [view_bank_num addSubview:img_bank_icon];
    
    UILabel *lab_bank_title = [[UILabel alloc]initWithFrame:CGRectMake(img_bank_icon.right + 11, 10, SCREEN_WIDTH - img_bank_icon.right - 22, 30)];
    lab_bank_title.text =@"中国工商银行（5954）";
    lab_bank_title.textColor = UIColorHex(333333);
    lab_bank_title.font = RFont(16);
    [view_bank_num addSubview:lab_bank_title];
    
    UIView *view_content = [[UIView alloc]initWithFrame:CGRectMake(11, view_bank_num.bottom + 12, SCREEN_WIDTH - 22, 193)];
    view_content.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view_content.layer.shadowColor = [UIColor colorWithRed:28/255.0 green:97/255.0 blue:201/255.0 alpha:0.1].CGColor;
    view_content.layer.shadowOffset = CGSizeMake(0,4);
    view_content.layer.shadowOpacity = 1;
    view_content.layer.shadowRadius = 10;
    view_content.layer.cornerRadius = 8;
    [self addSubview:view_content];
    //账户余额
    UILabel *lab_account_balance_title = [[UILabel alloc]initWithFrame:CGRectMake(14, 17, 100, 17)];
    lab_account_balance_title.text= @"账户余额:";
    lab_account_balance_title.font = RFont(16);
    lab_account_balance_title.textColor = NINE_COLOR;
    [view_content addSubview:lab_account_balance_title];
    
    UILabel *lab_account_balance = [[UILabel alloc]initWithFrame:CGRectMake(view_content.width/2, 18, view_content.width/2 - 15, 14)];
    lab_account_balance.text= @"￥8964.21";
    lab_account_balance.font = RFont(16);
    lab_account_balance.textColor = UIColorHex(333333);
    lab_account_balance.textAlignment = NSTextAlignmentRight;
    [view_content addSubview:lab_account_balance];
    
    UILabel *lab_line_one = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_account_balance_title.bottom + 17, view_content.width, 1)];
    lab_line_one.backgroundColor = UIColorHex(F0F0F0);
    [view_content addSubview:lab_line_one];
    
    //可提现金额
    UILabel *lab_Cashable_balance_title = [[UILabel alloc]initWithFrame:CGRectMake(14, lab_line_one.bottom + 16, 100, 17)];
    lab_Cashable_balance_title.text= @"可提现金额:";
    lab_Cashable_balance_title.font = RFont(16);
    lab_Cashable_balance_title.textColor = NINE_COLOR;
    [view_content addSubview:lab_Cashable_balance_title];
    
    UILabel *lab_Cashable_balance = [[UILabel alloc]initWithFrame:CGRectMake(view_content.width/2,  lab_line_one.bottom + 17, view_content.width/2 - 15, 14)];
    lab_Cashable_balance.text= @"￥990.20";
    lab_Cashable_balance.font = RFont(16);
    lab_Cashable_balance.textColor = UIColorHex(333333);
    lab_Cashable_balance.textAlignment = NSTextAlignmentRight;
    [view_content addSubview:lab_Cashable_balance];
    
    UILabel *lab_line_two = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Cashable_balance_title.bottom + 16, view_content.width, 1)];
    lab_line_two.backgroundColor = UIColorHex(F0F0F0);
    [view_content addSubview:lab_line_two];
    
    //提现金额
    UILabel *lab_Cash_withdrawal_title = [[UILabel alloc]initWithFrame:CGRectMake(14, lab_line_two.bottom + 17, 100, 17)];
    lab_Cash_withdrawal_title.text= @"提现金额:";
    lab_Cash_withdrawal_title.font = RFont(16);
    lab_Cash_withdrawal_title.textColor = NINE_COLOR;
    [view_content addSubview:lab_Cash_withdrawal_title];
    
    UITextField *txt_Cash_withdrawal = [[UITextField alloc]initWithFrame:CGRectMake(view_content.width/2,  lab_line_two.bottom , view_content.width/2 - 15, 48)];
    txt_Cash_withdrawal.placeholder = @"请输入";
    txt_Cash_withdrawal.textColor = CCC_COLOR;
    txt_Cash_withdrawal.font = RFont(16);
    txt_Cash_withdrawal.textAlignment = NSTextAlignmentRight;
    [view_content addSubview:txt_Cash_withdrawal];
    
    UILabel *lab_line_three = [[UILabel alloc]initWithFrame:CGRectMake(0, lab_Cash_withdrawal_title.bottom + 17, view_content.width, 1)];
    lab_line_three.backgroundColor = UIColorHex(F0F0F0);
    [view_content addSubview:lab_line_three];
    
    //手续费
    UILabel *lab_Service_Charge_title = [[UILabel alloc]initWithFrame:CGRectMake(14, lab_line_three.bottom + 14, 100, 15)];
    lab_Service_Charge_title.text= @"手续费:";
    lab_Service_Charge_title.font = RFont(16);
    lab_Service_Charge_title.textColor = CCC_COLOR;
    [view_content addSubview:lab_Service_Charge_title];
    
    UILabel *lab_Service_Charge = [[UILabel alloc]initWithFrame:CGRectMake(view_content.width/2,  lab_line_three.bottom + 15, view_content.width/2 - 15, 15)];
    lab_Service_Charge.text= @"￥0.00";
    lab_Service_Charge.font = RFont(16);
    lab_Service_Charge.textColor = NINE_COLOR;
    lab_Service_Charge.textAlignment = NSTextAlignmentRight;
    [view_content addSubview:lab_Service_Charge];
    
    
    UIButton *btn_cashWithdrewal = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_cashWithdrewal.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_cashWithdrewal.frame = CGRectMake(55, view_content.bottom + 29, SCREEN_WIDTH - 55*2, 40);
    [btn_cashWithdrewal setTitle:@"提现" forState:UIControlStateNormal];
    [btn_cashWithdrewal setTitleColor:UIColorHex(ffffff) forState:UIControlStateNormal];
    btn_cashWithdrewal.titleLabel.font = RFont(18);
    btn_cashWithdrewal.layer.cornerRadius = 20.0;
    btn_cashWithdrewal.layer.masksToBounds = YES;
    [self addSubview:btn_cashWithdrewal];
}
@end
