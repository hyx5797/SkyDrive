//
//  OpeningResultTableVCell.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/4.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 开奖结果
NS_ASSUME_NONNULL_BEGIN
//号码
@interface OpeningResultTableVCell : UITableViewCell

@end

//大小、单双
@interface OpeningResultBigOrSingleTableVCell : UITableViewCell

@end

//总和
@interface OpeningResultAllSumTableVCell : UITableViewCell

@end
NS_ASSUME_NONNULL_END
