//
//  Popup_DetailsOfRechargeView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "Popup_DetailsOfRechargeView.h"
@interface Popup_DetailsOfRechargeView()

@property (nonatomic,strong)UILabel *lab_trade_Title; //主题
@property (nonatomic,strong)UILabel *lab_trade_num; //交易编号
@property (nonatomic,strong)UILabel *lab_trade_start_time; //发起时间
@property (nonatomic,strong)UILabel *lab_trade_type; //类型
@property (nonatomic,strong)UILabel *lab_trade_money; //交易金额
@property (nonatomic,strong)UILabel *lab_trade_status; //交易状态
@property (nonatomic,strong)UILabel *lab_trade_method; //交易方式
@property (nonatomic,strong)UILabel *lab_trade_card_num; //交易卡号
@property (nonatomic,strong)UILabel *lab_trade_card_owner; //持卡人
@property (nonatomic,strong)UILabel *lab_trade_depositor; //存款人
@property (nonatomic,strong)UILabel *lab_trade_deposits_time; //存款时间
@property (nonatomic,strong)YYLabel *lab_trade_audit_remarks; //审核备注

@end

@implementation Popup_DetailsOfRechargeView

+ (instancetype)sharedInstance{
    
    static Popup_DetailsOfRechargeView * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
    });
    return instance;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        //上半部分添加透明button
        
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        //半透明视图
        self.whiteView = [[UIView alloc] init];
        self.whiteView.backgroundColor = [UIColor whiteColor];
        self.whiteView.frame = CGRectMake(35,(SCREEN_HEIGHT - 377)/2, SCREEN_WIDTH - 70, 377);
        self.whiteView.layer.cornerRadius = 5.0;
        self.whiteView.layer.masksToBounds = YES;
        [self addSubview:self.whiteView];
        
        //顶部的蓝色视图
        UIView *view_top = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.whiteView.width, 39)];
        view_top.backgroundColor = STATUS_BAR_BGCOLOR;
        [self.whiteView addSubview:view_top];
        //关闭
        UIButton *btnClose = [UIButton buttonWithType: UIButtonTypeCustom];
        [btnClose setImage:[UIImage imageNamed:@"icon_trade_close"] forState:UIControlStateNormal];
        btnClose.frame = CGRectMake(self.whiteView.width - 39, 0, 39, 39);
        btnClose.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [btnClose addTarget:self action:@selector(btnCloseClick) forControlEvents:UIControlEventTouchUpInside];
        [view_top addSubview:btnClose];
        //标题
        self.lab_trade_Title = [[UILabel alloc]initWithFrame:CGRectMake(39, 0, self.whiteView.width - 78, 39)];
        self.lab_trade_Title.text = @"充值详情";
        self.lab_trade_Title.textAlignment = NSTextAlignmentCenter;
        self.lab_trade_Title.font = LFont(21);
        self.lab_trade_Title.textColor = [UIColor whiteColor];
        [view_top addSubview: self.lab_trade_Title];
        
        UILabel *lab_trade_num_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, view_top.bottom + 12,100, 14)];
        lab_trade_num_Title.text = @"交易编号:";
       lab_trade_num_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_num_Title.font = MFont(15);
        lab_trade_num_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_num_Title];
        
        self.lab_trade_num = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, view_top.bottom + 12, self.whiteView.width - 140, 14)];
        self.lab_trade_num.text = @"10190256320785234523";
        self.lab_trade_num.font = MFont(15);
        self.lab_trade_num.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_num];
        
        UILabel *lab_trade_start_time_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_num_Title.bottom + 10,100, 14)];
        lab_trade_start_time_Title.text = @"发起时间:";
        lab_trade_start_time_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_start_time_Title.font = MFont(15);
        lab_trade_start_time_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_start_time_Title];
        
        self.lab_trade_start_time = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_num_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_start_time.text = @"209-08-26 12:22:01";
        self.lab_trade_start_time.font = MFont(15);
        self.lab_trade_start_time.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_start_time];
        
        UILabel *lab_trade_type_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_start_time_Title.bottom + 10,100, 14)];
        lab_trade_type_Title.text = @"交易类型:";
        lab_trade_type_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_type_Title.font = MFont(15);
        lab_trade_type_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_type_Title];
        
        self.lab_trade_type = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_start_time_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_type.text = @"充值";
        self.lab_trade_type.font = MFont(15);
        self.lab_trade_type.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_type];
        
        UILabel *lab_trade_money_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_type_Title.bottom + 10,100, 14)];
        lab_trade_money_Title.text = @"交易金额:";
        lab_trade_money_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_money_Title.font = MFont(15);
        lab_trade_money_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_money_Title];
        
        self.lab_trade_money = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_type_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_money.text = @"10000";
        self.lab_trade_money.font = MFont(15);
        self.lab_trade_money.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_money];
        
        UILabel *lab_trade_status_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_money_Title.bottom + 10,100, 14)];
        lab_trade_status_Title.text = @"交易状态:";
        lab_trade_status_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_status_Title.font = MFont(15);
        lab_trade_status_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_status_Title];
        
        self.lab_trade_status = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_money_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_status.text = @"充值失败";
        self.lab_trade_status.font = MFont(15);
        self.lab_trade_status.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_status];
        
        UILabel *lab_trade_method_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_status_Title.bottom + 10,100, 14)];
        lab_trade_method_Title.text = @"交易方式:";
        lab_trade_method_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_method_Title.font = MFont(15);
        lab_trade_method_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_method_Title];
        
        self.lab_trade_method = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_status_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_method.text = @"银行卡转账";
        self.lab_trade_method.font = MFont(15);
        self.lab_trade_method.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_method];
        
        UILabel *lab_trade_card_num_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_method_Title.bottom + 10,100, 14)];
        lab_trade_card_num_Title.text = @"卡号:";
        lab_trade_card_num_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_card_num_Title.font = MFont(15);
        lab_trade_card_num_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_card_num_Title];
        
        self.lab_trade_card_num = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_method_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_card_num.text = @"6215896324563249";
        self.lab_trade_card_num.font = MFont(15);
        self.lab_trade_card_num.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_card_num];
        
        UILabel *lab_trade_card_owner_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_card_num_Title.bottom + 10,100, 14)];
        lab_trade_card_owner_Title.text = @"持卡人:";
        lab_trade_card_owner_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_card_owner_Title.font = MFont(15);
        lab_trade_card_owner_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_card_owner_Title];
        
        self.lab_trade_card_owner = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_card_num_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_card_owner.text = @"公司";
        self.lab_trade_card_owner.font = MFont(15);
        self.lab_trade_card_owner.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_card_owner];
        
        UILabel *lab_trade_depositor_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_card_owner_Title.bottom + 10,100, 14)];
        lab_trade_depositor_Title.text = @"存款人:";
        lab_trade_depositor_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_depositor_Title.font = MFont(15);
        lab_trade_depositor_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_depositor_Title];
        
        self.lab_trade_depositor = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_card_owner_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_depositor.text = @"我自己";
        self.lab_trade_depositor.font = MFont(15);
        self.lab_trade_depositor.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_depositor];
        
        UILabel *lab_trade_deposits_time_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_depositor_Title.bottom + 10,100, 14)];
        lab_trade_deposits_time_Title.text = @"存款时间:";
        lab_trade_deposits_time_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_deposits_time_Title.font = MFont(15);
        lab_trade_deposits_time_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_deposits_time_Title];
        
        self.lab_trade_deposits_time = [[UILabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_depositor_Title.bottom + 10, self.whiteView.width - 140, 14)];
        self.lab_trade_deposits_time.text = @"2019-08-26 12:22:24";
        self.lab_trade_deposits_time.font = MFont(15);
        self.lab_trade_deposits_time.textColor =NINE_COLOR;
        [self.whiteView addSubview: self.lab_trade_deposits_time];
        
        UILabel *lab_trade_audit_remarks_Title = [[UILabel alloc]initWithFrame:CGRectMake(20, lab_trade_deposits_time_Title.bottom + 10,100, 14)];
        lab_trade_audit_remarks_Title.text = @"审核备注:";
        lab_trade_audit_remarks_Title.textAlignment = NSTextAlignmentRight;
        lab_trade_audit_remarks_Title.font = MFont(15);
        lab_trade_audit_remarks_Title.textColor = UIColorHex(4C4C4C);
        [self.whiteView addSubview: lab_trade_audit_remarks_Title];
        
        self.lab_trade_audit_remarks = [[YYLabel alloc]initWithFrame:CGRectMake(lab_trade_num_Title.right, lab_trade_deposits_time_Title.bottom + 8, self.whiteView.width - 140, 44)];
        self.lab_trade_audit_remarks.text = @"订审核失败，具体原因请联系客服！";
        self.lab_trade_audit_remarks.font = MFont(15);
        self.lab_trade_audit_remarks.numberOfLines = 0;
        self.lab_trade_audit_remarks.textColor =NINE_COLOR;
        self.lab_trade_audit_remarks.textVerticalAlignment = 0;
        [self.whiteView addSubview: self.lab_trade_audit_remarks];
        
        //确定
        UIButton *btn_sure= [UIButton buttonWithType:UIButtonTypeCustom];
        btn_sure.backgroundColor = STATUS_BAR_BGCOLOR;
        [btn_sure setTitle:@"确定" forState:UIControlStateNormal];
        btn_sure.titleLabel.font = LFont(16);
        [btn_sure setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn_sure.frame = CGRectMake((self.whiteView.width - 139)/2, self.lab_trade_audit_remarks.bottom + 10, 139, 28);
//        btn_sure.tag = 15;
//        [btn_sure addTarget:self action:@selector(btnAllClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteView  addSubview:btn_sure];

    }
    return self;
}
- (void)showWithCurrentImgUrl:(NSString *)url addFileName:(NSString *) strTitle Complate:(void (^)(void))complate{
    if (self.superview) {
        return;
    }

    //
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.whiteView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.whiteView.xmg_y = (SCREEN_HEIGHT -  self.whiteView.xmg_height)/2;
        self.whiteView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:nil];
    
}
#pragma mark - 关闭
- (void)btnCloseClick{
    [self dismiss];
}

-(void)dismiss
{
    if (!self.superview) {
        return;
    }
    [UIView animateWithDuration:0.15 animations:^{
        
        self.whiteView.xmg_y = SCREEN_HEIGHT;
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
- (void)btnCancleClick{
    [self dismiss];
}

- (void)buttonWornClick:(UIButton *)button{
    [self dismiss];
}

@end
