//
//  RechargeView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//
//头部是160
#import "RechargeView.h"
@interface RechargeView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *data_array;
@property (nonatomic,strong)NSMutableArray *data_Detail_array;
@property (nonatomic,strong)NSMutableArray *data_img_array;
@end

@implementation RechargeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.data_array = [[NSMutableArray alloc]initWithObjects:@"银行卡转账",@"云闪付支付(0.2%优惠)",@"支付宝转账(0.5%优惠)",@"微信转账(0.5%优惠)",@"微信在线支付",@"支付宝在线支付", nil];
    self.data_Detail_array = [[NSMutableArray alloc]initWithObjects:@"线下付款，你的首选",@"银联扫码在线支付",@"支付宝转账支付",@"微信转账支付",@"微信在线支付",@"支付宝在线支付", nil];
    self.data_img_array =  [[NSMutableArray alloc]initWithObjects:@"icon_fund_bank_transfer",@"icon_fund_unionpay",@"icon_fund_alipay",@"icon_fund_wechat_transfer",@"icon_fund_wechat_online",@"icon_fund_alipay", nil];
    [self addSubview:self.tableView];
    return self;
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        CGFloat ht = 302 ;//SCREEN_HEIGHT - 180
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH-24, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 49.0;
        _tableView.layer.shadowColor = [UIColor colorWithRed:57/255.0 green:132/255.0 blue:232/255.0 alpha:0.1].CGColor;
        _tableView.layer.shadowOffset = CGSizeMake(0,2);
        _tableView.layer.shadowOpacity = 1;
        _tableView.layer.shadowRadius = 7;
        _tableView.layer.cornerRadius = 8;
        _tableView.showsVerticalScrollIndicator = false;
        _tableView.scrollEnabled = NO;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.data_array count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"RechargeTableVCell";
    
    //从tableView的一个队列里获取一个cell
    RechargeTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[RechargeTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.img_icon.image = [UIImage imageNamed:self.data_img_array[indexPath.row]];
    cell.lab_title.text = self.data_array[indexPath.row];
    cell.lab_detail.text = self.data_Detail_array[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
@end




#pragma 充值的cell
@interface RechargeTableVCell()


@end

@implementation RechargeTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.img_icon = [[UIImageView alloc]initWithFrame:CGRectMake(5, 11, 30, 30)];
    self.img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.img_icon];
    
    self.lab_title = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right + 15, 9, self.contentView.bounds.size.width - self.img_icon.right - 20, 18)];
    self.lab_title.textColor = UIColorHex(333333);
    self.lab_title.font = LFont(14);
    [self.contentView addSubview:self.lab_title];
    
    self.lab_detail = [[UILabel alloc]initWithFrame:CGRectMake(self.img_icon.right + 14, self.lab_title.bottom + 3, self.contentView.bounds.size.width - self.img_icon.right - 20, 13)];
    self.lab_detail.textColor = NINE_COLOR;
    self.lab_detail.font = LFont(11);
    [self.contentView addSubview:self.lab_detail];
    
    //箭头
       UIImageView *img_more = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 36, 15, 9, 16)];
       img_more.image = [UIImage imageNamed:@"icon_mine_more"];
         img_more.centerY = self.img_icon.centerY;
       [self.contentView addSubview:img_more];
    return self;
}
- (void)setImg_icon:(UIImageView *)img_icon{
    _img_icon = img_icon;
}
-(void)setLab_title:(UILabel *)lab_title{
    _lab_title = lab_title;
}
- (void)setLab_detail:(UILabel *)lab_detail{
    _lab_detail = lab_detail;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
