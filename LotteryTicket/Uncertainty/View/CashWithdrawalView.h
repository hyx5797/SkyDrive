//
//  CashWithdrawalView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 提现视图
NS_ASSUME_NONNULL_BEGIN

@interface CashWithdrawalView : UIView

@property (nonatomic, copy) void (^btnBandingBankBlock)(void);//绑定银行卡

@end

NS_ASSUME_NONNULL_END
