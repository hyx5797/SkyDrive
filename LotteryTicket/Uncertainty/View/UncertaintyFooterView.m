//
//  UncertaintyFooterView.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UncertaintyFooterView.h"
@interface UncertaintyFooterView()

@property (nonatomic,strong)YYLabel *lab_Number_of_Notes;//注数
@property (nonatomic,strong)YYLabel *lab_Amount_bet_Total;//下注金额(计)
@property (nonatomic,strong)YYLabel *lab_Result_Total;//结果(总计)

@end
@implementation UncertaintyFooterView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //注数、下注金额(计)、结果(总计)
    CGFloat wD = SCREEN_WIDTH;
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, wD, 1)];
    labLine.backgroundColor = NINE_COLOR;
    [self addSubview:labLine];
 
//    UILabel *lab_Notes_title = [[UILabel alloc]initWithFrame:CGRectMake(15, labLine.bottom+10, 110, 20)];
//    lab_Notes_title.text = @"注数";
//    lab_Notes_title.textColor = [UIColor grayColor];
//    lab_Notes_title.font = RFont(16);
//    [self addSubview:lab_Notes_title];
    
    self.lab_Number_of_Notes = [[UILabel alloc]initWithFrame:CGRectMake(15, labLine.bottom+4, wD - 30, 13)];
    _lab_Number_of_Notes.text = @"注数：4";
    _lab_Number_of_Notes.textColor = SIX_COLOR;
    _lab_Number_of_Notes.font = MFont(13);
//    _lab_Number_of_Notes.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lab_Number_of_Notes];

//    UILabel *lab_bet_title = [[UILabel alloc]initWithFrame:CGRectMake(15, lab_Notes_title.bottom + 10, 110, 20)];
//    lab_bet_title.text = @"下注金额(计)";
//    lab_bet_title.textColor = [UIColor grayColor];
//    lab_bet_title.font = RFont(16);
//    [self addSubview:lab_bet_title];
    
    self.lab_Amount_bet_Total = [[UILabel alloc]initWithFrame:CGRectMake(15, _lab_Number_of_Notes.bottom + 5, wD - 30, 13)];
    self.lab_Amount_bet_Total.text = @"总下注金额：500";
    _lab_Amount_bet_Total.textColor = SIX_COLOR;
    _lab_Amount_bet_Total.font = MFont(13);
//    _lab_Amount_bet_Total.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.lab_Amount_bet_Total];
    
//    UILabel *lab_result_title = [[UILabel alloc]initWithFrame:CGRectMake(15, lab_bet_title.bottom + 10, 110, 20)];
//    lab_result_title.text = @"结果(总计)";
//    lab_result_title.textColor = [UIColor grayColor];
//    lab_result_title.font = RFont(16);
//    [self addSubview:lab_result_title];
    
    self.lab_Result_Total = [[UILabel alloc]initWithFrame:CGRectMake(15, _lab_Amount_bet_Total.bottom + 5, wD - 30, 13)];
    self.lab_Result_Total.text = @"可赢金额:884.5";
    _lab_Result_Total.textColor = SIX_COLOR;
    _lab_Result_Total.font = MFont(13);
//    _lab_Result_Total.textAlignment = NSTextAlignmentRight;
    [self addSubview: self.lab_Result_Total];
}
@end


#pragma 今日已结
@interface OverTodayFooterView()

@property (nonatomic,strong)YYLabel *lab_Amount_bet_Total;//下注金额
@property (nonatomic,strong)YYLabel *lab_win_loss_Total;//输赢金额

@end
@implementation OverTodayFooterView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self initWithHeaderViewFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithHeaderViewFrame{
    //下注金额：输赢金额
    CGFloat wD = SCREEN_WIDTH;
    UILabel *labLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, wD, 1)];
    labLine.backgroundColor = NINE_COLOR;
    [self addSubview:labLine];
    
    self.lab_Amount_bet_Total = [[UILabel alloc]initWithFrame:CGRectMake(15,4, wD -30, 13)];
    self.lab_Amount_bet_Total.text = @"下注金额 :400";
    self.lab_Amount_bet_Total.textColor = SIX_COLOR;
    self.lab_Amount_bet_Total.font = MFont(13);
//    self.lab_Amount_bet_Total.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.lab_Amount_bet_Total];
    
    self.lab_win_loss_Total = [[UILabel alloc]initWithFrame:CGRectMake(15, self.lab_Amount_bet_Total.bottom + 5, wD -30, 14)];
    self.lab_win_loss_Total.text = @"输赢金额 :884.5";
    self.lab_win_loss_Total.textColor =SIX_COLOR;
    self.lab_win_loss_Total.font = MFont(14);
//    self.lab_win_loss_Total.textAlignment = NSTextAlignmentRight;
    [self addSubview: self.lab_win_loss_Total];
    
}
@end
