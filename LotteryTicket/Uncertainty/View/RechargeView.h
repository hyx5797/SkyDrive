//
//  RechargeView.h
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/6.
//  Copyright © 2019 hyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma 充值视图
NS_ASSUME_NONNULL_BEGIN

@interface RechargeView : UIView

@end



//cell
@interface RechargeTableVCell : UITableViewCell


@property (nonatomic,strong)UIImageView *img_icon;
@property (nonatomic,strong)UILabel *lab_title;
@property (nonatomic,strong)UILabel *lab_detail;


@end
NS_ASSUME_NONNULL_END
