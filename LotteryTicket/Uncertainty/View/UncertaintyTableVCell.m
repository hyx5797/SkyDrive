//
//  UncertaintyTableVCell.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UncertaintyTableVCell.h"
@interface UncertaintyTableVCell()

@property (nonatomic,strong)YYLabel *lab_Note_number;//注单号内容
@property (nonatomic,strong)YYLabel *lab_Type;//类型
@property (nonatomic,strong)YYLabel *lab_play_method;//玩法
@property (nonatomic,strong)YYLabel *lab_bet;//下注
@property (nonatomic,strong)YYLabel *lab_win;//可赢

@end
@implementation UncertaintyTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self  initWithCellFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithCellFrame{
    CGFloat wD = SCREEN_WIDTH/5.0;
    CGFloat hT =  46.0;
    
    self.lab_Note_number = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, wD, hT)];
    self.lab_Note_number.numberOfLines = 0;
    self.lab_Note_number.text = @"2019-08-09\n13:40:57";
    self.lab_Note_number.font = MFont(12);
    self.lab_Note_number.textColor = SIX_COLOR;
    self.lab_Note_number.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_Note_number];
    
    self.lab_Type = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_Note_number.right, 0, wD, hT )];
    self.lab_Type.numberOfLines = 0;
    self.lab_Type.text = @"重庆时时彩\n20190204025";
    self.lab_Type.font = MFont(12);
    self.lab_Type.textColor = SIX_COLOR;
    self.lab_Type.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_Type];
    
    self.lab_play_method = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_Type.right, 0, wD, hT )];
    self.lab_play_method.numberOfLines = 0;
    [self.contentView addSubview:self.lab_play_method];
    self.lab_play_method.attributedText = [XYTTools getDiffientLabel:@"第九名 单" addOneColor:SIX_COLOR addOneFont:MFont(13) addTwoStr:@"@1.995" addTwoColor:UIColorHex(FF9600) addTwoFont:MFont(12)];
    self.lab_play_method.textAlignment = NSTextAlignmentCenter;
    
    
    self.lab_bet = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_play_method.right, 0, wD, hT)];
    self.lab_bet.numberOfLines = 0;
    self.lab_bet.text = @"150";
    self.lab_bet.font = MFont(12);
    self.lab_bet.textColor = SIX_COLOR;
     self.lab_bet.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_bet];
    
    self.lab_win = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_bet.right, 0, wD, hT)];
    self.lab_win.numberOfLines = 0;
    self.lab_win.text = @"149.3";
    self.lab_win.font = MFont(12);
    self.lab_win.textColor = SIX_COLOR;
     self.lab_win.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_win];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

#pragma 今日已结
@interface OverTodayTableVCell()

@property (nonatomic,strong)YYLabel *lab_game_name;//游戏名称
@property (nonatomic,strong)YYLabel *lab_number_pens;//笔数
@property (nonatomic,strong)YYLabel *lab_amount_bets;//下注金额
@property (nonatomic,strong)YYLabel *lab_wins_losses;//输赢金额
@property (nonatomic,strong)UIButton *btn_details;//详情

@end
@implementation OverTodayTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self  initWithCellFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithCellFrame{
    CGFloat wD = SCREEN_WIDTH/8.0;
    CGFloat hT =  51.0;
    
    self.lab_game_name = [[YYLabel alloc]initWithFrame:CGRectMake(0, 11, wD*2, hT - 22)];
    self.lab_game_name.text = @"快乐时时彩";
    self.lab_game_name.font = MFont(14);
    self.lab_game_name.textColor = SIX_COLOR;
    self.lab_game_name.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_game_name];
    
    self.lab_number_pens = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_game_name.right, 11, wD, hT - 22)];
    self.lab_number_pens.text = @"5";
    self.lab_number_pens.font = MFont(14);
    self.lab_number_pens.textColor = SIX_COLOR;
    self.lab_number_pens.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_number_pens];
    
    self.lab_amount_bets = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_number_pens.right, 11, wD*2, hT - 22)];
    self.lab_amount_bets.text = @"100";
    self.lab_amount_bets.font = MFont(14);
    self.lab_amount_bets.textColor = SIX_COLOR;
    self.lab_amount_bets.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_amount_bets];
    
    self.lab_wins_losses = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_amount_bets.right, 11, wD*2, hT - 22)];
    self.lab_wins_losses.text = @"99";
    self.lab_wins_losses.font = MFont(14);
    self.lab_wins_losses.textColor = SIX_COLOR;
    self.lab_wins_losses.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_wins_losses];
    
    self.btn_details = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn_details.frame = CGRectMake(self.lab_wins_losses.right, 0, wD, hT);
    [self.btn_details setImage:[UIImage imageNamed:@"icon_overtoday_details"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.btn_details];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


#pragma 今日已结详情
@interface OverTodayDetailTableVCell()
//彩种期号、下注明细、下注金额、输赢金额

@property (nonatomic,strong)YYLabel *lab_colour_period;
@property (nonatomic,strong)YYLabel *lab_bets_details;
@property (nonatomic,strong)YYLabel *lab_amount_bets;
@property (nonatomic,strong)YYLabel *lab_wins_losses;

@end
@implementation OverTodayDetailTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self  initWithCellFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithCellFrame{
    CGFloat wD = SCREEN_WIDTH/4.0;
    CGFloat hT = 60;
    
    self.lab_colour_period = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, wD, hT)];
    self.lab_colour_period.text = @"快乐时时彩\n20190204025";
    self.lab_colour_period.numberOfLines = 0;
    self.lab_colour_period.font = MFont(12);
    self.lab_colour_period.textColor = SIX_COLOR;
    self.lab_colour_period.textAlignment = NSTextAlignmentCenter;
    self.lab_colour_period.textVerticalAlignment = 1;
    [self.contentView addSubview:self.lab_colour_period];
    
    self.lab_bets_details = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_colour_period.right, 0, wD, hT)];
    self.lab_bets_details.text = @"第五球 -4\n@9.95\n#0";
    self.lab_bets_details.numberOfLines = 0;
    self.lab_bets_details.font = MFont(12);
    self.lab_bets_details.textColor = SIX_COLOR;
    self.lab_bets_details.textAlignment = NSTextAlignmentCenter;
     self.lab_bets_details.textVerticalAlignment = 1;
    [self.contentView addSubview:self.lab_bets_details];
    
    self.lab_amount_bets = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_bets_details.right, 0, wD, hT )];
    self.lab_amount_bets.text = @"20";
    self.lab_amount_bets.font = MFont(12);
    self.lab_amount_bets.textColor = SIX_COLOR;
    self.lab_amount_bets.textAlignment = NSTextAlignmentCenter;
    self.lab_amount_bets.textVerticalAlignment = 1;
    [self.contentView addSubview:self.lab_amount_bets];
    
    self.lab_wins_losses = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_amount_bets.right, 0, wD, hT )];
    self.lab_wins_losses.text = @"179";
    self.lab_wins_losses.font = MFont(12);
    self.lab_wins_losses.textColor = SIX_COLOR;
    self.lab_wins_losses.textAlignment = NSTextAlignmentCenter;
    self.lab_wins_losses.textVerticalAlignment = 1;
    [self.contentView addSubview:self.lab_wins_losses];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

#pragma 下注记录
@interface NoteRecordTableVCell()
///时间、、笔数、、输赢

@property (nonatomic,strong)YYLabel *lab_date_time;
@property (nonatomic,strong)YYLabel *lab_bets_count;
@property (nonatomic,strong)YYLabel *lab_wins_losses;

@end
@implementation NoteRecordTableVCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self  initWithCellFrame];
    return self;
}
#pragma 初始化界面UI
- (void)initWithCellFrame{
    CGFloat wD = SCREEN_WIDTH/3.0;
    CGFloat hT = 46;
    
    self.lab_date_time = [[YYLabel alloc]initWithFrame:CGRectMake(0, 0, wD, hT)];
    self.lab_date_time.text = @"2019-08-09\n星期一";
    self.lab_date_time.numberOfLines = 0;
    self.lab_date_time.font = MFont(12);
    self.lab_date_time.textColor = SIX_COLOR;
    self.lab_date_time.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_date_time];

    self.lab_bets_count = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_date_time.right, 0, wD, hT )];
    self.lab_bets_count.text = @"20";
    self.lab_bets_count.font = MFont(12);
    self.lab_bets_count.textColor = SIX_COLOR;
    self.lab_bets_count.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_bets_count];
    
    self.lab_wins_losses = [[YYLabel alloc]initWithFrame:CGRectMake(self.lab_bets_count.right, 0, wD, hT)];
    self.lab_wins_losses.text = @"179";
    self.lab_wins_losses.font = MFont(12);
    self.lab_wins_losses.textColor = SIX_COLOR;
    self.lab_wins_losses.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.lab_wins_losses];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
