//
//  OpeningResultVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/4.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "OpeningResultVC.h"
#import "OpeningResultTableVCell.h"
#import "WSDatePickerView.h"

@interface OpeningResultVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UIButton *selectedBtn;//选中

@end

@implementation OpeningResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"开奖结果";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];

    
    //导航栏右边的按钮
    UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(navbarRightItemClick)];
    self.navigationItem.rightBarButtonItem = right_Item;
}
#pragma 导航栏事件
- (void)navbarRightItemClick{
    [self tabHiddenOrShow];
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        CGFloat tabH = TabBarHeight;
        CGFloat ht = SCREEN_HEIGHT - tabH  ;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 62.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        [_tableView setTableHeaderView:[self createHeadeView]];
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedBtn.tag == 3) {
        //总和
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"OpeningResultAllSumTableVCell";
        //从tableView的一个队列里获取一个cell
        OpeningResultAllSumTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[OpeningResultAllSumTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row%2==0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else{
            cell.contentView.backgroundColor = UIColorHex(f5f9fe);
        }
        return cell;
    }else  if (self.selectedBtn.tag == 1 || self.selectedBtn.tag == 2) {
        //大小、单双
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"OpeningResultBigOrSingleTableVCell";
        //从tableView的一个队列里获取一个cell
        OpeningResultBigOrSingleTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[OpeningResultBigOrSingleTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row%2==0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else{
            cell.contentView.backgroundColor = UIColorHex(f5f9fe);
        }
        return cell;
    }else{
        //给每个cell设置ID号（重复利用时使用）
        static NSString *cellID = @"OpeningResultTableVCell";
        //从tableView的一个队列里获取一个cell
        OpeningResultTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        //判断队列里面是否有这个cell 没有自己创建，有直接使用
        if (cell == nil) {
            //没有,创建一个
            cell = [[OpeningResultTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row%2==0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else{
            cell.contentView.backgroundColor = UIColorHex(f5f9fe);
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
#pragma 创建顶部的按钮视图
- (UIView *)createHeadeView{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    headerView.backgroundColor = UIColorHex(F2F2F2);
    
    UIView *view_top = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 37)];
    view_top.backgroundColor = UIColorHex(1464CF);
    [headerView addSubview:view_top];
    // 创建标题按钮
    UIButton * button_left = [UIButton buttonWithType:UIButtonTypeCustom];
    button_left.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button_left setImage:[UIImage imageNamed:@"icon_title_drop_down"] forState:UIControlStateNormal];
    [button_left setImage:[UIImage imageNamed:@"icon_title_drop_up"] forState:UIControlStateSelected];
    [button_left setTitle:@"加拿大时时彩" forState:UIControlStateNormal];
    [button_left setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    button_left.titleLabel.font = MFont(16);
    button_left.frame = CGRectMake(25, 0, (SCREEN_WIDTH-1)/2 - 30, 37);
    button_left.titleEdgeInsets = UIEdgeInsetsMake(0, -button_left.imageView.frame.size.width - button_left.frame.size.width + button_left.titleLabel.frame.size.width+15, 0, 0);
    button_left.imageEdgeInsets = UIEdgeInsetsMake(0, button_left.titleLabel.frame.size.width +18, 0, 0);
//    -button_left.titleLabel.frame.size.width - button_left.frame.size.width + button_left.imageView.frame.size.width
    [button_left addTarget:self action:@selector(buttonLeftChooseGame:) forControlEvents:UIControlEventTouchUpInside];
    [view_top addSubview:button_left];
    
    UILabel *lab_center = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-1)/2, 0, 1, 37)];
    lab_center.backgroundColor = UIColorHex(004EB7);
    [view_top addSubview:lab_center];
    
    UIImageView *img_right_left = [[UIImageView alloc]initWithFrame:CGRectMake(lab_center.right + 20, 11, 15, 15)];
    img_right_left.image = [UIImage imageNamed:@"icon_fund_result_date"];
    img_right_left.contentMode = UIViewContentModeScaleAspectFit;
    [view_top addSubview:img_right_left];
    
    UIButton * button_right = [UIButton buttonWithType:UIButtonTypeCustom];
    button_right.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button_right setImage:[UIImage imageNamed:@"icon_fund_result_refresh"] forState:UIControlStateNormal];
    [button_right setImage:[UIImage imageNamed:@"icon_fund_result_refresh"] forState:UIControlStateSelected];
    [button_right setTitle:[self getCurrentTimes] forState:UIControlStateNormal];
    [button_right setTitleColor:UIColorHex(F8FCFF) forState:UIControlStateNormal];
    button_right.titleLabel.font = MFont(16);
    button_right.frame = CGRectMake(img_right_left.right +4, 0, SCREEN_WIDTH-img_right_left.right - 10, 37);
    button_right.titleEdgeInsets = UIEdgeInsetsMake(0, -button_right.imageView.frame.size.width - button_right.frame.size.width + button_right.titleLabel.frame.size.width, 0, 0);
    button_right.imageEdgeInsets = UIEdgeInsetsMake(0,  button_right.titleLabel.frame.size.width +3, 0, 0);
    [button_right addTarget:self action:@selector(buttonToDatePickerClick:) forControlEvents:UIControlEventTouchUpInside];
    [view_top addSubview:button_right];
    
    YYLabel *lab_Issue_number_title =  [[YYLabel alloc]initWithFrame:CGRectMake(0, view_top.bottom, 134, 32)];
    lab_Issue_number_title.text = @"期号";
    lab_Issue_number_title.textAlignment = NSTextAlignmentCenter;
    lab_Issue_number_title.font = MFont(13);
    lab_Issue_number_title.textColor = STATUS_BAR_BGCOLOR;
    [headerView addSubview:lab_Issue_number_title];
    //号码、大小、单双、总和
    NSArray *title_Array = @[@"号码",@"大小",@"单双",@"总和"];
    for (int i=0; i<title_Array.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(lab_Issue_number_title.right +( i*60), view_top.bottom, 60, 32);
        [btn setTitle:title_Array[i] forState:UIControlStateNormal];
        [btn setTitleColor:SIX_COLOR forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(FF9600) forState:UIControlStateSelected];
        btn.contentHorizontalAlignment = 1;
        btn.titleLabel.font = MFont(13);
        [headerView addSubview:btn];
        btn.tag = i;
        [btn addTarget:self action:@selector(btnMoreChooseMenuClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i==0) {
            btn.selected = YES;
            self.selectedBtn = btn;
        }
    }
    
    return headerView;
}
#pragma btnMoreChooseMenuClick
- (void)btnMoreChooseMenuClick:(UIButton *)btn{
    if (btn.tag == self.selectedBtn.tag) {
        return;
    }
    self.selectedBtn.selected = NO;
    btn.selected = YES;
    self.selectedBtn = btn;
    [self.tableView reloadData];
    [UIView animateWithDuration:0.25 animations:^{
//        self.img_choose_Menu.centerX = btn.centerX - 12;
        
    }];
}
#pragma 日期控件的选择
- (void)buttonToDatePickerClick:(UIButton *)btn{
    //年-月-日
    WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDay CompleteBlock:^(NSDate *selectDate) {
        
        NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd"];
        [btn setTitle:dateString forState:UIControlStateNormal];
    }];
    datepicker.dateLabelColor = STATUS_BAR_BGCOLOR;//年-月-日-时-分 颜色
    datepicker.datePickerColor = STATUS_BAR_BGCOLOR;//滚轮日期颜色
    datepicker.doneButtonColor = STATUS_BAR_BGCOLOR;//确定按钮的颜色
    [datepicker show];
}
//获取当前日期
-(NSString*)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd"];
    //现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];
    //----------将nsdate按formatter格式转成nsstring
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}
#pragma 游戏的分类选择
- (void)buttonLeftChooseGame:(UIButton *)btn{
    [self gameTypeShowOrHidden];
}
@end

