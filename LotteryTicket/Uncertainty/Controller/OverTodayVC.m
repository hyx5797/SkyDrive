//
//  OverTodayVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/3.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "OverTodayVC.h"
#import "UncertaintyTableVCell.h"
#import "UncertaintyHeaderView.h"
#import "UncertaintyFooterView.h"
#import "OverTodayDetailVC.h"
@interface OverTodayVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;

@end

@implementation OverTodayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"今日已结";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    CGFloat vw_ht = 0;
    if (isIphoneX) {
        vw_ht = 90;
    }else{
        vw_ht = 56;
    }
        CGFloat navH = NaviBarHeight;
     _tableView.height = SCREEN_HEIGHT - vw_ht - navH;
    OverTodayFooterView *footerView = [[OverTodayFooterView alloc]initWithFrame:CGRectMake(0, _tableView.bottom, SCREEN_WIDTH, vw_ht)];
    footerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:footerView];
    
    //导航栏右边的按钮
    UIImage *rightImage = [[UIImage imageNamed:@"icon_game_nav_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *right_Item = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(navbarRightItemClick)];
    self.navigationItem.rightBarButtonItem = right_Item;
}
#pragma 导航栏事件
- (void)navbarRightItemClick{
    [self tabHiddenOrShow];
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
//        CGFloat tabH = TabBarHeight;
        CGFloat ht = SCREEN_HEIGHT  ;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 40.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        OverTodayHeaderView *headerView = [[OverTodayHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 35)];
        [_tableView setTableHeaderView:headerView];
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"OverTodayTableVCell";
    
    //从tableView的一个队列里获取一个cell
    OverTodayTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[OverTodayTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row%2==0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
    }else{
            cell.contentView.backgroundColor = UIColorHex(f5f9fe);
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OverTodayDetailVC *vc = [OverTodayDetailVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
