//
//  AddBankVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/9/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "AddBankVC.h"

@interface AddBankVC ()

@end

@implementation AddBankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"添加银行卡";
    [self createViewFrame];
}
#pragma 初始化界面UI
- (void)createViewFrame{
    UIImageView *img_icon = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 184)/2, 35, 184, 112)];
    img_icon.image = [UIImage imageNamed:@"img_addbank"];
    img_icon.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:img_icon];
    
    //持卡人
    UILabel *lab_Cardholder = [[UILabel alloc]initWithFrame:CGRectMake(25, img_icon.bottom + 42, 100, 17)];
    lab_Cardholder.text= @"持卡人";
    lab_Cardholder.font = RFont(18);
    lab_Cardholder.textColor = UIColorHex(333333);
    [self.view addSubview:lab_Cardholder];
    
    UITextField *txt_Cardholder = [[UITextField alloc]initWithFrame:CGRectMake(lab_Cardholder.right + 15, img_icon.bottom + 26 , SCREEN_WIDTH - lab_Cardholder.right - 100, 49)];
    txt_Cardholder.placeholder = @"请输入持卡人姓名";
    txt_Cardholder.textColor = CCC_COLOR;
    txt_Cardholder.font = RFont(18);
    [self.view  addSubview:txt_Cardholder];
    
    UILabel *lab_line_one = [[UILabel alloc]initWithFrame:CGRectMake(0, txt_Cardholder.bottom, SCREEN_WIDTH, 1)];
    lab_line_one.backgroundColor = UIColorHex(F0F0F0);
    [self.view  addSubview:lab_line_one];
    
    //卡号
    UILabel *lab_Bank_Card_Number = [[UILabel alloc]initWithFrame:CGRectMake(25, lab_line_one.bottom + 17, 100, 17)];
    lab_Bank_Card_Number.text= @"卡号";
    lab_Bank_Card_Number.font = RFont(18);
    lab_Bank_Card_Number.textColor = UIColorHex(333333);
    [self.view addSubview:lab_Bank_Card_Number];
    
    UITextField *txt_Bank_Card_Number = [[UITextField alloc]initWithFrame:CGRectMake(lab_Cardholder.right + 15, lab_line_one.bottom , SCREEN_WIDTH - lab_Cardholder.right - 100, 49)];
    txt_Bank_Card_Number.placeholder = @"请输入银行卡号";
    txt_Bank_Card_Number.textColor = CCC_COLOR;
    txt_Bank_Card_Number.font = RFont(18);
    [self.view  addSubview:txt_Bank_Card_Number];
    
    UILabel *lab_line_two = [[UILabel alloc]initWithFrame:CGRectMake(0, txt_Bank_Card_Number.bottom, SCREEN_WIDTH, 1)];
    lab_line_two.backgroundColor = UIColorHex(F0F0F0);
    [self.view  addSubview:lab_line_two];
    
    //开户行
    UILabel *lab_Opening_Bank = [[UILabel alloc]initWithFrame:CGRectMake(25, lab_line_two.bottom + 17, 100, 17)];
    lab_Opening_Bank.text= @"开户行";
    lab_Opening_Bank.font = RFont(18);
    lab_Opening_Bank.textColor = UIColorHex(333333);
    [self.view addSubview:lab_Opening_Bank];
    
    UITextField *txt_Opening_Bank  = [[UITextField alloc]initWithFrame:CGRectMake(lab_Opening_Bank.right + 15, lab_line_two.bottom , SCREEN_WIDTH - lab_Opening_Bank.right - 100, 49)];
    txt_Opening_Bank.placeholder = @"请输入开户行";
    txt_Opening_Bank.textColor = CCC_COLOR;
    txt_Opening_Bank.font = RFont(18);
    [self.view  addSubview:txt_Opening_Bank];
    
    UILabel *lab_line_three = [[UILabel alloc]initWithFrame:CGRectMake(0, txt_Opening_Bank.bottom, SCREEN_WIDTH, 1)];
    lab_line_three.backgroundColor = UIColorHex(F0F0F0);
    [self.view  addSubview:lab_line_three];
    
    //开户支行
    UILabel *lab_opening_Branch = [[UILabel alloc]initWithFrame:CGRectMake(25, lab_line_three.bottom + 17, 100, 17)];
    lab_opening_Branch.text= @"开户支行";
    lab_opening_Branch.font = RFont(18);
    lab_opening_Branch.textColor = UIColorHex(333333);
    [self.view addSubview:lab_opening_Branch];
    
    UITextField *txt_opening_Branch = [[UITextField alloc]initWithFrame:CGRectMake(lab_opening_Branch.right + 15, lab_line_three.bottom , SCREEN_WIDTH - lab_opening_Branch.right - 100, 49)];
    txt_opening_Branch.placeholder = @"请输入开户支行";
    txt_opening_Branch.textColor = CCC_COLOR;
    txt_opening_Branch.font = RFont(18);
    [self.view  addSubview:txt_opening_Branch];
    
    UILabel *lab_line_four = [[UILabel alloc]initWithFrame:CGRectMake(0, txt_opening_Branch.bottom, SCREEN_WIDTH, 1)];
    lab_line_four.backgroundColor = UIColorHex(F0F0F0);
    [self.view  addSubview:lab_line_four];
    
    
    UIButton *btn_add_bank = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_add_bank.backgroundColor = STATUS_BAR_BGCOLOR;
    btn_add_bank.frame = CGRectMake(40, lab_line_four.bottom + 55, SCREEN_WIDTH -40*2, 40);
    [btn_add_bank setTitle:@"下一步" forState:UIControlStateNormal];
    [btn_add_bank setTitleColor:UIColorHex(ffffff) forState:UIControlStateNormal];
    btn_add_bank.titleLabel.font = RFont(18);
    btn_add_bank.layer.cornerRadius = 20.0;
    btn_add_bank.layer.masksToBounds = YES;
    [self.view addSubview:btn_add_bank];
}

@end
