//
//  FundManagementVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/26.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "FundManagementVC.h"
#import "FundManagementCell.h"
#import "Popup_DetailsOfRechargeView.h"
#import "LTButton.h"
#import "RechargeView.h"
#import "DetailsOfFundsView.h"
#import "CashWithdrawalView.h"
#import "AddBankVC.h"
@interface FundManagementVC ()

//@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UIImageView *img_photo;//用户头像
@property (nonatomic,strong)UILabel *lab_username;//用户昵称
@property (nonatomic,strong)UILabel *lab_email;//用户邮箱
@property (nonatomic,strong)UILabel *lab_account_yue;//账户余额
@property (nonatomic,strong)UIButton *selectedBtn;//选中
@property (nonatomic,strong)UIView *view_BtnBlueLine;//按钮选择下面的那条线
@property (nonatomic,strong)RechargeView *view_recharge;//充值视图
@property (nonatomic,strong)CashWithdrawalView *view_cashWithdrawal;//提现视图
@property (nonatomic,strong)DetailsOfFundsView *view_detailFund;//资金明细视图
@end

@implementation FundManagementVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;//黑色
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"资金管理";
    self.view.backgroundColor = [UIColor whiteColor];
//    COLOR(237, 236, 237);
    
    [self create_new_headerView];
    
    CGFloat tabHt = TabBarHeight;
    CGFloat navHt = NaviBarHeight;
    self.view_recharge = [[RechargeView alloc]initWithFrame:CGRectMake(0, navHt + 175, SCREEN_WIDTH, SCREEN_HEIGHT - tabHt - 160- navHt)];
    [self.view addSubview:self.view_recharge];
    
    self.view_cashWithdrawal= [[CashWithdrawalView alloc]initWithFrame:CGRectMake(0, navHt + 175, SCREEN_WIDTH, SCREEN_HEIGHT - tabHt - 160 -navHt)];
    self.view_cashWithdrawal.hidden = YES;
    self.view_cashWithdrawal.btnBandingBankBlock = ^{
        AddBankVC *vc = [AddBankVC new];
        [self.navigationController pushViewController:vc animated:YES];
    };
    [self.view addSubview:self.view_cashWithdrawal];
    
    self.view_detailFund  = [[DetailsOfFundsView alloc]initWithFrame:CGRectMake(0, navHt + 175, SCREEN_WIDTH, SCREEN_HEIGHT - tabHt - 160 -navHt)];
    self.view_detailFund.hidden = YES;
    [self.view addSubview:self.view_detailFund];
}
#pragma 创建新的资金管理的头部
- (void)create_new_headerView{
    CGFloat navHt = NaviBarHeight;
    CGFloat stauHt = StatusBarHeight;
    
    UIImageView *headerView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 162+navHt)];
    headerView.backgroundColor =  [UIColor whiteColor];
    headerView.userInteractionEnabled =YES;
    headerView.contentMode = UIViewContentModeScaleAspectFill;
//
    //头部的蓝色条
    UIImageView *img_top_bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, -5, SCREEN_WIDTH, 133+navHt)];
    img_top_bg.image = [UIImage imageNamed:@"img_fund_headerbg"];
      img_top_bg.userInteractionEnabled =YES;
    [headerView addSubview:img_top_bg];
    
    //标题
    UILabel *lab_nav_title = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100)/2, stauHt,100 , 44)];
    lab_nav_title.text = @"资金明细";
    lab_nav_title.textColor = [UIColor whiteColor];
    lab_nav_title.font = MFont(19);
    //    RFont(18);
    lab_nav_title.textAlignment = NSTextAlignmentCenter;
    [img_top_bg addSubview:lab_nav_title];
    //有侧边栏
    UIButton * btn_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_right.frame = CGRectMake(headerView.width - 44-15, stauHt, 44, 44);
    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateNormal];
    [btn_right setImage:[UIImage imageNamed:@"icon_game_nav_right"] forState:UIControlStateSelected];
    btn_right.contentHorizontalAlignment =UIControlContentHorizontalAlignmentRight;
    btn_right.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [btn_right addTarget:self action:@selector(btn_right_mine_click) forControlEvents:UIControlEventTouchUpInside];
    [img_top_bg addSubview:btn_right];
    
    UIImageView *img_photo = [[UIImageView alloc]initWithFrame:CGRectMake(11, navHt+10, 48, 48)];
    img_photo.backgroundColor = [UIColor whiteColor];
    img_photo.layer.cornerRadius = 24.0;
    img_photo.layer.masksToBounds = YES;
    [img_top_bg addSubview:img_photo];
    
    UILabel *lab_username = [[UILabel alloc]initWithFrame:CGRectMake(img_photo.right + 10, navHt+20, img_top_bg.width - img_photo.right -30, 16)];
    lab_username.text = @"上午好，10254655khg";
    lab_username.font = RFont(14);
    lab_username.textColor = [UIColor whiteColor];
    [img_top_bg addSubview:lab_username];
    
    
    UILabel *lab_email = [[UILabel alloc]initWithFrame:CGRectMake(img_photo.right + 10,lab_username.bottom + 6, img_top_bg.width - img_photo.right -30, 15)];
    lab_email.text = @"余额：0.000元";
    lab_email.font = RFont(15);
    lab_email.textColor = [UIColor whiteColor];
    [img_top_bg addSubview:lab_email];
    
    UIView *view_center = [[UIView alloc]initWithFrame:CGRectMake(11, img_photo.bottom + 29, headerView.width - 22, 79)];
    view_center.backgroundColor = [UIColor whiteColor];
    view_center.layer.shadowColor = [UIColor colorWithRed:57/255.0 green:132/255.0 blue:232/255.0 alpha:0.1].CGColor;
    view_center.layer.shadowOffset = CGSizeMake(0,2);
    view_center.layer.shadowOpacity = 1;
    view_center.layer.shadowRadius = 7;
    view_center.layer.cornerRadius = 6;
    [headerView addSubview:view_center];
    
    //充值
    UPdownButton *btn_Recharge = [UPdownButton buttonWithType:UIButtonTypeCustom];
    [btn_Recharge setImage:[UIImage imageNamed:@"icon_fund_recharge"] forState:UIControlStateNormal];
    [btn_Recharge setTitle:@"充值" forState:UIControlStateNormal];
    [btn_Recharge setTitleColor:UIColorHex(333333) forState:UIControlStateNormal];
    [btn_Recharge setTitleColor:UIColorHex(3C87ED) forState:UIControlStateSelected];
    btn_Recharge.titleLabel.font = RFont(18);
    btn_Recharge.frame = CGRectMake(14, 12.5, 50, 54);
    btn_Recharge.tag =0;
    btn_Recharge.selected = YES;
    self.selectedBtn = btn_Recharge;
    [btn_Recharge addTarget:self action:@selector(btn_threeBtn_click:) forControlEvents:UIControlEventTouchUpInside];
    [view_center addSubview:btn_Recharge];
    
//    UILabel *lab_line_one = [[UILabel alloc]initWithFrame:CGRectMake(btn_Recharge.right + 50, 21, 1, 32)];
//    lab_line_one.backgroundColor = UIColorHex(F0F0F0);
//    [view_center addSubview:lab_line_one];
    
    //提现
    UPdownButton *btn_cash = [UPdownButton buttonWithType:UIButtonTypeCustom];
    [btn_cash setImage:[UIImage imageNamed:@"icon_fund_cash"] forState:UIControlStateNormal];
    [btn_cash setTitle:@"提现" forState:UIControlStateNormal];
    [btn_cash setTitleColor:UIColorHex(333333) forState:UIControlStateNormal];
    [btn_cash setTitleColor:UIColorHex(3C87ED) forState:UIControlStateSelected];
    btn_cash.titleLabel.font = RFont(18);
//    btn_cash.frame = CGRectMake(14, 10, 50, 54);
//    btn_cash.centerX = view_center.centerX;
     btn_cash.frame = CGRectMake((view_center.width - 50)/2 - 14, 12.5, 50, 54);
    btn_cash.tag =1;
    [btn_cash addTarget:self action:@selector(btn_threeBtn_click:) forControlEvents:UIControlEventTouchUpInside];
    [view_center addSubview:btn_cash];
    
//    UILabel *lab_line_two = [[UILabel alloc]initWithFrame:CGRectMake(btn_cash.right + 50, 21, 1, 32)];
//    lab_line_two.backgroundColor = UIColorHex(F0F0F0);
//    [view_center addSubview:lab_line_two];
    
    //资金明细
    UPdownButton *btn_fund_details = [UPdownButton buttonWithType:UIButtonTypeCustom];
    [btn_fund_details setImage:[UIImage imageNamed:@"icon_fund_funddetail"] forState:UIControlStateNormal];
    [btn_fund_details setTitle:@"资金明细" forState:UIControlStateNormal];
    [btn_fund_details setTitleColor:UIColorHex(333333) forState:UIControlStateNormal];
    [btn_fund_details setTitleColor:UIColorHex(3C87ED) forState:UIControlStateSelected];
    btn_fund_details.titleLabel.font = RFont(18);
    btn_fund_details.frame = CGRectMake(view_center.width - 82, 12.5, 72, 54);
    btn_fund_details.tag =2;
    [btn_fund_details addTarget:self action:@selector(btn_threeBtn_click:) forControlEvents:UIControlEventTouchUpInside];
    [view_center addSubview:btn_fund_details];
    
    [self.view addSubview:headerView];
}

#pragma 导航栏右边的按钮
- (void)btn_right_mine_click{
    [self tabHiddenOrShow];
}
#pragma 充值、提现、资金明细
- (void)btn_threeBtn_click:(UIButton *)btn{
    if (btn.tag == self.selectedBtn.tag) {
        return;
    }
    self.selectedBtn.selected = NO;
    btn.selected = YES;
    self.selectedBtn = btn;
    
    if (btn.tag ==0) {
        self.view_recharge.hidden = NO;
        self.view_cashWithdrawal.hidden = YES;
        self.view_detailFund.hidden = YES;
    }else if(btn.tag == 1){
         self.view_recharge.hidden = YES;
        self.view_cashWithdrawal.hidden = NO;
        self.view_detailFund.hidden = YES;
    }else if (btn.tag == 2){
        self.view_recharge.hidden = YES;
        self.view_cashWithdrawal.hidden = YES;
        self.view_detailFund.hidden = NO;
    }
}

@end
