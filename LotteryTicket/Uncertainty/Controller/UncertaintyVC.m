//
//  UncertaintyVC.m
//  LotteryTicket
//
//  Created by BRAINDESIGN on 2019/8/9.
//  Copyright © 2019 hyx. All rights reserved.
//

#import "UncertaintyVC.h"
#import "UncertaintyTableVCell.h"
#import "UncertaintyHeaderView.h"
#import "UncertaintyFooterView.h"

#import "OverTodayVC.h"

@interface UncertaintyVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;

@end

@implementation UncertaintyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"未结算";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    CGFloat vw_ht = 0;
    if (isIphoneX) {
        vw_ht = 105;
    }else{
        vw_ht = 71;
    }
      CGFloat navH = NaviBarHeight;
    _tableView.height = SCREEN_HEIGHT - vw_ht - navH;
    UncertaintyFooterView *footerView = [[UncertaintyFooterView alloc]initWithFrame:CGRectMake(0, _tableView.bottom, SCREEN_WIDTH, vw_ht)];
    footerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:footerView];
}
#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
//        CGFloat tabH = TabBarHeight;
//        CGFloat navH = NaviBarHeight;
        CGFloat ht = SCREEN_HEIGHT;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ht) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 51.0;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        UncertaintyHeaderView *headerView = [[UncertaintyHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 33)];
        [_tableView setTableHeaderView:headerView];
        [_tableView setTableFooterView:[UIView new]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return _tableView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //给每个cell设置ID号（重复利用时使用）
    static NSString *cellID = @"UncertaintyTableVCell";
    
    //从tableView的一个队列里获取一个cell
    UncertaintyTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //判断队列里面是否有这个cell 没有自己创建，有直接使用
    if (cell == nil) {
        //没有,创建一个
        cell = [[UncertaintyTableVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OverTodayVC *vc = [OverTodayVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
